<?php

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\PlanController;
use App\Http\Controllers\Admin\StripeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Chat\ChatController;
use App\Http\Controllers\Chat\GroupChatController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EventScheduleController;
use App\Http\Controllers\FileshareController;
use App\Http\Controllers\Firstlogin as ControllersFirstlogin;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Invite\InviteController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\TodoController;
use App\Http\Middleware\Firstlogin;
use App\Http\Middleware\isAdmin;
use App\Http\Middleware\PlanAndPayment;
use App\Http\Middleware\SetSpace;
use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');




Route::post('/broadcast',[ChatController::class , 'broadcastauth']);

Route::get('/auth/who' ,[HomeController::class , 'who'] );


// public routes
Route::prefix('/')->group( function () {



    //auth routes
    Route::get('/register', function() {
        return view('auth.register');
    })->name("register");

    Route::get('/login', function() {
        return view('auth.login');
    })->name("login");



    Route::post('/login', [LoginController::class , 'login' ]);


    Route::get('/logout', [LoginController::class , 'logout' ]);




});




Route::middleware(['auth' , Firstlogin::class , PlanAndPayment::class])->group( function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');






});


// first login

Route::middleware(['auth'])->prefix('/first')->group( function () {
    Route::get('/setup/brand', [ControllersFirstlogin::class, 'brandsetup']);
    Route::post('/setup/brand/add', [ControllersFirstlogin::class, 'addbrand']);

    Route::get('/setup/plan', [PlanController::class , 'setupPlan' ]);
    Route::get('/setup/plan/{id}', [PlanController::class , 'updatePlan' ]);

    // setup payment
    Route::get('/setup/payment', [PlanController::class , 'setupPayment' ]);

    // payment success
    Route::get('/setup/payment/success', [PlanController::class , 'setupPaymentSuccess' ]);

    // payment success
    Route::get('/setup/payment/cancel', [PlanController::class , 'setupPaymentCancel' ]);


});



Route::middleware('auth')->group(function(){


Route::get('/', function () {
    return redirect('/home');
});



// Activity Global


Route::middleware([ Firstlogin::class , SetSpace::class, PlanAndPayment::class  ])->group(function () {

    Route::get('/activity' , [ActivityController::class , 'index']);

    // Todos

    Route::prefix('/todo')->group( function () {



        Route::prefix('/list')->group( function () {
            // index
            Route::get('/', [TodoController::class, 'index']);



            // new todo list
            Route::get('/new', [TodoController::class, 'newTodoList']);
            Route::post('/new', [TodoController::class, 'storeTodoList']);



            // delete
            Route::delete('/', [TodoController::class, 'deleteTodoList']);


            // get Todos
            Route::get('/{id}', [TodoController::class, 'insideList']);

        });


        // Addnew Todo
        Route::get('/add/{id}', [TodoController::class, 'addTodo']);
        Route::post('/add/{id}', [TodoController::class, 'storeTodo']);


        // Show Todo
        Route::get('/show/{listid}/{todoid}', [TodoController::class, 'showtodo']);

        Route::get('/removeuser/{todoid}/{userid}', [TodoController::class, 'removeuser']);


        // delete
        Route::delete('/delete', [TodoController::class, 'deleteTodo']);
        Route::post('/toggle', [TodoController::class, 'toggletodo']);


        // space
        Route::get('/{space}/{id}', [TodoController::class, 'index']);



    });

    // Route::prefix('/event')


    Route::prefix('/fileshare')->group(function(){





        // folder
        Route::prefix('/folder')->group(function(){
            Route::post('/new' , [FileshareController::class , 'addFolder']);
            Route::post('/delete' , [FileshareController::class , 'deletefolder']);
            Route::get('/{code}' , [FileshareController::class , 'getfolder']);
        });

        // file
        Route::prefix('/file')->group(function(){
            Route::post('/new' , [FileshareController::class , 'addFile']);
            Route::post('/delete' , [FileshareController::class , 'deletefile']);
            Route::get('/{code}' , [FileshareController::class , 'getfile']);

            Route::get('/removeuser/{fileid}/{userid}' , [FileshareController::class , 'removeFileUser']);
            Route::post('/adduser' , [FileshareController::class , 'addFileUser']);
        });

        // Document
        Route::prefix('/document')->group(function(){
            Route::post('/new' , [FileshareController::class , 'addDocument']);
            Route::post('/update' , [FileshareController::class , 'updateDocument']);
            Route::post('/delete' , [FileshareController::class , 'deleteDocument']);
            Route::post('/copy' , [FileshareController::class , 'copyDocument']);
            Route::post('/move' , [FileshareController::class , 'moveDocument']);

            // access add or remove
            Route::get('/removeuser/{documentid}/{userid}' , [FileshareController::class , 'removeDocumentUser']);
            Route::post('/adduser' , [FileshareController::class , 'addDocumentUser']);

            // Document iframe
            Route::get('iframe/{code}' , [FileshareController::class , 'getiframe']);
            Route::get('/{code}' , [FileshareController::class , 'getdocument']);

        });

        // Document
        Route::prefix('/google-doc')->group(function(){
            Route::post('/new' , [FileshareController::class , 'addGoogleDocument']);
            // Route::post('/update' , [FileshareController::class , 'updateDocument']);
            Route::post('/delete' , [FileshareController::class , 'deleteGoogleDocument']);
            Route::post('/copy' , [FileshareController::class , 'copyGoogleDocument']);
            Route::post('/move' , [FileshareController::class , 'moveGoogleDocument']);

            // // access add or remove
            Route::get('/removeuser/{documentid}/{userid}' , [FileshareController::class , 'removeGoogleDocumentUser']);
            Route::post('/adduser' , [FileshareController::class , 'addGoogleDocumentUser']);

            // // Document iframe
            // Route::get('iframe/{code}' , [FileshareController::class , 'getiframe']);
            Route::get('/{code}' , [FileshareController::class , 'getGoogleDoc']);

        });

        Route::get('/getsubfolder/{folderid}' , [FileshareController::class , 'getsubfolder']);
        Route::post('/copy' , [FileshareController::class , 'copyfile']);
        Route::post('/move' , [FileshareController::class , 'movefile']);

        // space
        Route::get('/{space}/{id}', [FileshareController::class, 'index']);

    });

    // chat
    Route::prefix('/chat')->group(function(){

        Route::get('/', [ChatController::class , 'index']);
        Route::get('/messages/{with}', [ChatController::class , 'fetchMessages']);
        Route::post('/messages/{with}', [ChatController::class , 'sendMessage']);


        // space
        Route::get('/{space}/{id}', [ChatController::class, 'setSpace']);

    });

    // chat
    Route::prefix('/groupchat')->group(function(){

        Route::get('/', [GroupChatController::class , 'index']);
        Route::get('/messages', [GroupChatController::class , 'fetchMessages']);
        Route::post('/messages', [GroupChatController::class , 'sendMessage']);


        // space
        Route::get('/{space}/{id}', [GroupChatController::class, 'setSpace']);

    });



    // schedule
    Route::prefix('/schedule')->group(function(){

        Route::get('/', [EventScheduleController::class , 'index']);
        Route::post('/event/store', [EventScheduleController::class , 'store']);
        Route::post('/event/delete', [EventScheduleController::class , 'delete']);
        Route::post('/event/update', [EventScheduleController::class , 'update']);



        // space
        Route::get('/{space}/{id}', [EventScheduleController::class, 'setSpace']);

    });




    // company

    Route::prefix('/company')->group( function () {

        // update logo
        Route::post('/update/logo/{id}', [CompanyController::class, 'logoupdate']);

        // add
        Route::post('/add', [CompanyController::class, 'create']);


        // controlpanel
        Route::get('/controlpanel', [CompanyController::class, 'controlpanel']);
        // Route::get('/todos', [CompanyController::class, 'showtodos']);

        // invite
        Route::get('/invite', [CompanyController::class, 'invite']);


        //////////// ajax

        // rename
        Route::post('/rename/{id}', [CompanyController::class, 'rename']);
        Route::post('/color/{id}', [CompanyController::class, 'changecolor']);
        Route::post('/pin/{id}', [CompanyController::class, 'pinstatus']);
        Route::post('/ajax/delete/{id}', [CompanyController::class, 'ajaxdelete']);





    });


    // team

    Route::prefix('/team')->group( function () {



        // add
        Route::post('/add', [TeamController::class, 'create']);

        // controlpanel
        Route::get('/controlpanel', [TeamController::class, 'controlpanel']);



        //////////// ajax

        // rename
        Route::post('/rename/{id}', [TeamController::class, 'rename']);
        Route::post('/ajax/delete/{id}', [TeamController::class, 'ajaxdelete']);





    });



    // project

    Route::prefix('/project')->group( function () {



        // add
        Route::post('/add', [ProjectController::class, 'create']);


        // controlpanel
        Route::get('/controlpanel', [ProjectController::class, 'controlpanel']);

        //////////// ajax

        // rename
        Route::post('/rename/{id}', [ProjectController::class, 'rename']);
        Route::post('/ajax/delete/{id}', [ProjectController::class, 'ajaxdelete']);





    });



});










});


// invite

Route::prefix('/invite')->group( function () {

    // root
    Route::get('/', [InviteController::class, 'invite']);

    // add
    Route::post('/add', [InviteController::class, 'addInvitation']);


    // add
    // Route::get('/accept/{code}', [InviteController::class, 'acceptInvitation']);

    // email preview for test only
    Route::get('/emailpreview', [InviteController::class, 'emailpreview']);

    // invite
    Route::get('/accept/{code}', [InviteController::class, 'acceptInvitation']);


    // space
    Route::get('/{space}/{id}', [InviteController::class, 'setSpace']);



});


Route::get('/admin-login' , [LoginController::class , 'Adminloginform']);
Route::post('/admin-login' , [LoginController::class , 'Adminlogin']);

Route::prefix('/admin')->middleware([isAdmin::class])->group(function(){

    Route::get('/' , [AdminController::class , 'index']);
    Route::get('/dashboard' , [AdminController::class , 'index']);
    Route::get('/users' , [AdminController::class , 'users']);
    Route::post('/user/delete' , [AdminController::class , 'delete_user']);
    Route::post('/user/disablestatus' , [AdminController::class , 'toggle_status_user']);


    // plans
    Route::prefix('/plans')->group( function () {
        Route::get('/' , [PlanController::class , 'index']);
        Route::post('/new' , [PlanController::class , 'store']);
        Route::post('/update/{id}' , [PlanController::class , 'update']);
    });


    // plans
    Route::prefix('/stripe/plans')->group( function () {
        Route::get('/' , [StripeController::class , 'listplans']);
        Route::post('/new' , [StripeController::class , 'store']);
        Route::post('/update/{id}' , [StripeController::class , 'update']);
    });

});

