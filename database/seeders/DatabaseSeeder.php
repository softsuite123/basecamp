<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // DB::table('users')->delete();
        // DB::table('admins')->delete();


        // DB::table('admins')->insert([

        //     [
        //         'name' => 'admin',
        //         'email' => 'admin@admin.com',
        //         'password' => bcrypt('12345678'),
        //         'created_at' => date('Y-m-d H:i:s'),
        //         'updated_at' => date('Y-m-d H:i:s'),

        //     ]


        // ]);

        // DB::table('users')->insert([

        //     [
        //         'name' => 'demo',
        //         'email' => 'demo@demo.com',
        //         'password' => bcrypt('12345678'),
        //         'created_at' => date('Y-m-d H:i:s'),
        //         'updated_at' => date('Y-m-d H:i:s'),
        //         'first_login' => 1
        //     ]


        // ]);


        DB::table('configs')->insert([

            [
                'name' => 'trial_days',
                'value' => '14',


            ]


        ]);


    }
}
