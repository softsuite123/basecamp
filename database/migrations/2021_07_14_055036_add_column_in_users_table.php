<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->foreignId('payment_plan_id')->nullable();
            $table->integer('plan_amount')->nullable();
            $table->integer('payment_status')->default(0);
            $table->timestamp('trial_start')->nullable();
            $table->timestamp('trial_ends')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('plan_id');
            $table->dropColumn('trial_start');
            $table->dropColumn('plan_amount');
            $table->dropColumn('trial_start');
            $table->dropColumn('trial_ends');
        });
    }
}
