<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoogleDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_docs', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->text('link')->nullable();
            $table->text('code');
            $table->text('space');
            $table->integer('space_id');
            $table->foreignId('folder_id')->nullable();
            $table->text('folder_code')->nullable();
            $table->text('icon')->nullable();
            $table->foreignId('user_id');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_docs');
    }
}
