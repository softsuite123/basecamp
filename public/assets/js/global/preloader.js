var $preloader = false;

function preloader_init(){

    $('body').append(`

        <div id="preloader" class="preloader" style="position:fixed; width:100vw; height: 100vh; z-index:100; top:0; left:0; background-color:#00000036; display:none;" >
            <img src="${$baseurl}/assets/images/boat-gif.gif" style="height:130px; width:auto ; position: relative ; top: 50%; left: 50% ; transform: translate(-50% , -50%);" >
        </div>

    `);

}

function preloader(setto){
    if(setto == true){
        $('#preloader').fadeIn();
    }
    if(setto == false){
        $('#preloader').fadeOut();

    }
}


$(document).ready(function(){
    $preloader = true;
    preloader_init();
});
