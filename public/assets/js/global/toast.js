function toast(config){
    var msg = config.message;
    var icon = config.icon;
    var type = config.type;

    if(type == "success"){
        var theme= 'green';
    }
    if(type == "warning"){
        var theme= 'red';
    }


    var toast= `

        <div class="toast">
            <div class="icon">
                <i class="${icon}" style="color:${theme};"></i>
            </div>
            <div class="message"> ${msg} </div>
        </div>
    `;


    $('body').append(toast);

    $('.toast').animate({
        "top": "60px"
    });

    setTimeout(() => {
        closetoast();
    }, 3000);


}


function closetoast(){
    $('.toast').animate({
        "top": "-50px"
    });

    setTimeout(() => {
        $('.toast').remove();
    }, 1000);

}
