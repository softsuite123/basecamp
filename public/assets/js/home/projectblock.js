


// project block script



$(document).ready(function(){

    $('.projectsection .editor-toggle').click(function(){
        var parent = $(this).parent().parent();


        var menu = parent.find('.menu');

        // console.log(menu.html());

        menu.css({'transform': "translateX(0%)"});



    });

    $('.projectsection .editor-close').click(function(){
        var parent = $(this).parent().parent();


        var menu = parent.find('.menu');

        // console.log(menu.html());

        menu.css({'transform': "translateX(100%)"});



    });


    $('.projectsection .rename').click(function(){
        var parent = $(this).parent().parent().parent();

        var editor = parent.find('.editor');
        editor.css({'transform': "translateX(0%)"});



    });

    $('.projectsection .renamecancel').click(function(){
        var parent = $(this).parent().parent().parent();

        var editor = parent.find('.editor');
        editor.css({'transform': "translateX(100%)"});



    });

    // rename team
    $('.projectsection .renamesave').click(function(){
        var parent = $(this).parent().parent().parent();

        var editor = parent.find('.editor');
        var menu = parent.find('.menu');
        var view = parent.find('.view');

        // editor.css({'transform': "translateX(100%)"});

        var newname = parent.find('.renameinput').val();
        var project_id = parent.find('.renameinput').attr('projectid');


        if(newname.length > 0){
            $.ajax({

                url: `${$baseurl}/project/rename/${project_id}`,
                method: 'POST',
                data: {
                    name: newname,
                    _token : $token
                },
                success: function(data){
                    // console.log(data);
                    if(data.status == 200){
                        editor.css({'transform': "translateX(100%)"});
                        menu.css({'transform': "translateX(100%)"});

                        parent.find('.project_name').html(newname);

                        view.addClass('successpulse');
                        setTimeout(() => {
                            view.removeClass('successpulse');

                        }, 500);


                        // view.animate({'background-color' : 'green'} , 1000 , function(){
                        //     view.animate({'background-color' : "white"});
                        // })



                    }
                }


            });
        }


    });


    $('.projectsection .teamdelete').click(function(){
        var parent = $(this).parent().parent().parent();

        var editor = parent.find('.editor');
        var menu = parent.find('.menu');
        var view = parent.find('.view');
        var projectid = parent.attr('projectid');

        $.ajax({

            url: `${$baseurl}/project/ajax/delete/${projectid}`,
            method: 'POST',
            data: {

                _token : $token
            },
            success: function(data){
                // console.log(data);
                if(data.status == 200){
                    parent.parent().fadeOut('slow');
                    setTimeout(() => {
                        parent.parent().remove();
                    }, 1500);


                }
            }


        });

    });



});
