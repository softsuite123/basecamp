


// company block script



$(document).ready(function(){

    $('.companysection .editor-toggle').click(function(){
        var parent = $(this).parent().parent();


        var menu = parent.find('.menu');

        // console.log(menu.html());

        menu.css({'transform': "translateX(0%)"});



    });

    $('.companysection .editor-close').click(function(){

        var parent = $(this).parents('.card');


        var menu = parent.find('.menu');

        // console.log(menu.html());

        menu.css({'transform': "translateX(100%)"});



    });


    $('.companysection .rename').click(function(){
        var parent = $(this).parents('.card');

        var editor = parent.find('.editor');
        editor.css({'transform': "translateX(0%)"});



    });

    $('.companysection .renamecancel').click(function(){
        var parent = $(this).parents('.card');

        var editor = parent.find('.editor');
        editor.css({'transform': "translateX(100%)"});



    });

    // rename company
    $('.companysection .renamesave').click(function(){
        var parent = $(this).parents('.card');

        var editor = parent.find('.editor');
        var menu = parent.find('.menu');
        var view = parent.find('.view');

        // editor.css({'transform': "translateX(100%)"});

        var newname = parent.find('.renameinput').val();
        var company_id = parent.find('.renameinput').attr('companyid');


        if(newname.length > 0){
            $.ajax({

                url: `${$baseurl}/company/rename/${company_id}`,
                method: 'POST',
                data: {
                    name: newname,
                    _token : $token
                },
                success: function(data){
                    // console.log(data);
                    if(data.status == 200){
                        editor.css({'transform': "translateX(100%)"});
                        menu.css({'transform': "translateX(100%)"});

                        parent.find('.company_name').html(newname);

                        view.addClass('successpulse');
                        setTimeout(() => {
                            view.removeClass('successpulse');

                        }, 500);


                        // view.animate({'background-color' : 'green'} , 1000 , function(){
                        //     view.animate({'background-color' : "white"});
                        // })



                    }
                }


            });
        }


    });


    $('.companysection .companydelete').click(function(){
        var parent = $(this).parents('.card');

        var editor = parent.find('.editor');
        var menu = parent.find('.menu');
        var view = parent.find('.view');
        var companyid = parent.attr('companyid');

        $.ajax({

            url: `${$baseurl}/company/ajax/delete/${companyid}`,
            method: 'POST',
            data: {

                _token : $token
            },
            success: function(data){
                // console.log(data);
                if(data.status == 200){
                    parent.parent().fadeOut('slow');
                    setTimeout(() => {
                        parent.parent().remove();
                    }, 1500);


                }
            }


        });

    });


    $('.companysection .colorpicker').change(function(){
        var parent = $(this).parents('.card');

        var editor = parent.find('.editor');
        var menu = parent.find('.menu');
        var view = parent.find('.view');
        var company_id = parent.attr('companyid');


        var color = $(this).val();

        // console.log(view.html());
        $(view).css({"background-color": `${color}40`} );
        $.ajax({

            url: `${$baseurl}/company/color/${company_id}`,
            method: 'POST',
            data: {

                _token : $token,
                color: color
            },
            success: function(data){
                // console.log(data);
                if(data.status == 200){
                    editor.css({'transform': "translateX(100%)"});
                    menu.css({'transform': "translateX(100%)"});


                    view.addClass('successpulse');
                    setTimeout(() => {
                        view.removeClass('successpulse');

                    }, 500);


                    // view.animate({'background-color' : 'green'} , 1000 , function(){
                    //     view.animate({'background-color' : "white"});
                    // })



                }
            }


        });
    });




    $('.companysection .pin').click(function(){
        var parent = $(this).parents('.card');

        var editor = parent.find('.editor');
        var menu = parent.find('.menu');
        var view = parent.find('.view');
        var company_id = parent.attr('companyid');


        var pin = parseInt($(this).attr('pin'));
        // pin=  !pin;

        // pin;
        if(pin == 1){
            pin = 0;
        }
        else{
            pin = 1;
        }
        // console.log(pin);
        // return;



        $.ajax({

            url: `${$baseurl}/company/pin/${company_id}`,
            method: 'POST',
            data: {

                _token : $token,
                pin: pin
            },
            success: function(data){
                // console.log(data);
                if(data.status == 200){
                    editor.css({'transform': "translateX(100%)"});
                    menu.css({'transform': "translateX(100%)"});

                    console.log(pin);

                    if(pin == 1){
                        view.find('.pin-icon').show();
                        menu.find('.pin').attr('pin' , 1);
                    }
                    else{
                        view.find('.pin-icon').hide();
                        menu.find('.pin').attr('pin' , 0);

                    }

                    view.addClass('successpulse');
                    setTimeout(() => {
                        view.removeClass('successpulse');

                    }, 500);


                    // view.animate({'background-color' : 'green'} , 1000 , function(){
                    //     view.animate({'background-color' : "white"});
                    // })



                }
            }


        });
    });

});
