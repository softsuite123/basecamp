<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        Blade::if('owner', function ($model) {
            // return '&lt;?php endif; ?&gt;';
            if($model->user_id == Auth::user()->id){
                return true;
            }
            else{
                return false;
            }



        });

        Blade::if('hasAccess', function ($model) {
            // return '&lt;?php endif; ?&gt;';
            $model->load('users');
            $users = $model->users->pluck('id')->toArray();

            // dd($users);
            if(Auth::user()->id == $model->user_id){
                return true;
            }

            if(in_array(Auth::user()->id , $users)){
                return true;
            }
            else{
                return false;
            }



        });
    }
}
