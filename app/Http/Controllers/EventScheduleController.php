<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Event\CalendarEvent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventScheduleController extends Controller
{
    //

    public function index(){
        $events = $this->get();
        // dd($events);
        return view('front.event-schedule.index')->with('events', $events);
    }

    public function store(Request $request){
        // dd($request->all());
        // Load Space
        $space = SpaceController::get();
        $space->load('user');


        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);

        $event = CalendarEvent::create([
            'name' => $request->name,
            'description' => $request->description,
            'from' => $request->from,
            'to' => $request->to,
            'user_id' => Auth::user()->id,
            'space' => $namespace,
            'space_id' => $space->id
        ]);

        $fd =Carbon::parse($event->from, 'UTC');

        $fromdate = $fd->isoFormat('MMM Do YY');
        Activity::setActivity(
            "New Event Added to Calendar" ,
            "$event->name is at $fromdate",
            "/schedule"
        );

        return response()->json([
            'status' => 200 ,
            'event' => $event
        ]);
    }


    public function get(){
        $space = SpaceController::get();
        $space->load('user');


        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);

        $events = CalendarEvent::where([
            "space" => $namespace,
            "space_id" => $space->id

        ])->get();


        $thisevent = [];

        foreach($events as $event){
            $newevent =  [
                'id' => $event->id,
                'title' => $event->name,
                'description' => $event->description,
                'start' => $event->from,
                'end' => $event->to,
                'allday' => true
            ];
            array_push($thisevent, $newevent);
        }



        return $thisevent;

    }

    public function delete(Request $request){
        // dd($request->all());
        $event = CalendarEvent::where('id' ,$request->eventid)->first();

        if($event){
            if($event->user_id != Auth::user()->id){
                return response()->json([
                    "status" => 403,
                    "message" => "You are not allowed to delete this event"
                ]);
            }
            else{
                $event->delete();
                return response()->json([
                    "status" => 200,
                    "message" => "$event->name is Deleted!"
                ]);
            }
        }
        else{
            return response()->json([
                "status" => 404,
                "message" => "Event not found"
            ]);
        }
    }

    public function update(Request $request){
        $event = CalendarEvent::where('id' ,$request->eventid)->first();

        if($event){
            if($event->user_id != Auth::user()->id){
                return response()->json([
                    "status" => 403,
                    "message" => "You are not allowed to Update this event"
                ]);
            }
            else{
                $event->from = $request->from;
                $event->to = $request->to;

                $event->save();

                return response()->json([
                    "status" => 200,
                    "message" => "$event->name is Updated!"
                ]);
            }
        }
        else{
            return response()->json([
                "status" => 404,
                "message" => "Event not found"
            ]);
        }
    }



    public function setSpace(Request $request){
        SpaceController::set($request->space , $request->id );
        return redirect('/schedule');
    }

}
