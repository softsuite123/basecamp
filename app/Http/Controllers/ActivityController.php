<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    //

    public function index(){

        $activities = (User::where('id' , Auth::user()->id)->with('activities' , function($q){
            // dd($q->get());
            $q->with('author')->orderBy('created_at', 'desc');

        })->first())->activities;
        $counter = 1;
        $slingleft = $slingright = [];
        foreach($activities as $activity){
            if($counter % 2 ==0){
                array_push($slingright, $activity);
            }
            else{
                array_push($slingleft, $activity);
            }
            // echo($counter);
            $counter = $counter + 1;
        }
        // dd($slingleft , $slingright);
        return view('front.global.sidebar.activity')->with(compact('slingleft', 'slingright'));
    }



}
