<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd("fd");
        // $stripe = \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        // $session = \Stripe\BillingPortal\Session::create([
        //     'customer' => Auth::user()->stripe_account,
        //     'return_url' => url('/').'/home',
        // ]);

        // return redirect()->to($session->url);



        return view('front.home.index');
    }

    public function who(){
        return response()->json([
            'auth' => Auth::user(),
        ]);
    }
}
