<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamController extends Controller
{
    //

    public function controlpanel(){
        return view('front.controlpanel.team.index');
    }


    public function create(Request $request){
        // dd($request->all());

        $team  = Team::create([
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => Auth::user()->id,
            'company_id' => $request->company_id
        ]);

        $user = User::where("id",Auth::user()->id)->pluck('id');

            $team->user()->attach($user , [
                'type' => 'owner'
            ]);

        return redirect()->back();


    }


    public function rename(Request $request){
        $team = Team::where('id' , $request->id)->first();
        $team->name = $request->name;
        $team->save();


        return response()->json([
            "status" => 200
        ]);


    }

    public function ajaxdelete(Request $request){
        $team = Team::where([
            'id' => $request->id,
            'user_id' => Auth::user()->id
        ])->first();

        $team->delete();


        return response()->json([
            "status" => 200
        ]);


    }

}
