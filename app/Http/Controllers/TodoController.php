<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Company;
use App\Models\Todo;
use App\Models\TodoActivity;
use App\Models\TodoList;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class TodoController extends Controller
{
    //

    public function index(Request $request){

        // dd($request->space);

        SpaceController::set($request->space , $request->id );

        return view('front.todos.index');
    }

    public function newTodoList(){
        // dd('hit');
        return view('front.todos.newlist');

    }

    public function storeTodoList(Request $request){
        // Load Space
        $space = SpaceController::get();
        // dd($space);

        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);
        // dd($space_class);

        // create Todo
        $todolist = TodoList::create([
            'name' => $request->name,
            'description' => $request->description,
            'space' => $namespace,
            'user_id' => Auth::user()->id
        ]);


        // Attach Space with Todo
        $todolist->$space_class()->attach($space->id , [
            'type' => 'owner'
        ]);

        return redirect(URL::to('/todo/list'));

    }


    public function deleteTodoList(Request $request){
        // dd($request->all());
        $space = SpaceController::get();

        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);

        $todolist = TodoList::where('id', $request->listid)->first();



        // Attach Space with Todo
        $todolist->$space_class()->detach($space->id );


        // remove todo List
        $todolist->delete();


        return response()->json([
            "code" => 200,
            "message" => "Todo List Deleted!"
        ]);




    }

    public function insideList(Request $request){
        // dd($request->all());
        $todo_list_id = $request->id;
        return view('front.todos.todos')->with('todo_list_id',$todo_list_id);
    }


    public function addTodo(Request $request){
        // dd('hit');
        $todo_list_id = $request->id;
        // $list = TodoList::where('id',$list_id)->first();
        return view('front.todos.newtodo')->with('todo_list_id',$todo_list_id);


    }

    public function storeTodo(Request $request){

        // dd($request->all());

        $todo_list_id = $request->id;

        $space = SpaceController::get();
        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);



        $assigned_users = $request->assignedUsers  ;
        if(!isset($request->assignedUsers)){
            $assigned_users = [];
        }
        $notify_users = $request->notifyUsers;
        if(!isset($request->notifyUsers)){
            $notify_users = [];
        }
        $date_option = $request->radio_date;


        if($date_option == 1){
            // $date
            $due_date = null;

        }
        elseif($date_option == 2){
            $due_date = $request->due_date;

        }
        elseif($date_option == 3){
            $due_date = $request->due_multiple;

        }
        else{
            $due_date = null;
        }


        $todo = Todo::create([
            'name' => $request->name,
            'description' => $request->description,
            'space' => $namespace,
            'space_id' => $space->id,
            'user_id' => Auth::user()->id,
            'status' => 1,
            'todo_list_id' => $todo_list_id,
            'due_date' => $due_date,
            'is_completed' => false
        ]);

        $user = User::where('id', Auth::user()->id)->first();

        $activity = TodoActivity::create([
            "note" => "Created By $user->name",
            'user_id' => $user->id,
            'space' => $namespace,
            'space_id' => $space->id,
            'todo_id' => $todo->id
        ]);


        Activity::setActivity(
            "Todo Created By $user->name" ,
            "$request->name is created ",
            "/todo/show/$todo_list_id/$todo->id"
        );





        $users = User::where('id', $assigned_users)->get();

        foreach($users as $user){
            // Attach User with Todo
            $todo->User()->attach($user , [
                'type' => 'member'
            ]);



            $data = [
                "todo" => $todo
            ];
            $email_to = $user->email;
            $name_to = $user->name;
            $ref_name = Auth::user()->name;

            Mail::send('front.todos.emailAssignTodo', $data, function($message) use ($email_to , $name_to , $ref_name) {
                $message->to($email_to, $name_to)->subject
                   ("$ref_name assigned a Todo");
                $message->from(env('MAIL_FROM_ADDRESS'),env('APP_NAME'));
            });


        }

        $todoList = TodoList::where('id' , $todo_list_id)->first();

        // Attach List with Todo
        $todoList->Todo()->attach($todo->id , [
            'type' => 'owner'
        ]);

        // Attach User with Todo
        $todo->User()->attach(Auth::user()->id , [
            'type' => 'owner'
        ]);


        // dd($todo);
        return redirect(URL::to("/todo/list/$todo_list_id"));
    }


    public function deleteTodo(Request $request){

        $space = SpaceController::get();

        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);

        $todo = Todo::where('id', $request->todoid)->with('User')->first();
        $todolist = TodoList::where('id' , $todo->todo_list_id)->first();

        // dd($todo);
        // Detach Space with Todo
        // $todo->TodoList()->detach($todo->todo_list_id );
        $todolist->Todo()->detach($todo->id );


        foreach($todo->User as $user){

            // Detach User with Todo
            $todo->User()->detach($user->id );
        }



        // remove todo
        $todo->delete();


        return response()->json([
            "code" => 200,
            "message" => "Todo Deleted!"
        ]);




    }


    public function toggletodo(Request $request){
        // dd($request->all());
        $status = $request->status ;
        if($status == 1){
            $status_type = 'Done';
        }
        else{
            $status_type = 'Not Done';
        }
        $todo_id = $request->id;
        $type = $request->type;
        // $listid = $request->listid;

        $todo = Todo::where('id', $todo_id)->first();
        $todo->is_completed= $status;
        $todo->save();


        $space = SpaceController::get();
        // Get Space namespace
        $namespace = get_class($space);

        $user = User::where('id', Auth::user()->id)->first();


        $activity = TodoActivity::create([
            "note" => "Changed the Status to $status_type ",
            'user_id' => $user->id,
            'space' => $namespace,
            'space_id' => $space->id ,
            'todo_id' => $todo->id
        ]);

        Activity::setActivity(
            "Changed the Status to $status_type" ,
            "Changed the Status to $status_type of $todo->name",
            "/todo/show/$todo->todo_list_id/$todo->id"
        );


        // dd($todo);
        return response()->json([
            "status" => 200,
            "todo" => $todo,
            "message" => "$todo->name is marked as $status_type !"
        ]);
    }


    public function showtodo(Request $request){
        $todoid = $request->todoid;
        $listid = $request->listid;
        return view('front.todos.showtodo')->with([
            "todo_id" => $todoid,
            "list_id" => $listid
        ]);
    }

    public function removeuser(Request $request){
        $space = SpaceController::get();

        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);

        $todo = Todo::where('id', $request->todoid)->with('User')->first();
        $todolist = TodoList::where('id' , $todo->todo_list_id)->first();

        // dd($todo);
        // Detach Space with Todo
        // $todo->TodoList()->detach($todo->todo_list_id );
        $todolist->Todo()->detach($todo->id );



        // Detach User with Todo
        $todo->User()->detach($request->userid);


        // remove todo
        // $todo->delete();


        return redirect()->back();

    }


}
