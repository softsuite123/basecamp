<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Project;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SelectionController extends Controller
{
    //


    public static function set($type , $id=null){
        if($type == 'company'){
            self::setCompany($id);
        }
        if($type == 'team'){
            self::setTeam($id);

        }
    }


    public static function setCompany($id = null){
        // dd('set');

        if($id == null){
            $user = User::where('id' , Auth::user()->id)->with('companies')->first();
            // dd($user);
            $companies = $user->companies ;


            $company =  $companies[0];
            // dd($company);
        }
        else{
            $company = Company::where('id', $id  )->with('user')->first();
            // $user = User::where('id' , Auth::user()->id)->with('companies')->first();
            // $companies = $user->companies ;


            // $company =  $companies[0];

            // dd($company);

        }

        session()->put('company', $company);
    }


    public static function getCompany(){


        if(session()->has('company')){
            $company =  session()->get('company');
            // dd('hit');
        }
        else{


            $user = User::where('id' , Auth::user()->id)->with('companies')->first();
            $companies = $user->companies ;

            // dd($user);

            $company =  $companies[0];

            session()->put('company', $company);


            $company =  session()->get('company');
        }

        // dd($company);
        return $company;



    }

    public static function setTeam($id){

        if($id == null){
            $user = User::where('id' , Auth::user()->id)->with('teams')->first();
            // dd($user);
            $teams = $user->teams ;


            $team =  $teams[0];

        }
        else{
            $team = Team::where('id', $id  )->with('user')->first();


        }

        session()->put('team', $team);
    }


    public static function getTeam(){


        if(session()->has('team')){
            $team =  session()->get('team');
            // dd('hit');
        }
        else{


            $user = User::where('id' , Auth::user()->id)->with('teams')->first();
            $teams = $user->teams ;

            // dd($user);

            $team =  $teams[0];

            session()->put('team', $team);


            $team =  session()->get('team');
        }

        // dd($company);
        return $team;



    }



    public static function setProject($id){

        if($id == null){
            $user = User::where('id' , Auth::user()->id)->with('projects')->first();
            // dd($user);
            $projects = $user->projects ;


            $project =  $projects[0];

        }
        else{
            $project = Project::where('id', $id  )->with('user')->first();


        }

        session()->put('project', $project);
    }


    public static function getProject(){


        if(session()->has('project')){
            $project =  session()->get('project');
            // dd('hit');
        }
        else{


            $user = User::where('id' , Auth::user()->id)->with('project')->first();
            $projects = $user->projects ;

            // dd($user);

            $project =  $projects[0];

            session()->put('project', $project);


            $project =  session()->get('project');
        }

        // dd($company);
        return $project;



    }





}
