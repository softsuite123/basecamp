<?php

namespace App\Http\Controllers\Chat;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SpaceController;
use App\Models\Chat\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Events\MessageSent;
use Pusher\Pusher;

class ChatController extends Controller
{
    //
    //remember to use

    public function broadcastauth(Request $request){
        $pusher = new Pusher(env('PUSHER_APP_KEY'),
        env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'));
        return $pusher->socket_auth($request->request->get('channel_name'),$request->request->get('socket_id'));
    }


    public function setSpace(Request $request){
        SpaceController::set($request->space , $request->id );
        return redirect('/chat');
    }

    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.chat.private');
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages(Request $request)
    {

        // Load Space
        $space = SpaceController::get();
        $space->load('user');

        // dd($space);
        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);


        $messages = Message::where('space' , $namespace )->where('space_id' , $space->id)->whereIn(
            'with' , [$request->with , Auth::user()->id],

        )->whereIn('user_id'  , [$request->with , Auth::user()->id] )->with('user')->get();

        // $messages = $messages->where('space' , $namespace );
        // $messages = $messages->where('space_id' , $space->id);

        // dd($messages);

        return response()->json($messages);
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    // public function sendMessage(Request $request)
    // {
    //     $user = Auth::user();


    //     $message = $user->messages()->create([
    //         'message' => $request->input('message')
    //     ]);

    //     return ['status' => 'Message Sent!'];
    // }



    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {


        // Load Space
        $space = SpaceController::get();
        $space->load('user');


        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);

        $user = Auth::user();

        $message = $user->messages()->create([
            'message' => $request->input('message'),
            'with' => $request->with,
            'space' => $namespace,
            'space_id' => $space->id
        ]);

        broadcast(new MessageSent($user, $message))->toOthers();

        return ['status' => 'Message Sent!'];
    }
}
