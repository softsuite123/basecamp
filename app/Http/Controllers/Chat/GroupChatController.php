<?php

namespace App\Http\Controllers\Chat;

use App\Events\MessageSent;
use App\Events\MessageSentGroup;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SpaceController;
use App\Models\Chat\GroupMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupChatController extends Controller
{
    //

    public function index()
    {
        return view('front.chat.group');
    }

    public function setSpace(Request $request){
        SpaceController::set($request->space , $request->id );
        return redirect('/groupchat');
    }


    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages(Request $request)
    {

        // Load Space
        $space = SpaceController::get();
        $space->load('user');

        // dd($space);
        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);


        $messages = GroupMessage::where('space' , $namespace )->where('space_id' , $space->id)->with('user')->get();



        return response()->json($messages);
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    // public function sendMessage(Request $request)
    // {
    //     $user = Auth::user();


    //     $message = $user->messages()->create([
    //         'message' => $request->input('message')
    //     ]);

    //     return ['status' => 'Message Sent!'];
    // }



    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {


        // Load Space
        $space = SpaceController::get();
        $space->load('user');


        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);

        $user = Auth::user();

        $message = $user->group_messages()->create([
            'message' => $request->input('message'),
            'with' => $request->with,
            'space' => $namespace,
            'space_id' => $space->id
        ]);

        broadcast(new MessageSent($user, $message))->toOthers();

        return ['status' => 'Message Sent!'];
    }


}
