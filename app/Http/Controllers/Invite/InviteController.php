<?php

namespace App\Http\Controllers\Invite;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SpaceController;
use App\Models\Invite;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class InviteController extends Controller
{
    //


    public function setSpace(Request $request){
        SpaceController::set($request->space , $request->id );
        return redirect('/invite');
    }


    public function invite(){
        return view('front.controlpanel.invite.index');

    }

    public function addInvitation(Request $request){
        // dd($request->all());


        // Load Space
        $space = SpaceController::get();
        $space->load('user');


        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);


        $users = $request->users;
        $personal_note = $request->personal_note;
        $ref_id = Auth::user()->id;
        $ref_name = Auth::user()->name;

        foreach($users as $user){
            $user = json_decode(json_encode($user));
            // dd($user);
            $selfuser = User::where('email', $user->email)->first();
            if($selfuser->id != Auth::user()->id){


                $email_to = $user->email;
                $name_to = $user->name;

                $data =[
                    'name' => $user->name,
                    'email' => $user->email,
                    'title' => $user->title,
                    'space' => $namespace,
                    'space_id' => $user->company,
                    'code' => $user->id,
                    'ref_id' => $ref_id,
                    'personal_note' => $personal_note

                ];

                // return view('front.company.invite.emailpreview')->with($data);


                Invite::create($data);


                $check = User::where('email' , $user->email)->first();

                if($check){
                    $company = $namespace::where('id' , $user->company)->first();
                    $company->user()->attach($check->id,
                        [
                            'type' => 'member'
                        ]
                    );
                    $data['processed'] = true;
                    Mail::send('front.controlpanel.invite.emailpreview', $data, function($message) use ($email_to , $name_to , $ref_name) {
                        $message->to($email_to, $name_to)->subject
                        ("$ref_name invited you to their Teams Leader!");
                        $message->from(env('MAIL_FROM_ADDRESS'),env('APP_NAME'));
                    });
                }
                else{
                    Mail::send('front.controlpanel.invite.emailpreview', $data, function($message) use ($email_to , $name_to , $ref_name) {
                        $message->to($email_to, $name_to)->subject
                        ("$ref_name invited you to their Teams Leader!");
                        $message->from(env('MAIL_FROM_ADDRESS'),env('APP_NAME'));
                    });
                }



            }



        }


        return response()->json([
            'status' => 200
        ]);

    }

    public function emailpreview(){
        return view('front.company.invite.emailpreview');
    }

    public function acceptInvitation(Request $request){
        $code = $request->code;
        // dd($code);
        $invite = Invite::where('code' , $code)->first();
        return redirect(URL::to("/register?code=$code"));
    }


}
