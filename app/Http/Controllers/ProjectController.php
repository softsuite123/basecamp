<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    //

    public function controlpanel(){
        return view('front.controlpanel.project.index');
    }

    public function create(Request $request){
        // dd($request->all());

        $project  = Project::create([
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => Auth::user()->id,
            'company_id' => $request->company_id
        ]);


        $user = User::where("id",Auth::user()->id)->pluck('id');

        $project->user()->attach($user , [
            'type' => 'owner'
        ]);

        return redirect()->back();


    }


    public function rename(Request $request){
        $project = Project::where('id' , $request->id)->first();
        $project->name = $request->name;
        $project->save();


        return response()->json([
            "status" => 200
        ]);


    }

    public function ajaxdelete(Request $request){
        $project = Project::where([
            'id' => $request->id,
            'user_id' => Auth::user()->id
        ])->first();

        $project->delete();


        return response()->json([
            "status" => 200
        ]);


    }
}
