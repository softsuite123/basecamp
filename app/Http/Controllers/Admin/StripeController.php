<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Models\StripePlan;
use Illuminate\Http\Request;

class StripeController extends Controller
{
    //


    // payment plans
    public function listplans(){

        // $plan = StripePlan::first();

        // $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));

        // $product  =$stripe->products->retrieve(
        //     $plan->product_code,
        //     []
        // );

        // dd($product);


        return view('admin.stripe_plans.index');

    }


    public function store(Request $request){

        // dd($request->all());

        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));

        $product =  $stripe->products->create([
            'name' => $request->name,
        ]);

        $price = $stripe->prices->create([
            'unit_amount' => ($request->price * 100) ,
            'currency' => 'usd',
            'recurring' => [
                'interval' => 'day',
                'interval_count' => $request->days,
                'trial_period_days' => $request->trial_days,
            ],
            'product' => $product->id,
        ]);


        $plan  = StripePlan::create([
            'name' => $request->name,
            'price' => $request->price,
            'detail' => $request->detail,
            'days' => $request->days,
            'status' => $request->status,
            'product_code' => $product->id,
            'type' => $request->type,
            'price_code' => $price->id,
            'trial_days' => $request->trial_days
        ]);





        return redirect()->back();


        // dd($product , $price);

    }


    public function update(Request $request){


        // dd(intval(Config::trial_days()));
        $plan = StripePlan::where('id', $request->id)->first();

        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));


        $product =  $stripe->products->update( $plan->product_code ,[

            'name' => $request->name,
        ]);

        $price = $stripe->prices->update($plan->price_code ,[
            'active' =>false,
            // // 'unit_amount' => ($request->price * 100) ,
            // // 'currency' => 'usd',
            // 'recurring' => [
            //     // 'interval' => 'day',
            //     // 'interval_count' => $request->days,
            //     'trial_period_days' => $request->trial_days
            // ],

        ]);

        $newprice = $stripe->prices->create([
            'unit_amount' => ($request->price * 100) ,
            'currency' => 'usd',
            'recurring' => [
                'interval' => 'day',
                'interval_count' => $request->days,
                'trial_period_days' => $request->trial_days,
            ],
            'product' => $product->id,
        ]);


        $plan->update($request->except('_token'));
        $plan->price_code = $newprice->id;
        $plan->save();
        return redirect()->back();
    }

}
