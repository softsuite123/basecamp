<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //

    public function index(){
        return view('admin.dashboard');
    }

    public function users(){
        return view('admin.users');
    }



    public function delete_user(Request $request){
        // dd($request->all());
        $user = User::where('id', $request->userid)->first();

        if($user){
            $user->is_deleted = 1;
            $user->save();
            return response()->json([
                "status" => 200,
                "message" => "User deleted successfully"
            ]);
        }

    }

    public function toggle_status_user(Request $request){

        $user = User::where('id', $request->userid)->first();
        // dd($request->all());
        if($user){
            $user->is_disabled = $user->is_disabled == 0 ? 1 : 0 ;
            $user->save();
            if($user->is_disabled == 1){
                $userstatus = "Disabled";
                $btnstatus = "Enable";
            }
            else{
                $userstatus = "Enabled";
                $btnstatus = "Disable";

            }
            return response()->json([
                "status" => 200,
                "message" => "User deleted successfully",
                "userstatus" => $userstatus,
                "btnstatus" => $btnstatus,
            ]);
        }

    }

}
