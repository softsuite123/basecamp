<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentPlan;
use App\Models\StripePlan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;
class PlanController extends Controller
{
    //

    public function index(){
        // return " ok ";
        return view('admin.plans.index');
    }

    public function store(Request $request){
        // dd($request->all());

        $plan  = PaymentPlan::create([
            'name' => $request->name,
            'price' => $request->price,
            'detail' => $request->detail,
            'days' => $request->days,
            'status' => $request->status
        ]);

        return redirect()->back()->with('success', 'Plan Added!');


    }

    public function update(Request $request){
        $plan = PaymentPlan::where('id' , $request->id)->first();
        $plan->update($request->except('_token'));
        $plan->save();
        return redirect()->back();
    }



    public function setupPlan(Request $request){
        return view("firstlogin.setup.plan.index");
    }

    public function updatePlan(Request $request){
        $plan = StripePlan::where('id' , $request->id)->first();
        $user = User::where('id', Auth::user()->id)->first();


        $stripe = \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        if($user->stripe_account == null){
            $customer = \Stripe\Customer::create([
                'name' => $user->name,
                'email' => $user->email,

                'payment_method' => 'pm_card_visa',
                'invoice_settings' => [
                  'default_payment_method' => 'pm_card_visa',
                ],
            ]);
            $user->stripe_account = $customer->id;
        }


        if($user->stripe_subscription == null){
            // add subscription
            $subscription = \Stripe\Subscription::create([
                'customer' => $user->stripe_account,
                'items' => [[
                    'price' => $plan->price_code,
                ]],
                'trial_period_days' => $plan->trial_days

            ]);
            $user->stripe_subscription = $subscription->id;
        }



        $user->payment_plan_id = $plan->id;
        $user->plan_amount = $plan->price;
        $startdate = Carbon::now();

        $user->trial_start = $startdate;
        $user->trial_ends = Carbon::now()->addDays($plan->trial_days);

        $user->payment_status = -1;
        $user->save();


        // dd($subscription);


        // dd($plan);

        // return redirect()->to('/first/setup/payment');
        // $this->setupPaymentSuccess();
        return redirect()->to('/home');


    }

    public function setupPayment(){

        // get plan
        $plan = StripePlan::where('id' , Auth::user()->payment_plan_id)->first();
        // dd($plan);

        // stripe key
        $stripe = \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        // // start session
        // $session = \Stripe\Checkout\Session::create([
        //     'payment_method_types' => ['card'],
        //     'line_items' => [[
        //       'price_data' => [
        //         'currency' => 'usd',
        //         'product_data' => [
        //           'name' => $plan->name,
        //         ],
        //         'unit_amount' => ($plan->price * 100),
        //       ],
        //       'quantity' => 1,
        //     ]],
        //     'mode' => 'payment',
        //     'success_url' => url('/')."/first/setup/payment/success",
        //     'cancel_url' => url('/')."/first/setup/payment/cancel",
        // ]);

        // $session = \Stripe\Checkout\Session::create([

        //     'line_items' => [[
        //       'price' => $plan->price_code,
        //       'quantity' => 1,

        //     ]],
        //     // 'trial_period_days' => $plan->trial_days,
        //     'payment_method_types' => ['card'],
        //     'mode' => 'subscription',
        //     'success_url' => url('/')."/first/setup/payment/success",
        //     'cancel_url' => url('/')."/first/setup/payment/cancel",
        // ]);


        $session = \Stripe\Checkout\Session::create([

            'customer' => Auth::user()->stripe_account,
            'payment_method_types' => ['card'],
            'line_items' => [[
                'price' => $plan->price_code,
                'quantity' => 1,
            ]],
            'mode' => 'subscription',
            'success_url' => url('/')."/first/setup/payment/success",
            'cancel_url' => url('/')."/first/setup/payment/cancel",
        ]);




        return redirect()->to($session->url);

        // dd($session->url, $session);
        // return $response->withHeader('Location', $session->url)->withStatus(303);
        // return view('firstlogin.setup.payment.index');
    }

    public function setupPaymentSuccess(){
        $user = User::where('id' , Auth::user()->id)->first();
        $user->payment_status =1;
        $user->save();
        return redirect()->to('/home');
        // dd('success', $response , $request);
    }

    public function setupPaymentCancel(Request $request , Response $response){
        dd('cancel');

    }

    public function cancelsubscription(Request $request){
        $res = $request->data['object']['customer'];
        $user  = User::where('stripe_account' , $res)->first();
        $user->stripe_subscription = null;
        $user->save();
        // return $res;
        // return response()->json($res);
    }

}
