<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Project;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Mockery\Expectation;

class SpaceController extends Controller
{
    //

    public static function set($space , $id){
        if($space == 'company'){
            $space = Company::where('id' , '=', $id)->first();
            $name = "App\Models\Company";
        }
        if($space == 'team'){
            $space = Team::where('id' , '=', $id)->first();
            $name = "App\Models\Team";

        }
        if($space == 'project'){
            $space = Project::where('id' , '=', $id)->first();
            $name = "App\Models\Team";

        }

        if($space->id){
                session()->put('space', $space);

                return $space;
        }
        else{
            // return redirect()->route('home');
            $space = Company::where('user_id' , Auth::user()->id)->first();
            session()->put('space', $space);

            return $space;
        }

        // return  response([
        //     'space' => $space,
        //     "name"=> $name
        // ]);

    }

    public static function get(){
        // dd(session()->has('space'));
        // dd(session()->get('space'));
        if(session()->has('space')){
            return session()->get('space');
        }
        else{
            // // dd('hit');
            // header('Location: /home');
            // exit();
            $space = Company::where('user_id' , Auth::user()->id)->first();

            self::set('company', $space->id);
            return session()->get('space');


        }
    }



}
