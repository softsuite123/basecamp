<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Invite;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class Firstlogin extends Controller
{
    //

    public function brandsetup(){
        $user = User::where('id' , Auth::user()->id)->first();

        if($user->invite_code){
            $invite = Invite::where('code' , $user->invite_code)->first();
            $company = $invite->space::where('id' , $invite->space_id)->first();
            $company->user()->attach($user,
                [
                    'type' => 'member'
                ]
            );
            $user->first_login = 0;
            $user->save();

            return redirect(URL::to('/'));

        }
        else{

            return view('firstlogin.setup.brand.index');
        }


    }

    public function addbrand(Request $request){
        // dd($request->all());

        $company = Company::create([
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => Auth::user()->id
        ]);


        $user = User::where('id' , Auth::user()->id)->first();

        $user->first_login = 0;

        $user->save();

        $user = User::where("id",Auth::user()->id)->pluck('id');

        $company->user()->attach($user , [
            'type' => 'owner'
        ]);


        return redirect(URL::to('/home'));



    }


}
