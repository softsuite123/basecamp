<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    //



    // update logo

    public function logoupdate(Request $request){
        // dd($request->files());


        request()->validate([
            'companylogoinput' => ['required'],
        ]);
        if ($files = $request->file('companylogoinput')) {

            // Define upload path
            $destinationPath = public_path('/company/logo/'); // upload path

            // Upload Orginal Image
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);

            $insert['image'] = "$profileImage";

            // Save In Database
            // dd('inside');


            $company = Company::where([
                "user_id" => Auth::user()->id,
                "id" => $request->id,
            ])->first();

            $company->logo = "$profileImage";

            $company->save();

        }else{
            dd('hit');
        }


        return response()->json([
            'status' => 200,
            "logo" =>  "$profileImage"
        ]);


    }


    public function create(Request $request){
        // dd($request->all());


        request()->validate([
            'logo' => ['required'],
        ]);
        if ($files = $request->file('logo')) {

            // Define upload path
            $destinationPath = public_path('/company/logo/'); // upload path

            // Upload Orginal Image
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);

            $insert['image'] = "$profileImage";

            // Save In Database
            // dd('inside');


            $newcompany = Company::create([
                "user_id" => Auth::user()->id,
                "logo" => "$profileImage",
                "name" => $request->name,
                "description" => $request->description,
            ]);

            // dd($newcompany->id);

            $user = User::where("id",Auth::user()->id)->pluck('id');

            $newcompany->user()->attach($user , [
                'type' => 'owner'
            ]);
            // $user->companies()->attach($company);

            return redirect()->back();

        }else{
            dd('hit');
        }








    }



    public static function controlpanel(){
        return view('front.controlpanel.company.index');
    }


    public static function showtodos(){
        return view('front.todos.company.index');

    }

    public function invite(){
        return view('front.company.invite.index');

    }

    public function rename(Request $request){
        $team = Company::where('id' , $request->id)->first();
        $team->name = $request->name;
        $team->save();


        return response()->json([
            "status" => 200
        ]);


    }

    public function ajaxdelete(Request $request){
        $team = Company::where([
            'id' => $request->id,
            'user_id' => Auth::user()->id
        ])->first();

        $team->delete();


        return response()->json([
            "status" => 200
        ]);


    }

    public function changecolor(Request $request){
        $company = Company::where('id' , $request->id)->first();
        $company->color = $request->color;
        $company->save();


        return response()->json([
            "status" => 200
        ]);
    }


    public function pinstatus(Request $request){
        $company = Company::where('id' , $request->id)->first();

        // if($request->pin == true){
        //     $pin = 1;
        // }
        // else{
        //     $pin = 0;
        // }

        $pin = $request->pin;

        $user = User::where("id",Auth::user()->id)->pluck('id');

        $company->user()->updateExistingPivot($user , [
            'pin' => $pin
        ]);


        return response()->json([
            "status" => 200
        ]);
    }



}
