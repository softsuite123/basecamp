<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
// use Auth::guard('guardName')

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function Adminloginform(){
        return view('auth.admin.login');
    }

    public function Adminlogin(Request $request){
        // dd($request->all());
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);

        if(Auth::guard('Admin')->attempt($request->only('email', 'password'))){
            // dd(Auth::guard('Admin'));
            return redirect('/admin/dashboard');

        }






        // $admin = Admin::where('email', $request->email)->first();

        // // dd($admin);
        // // dd(Auth::guard('Admin')->attempt(['email' => $email, 'password' => $password]));
        // if (!$admin) {
        //     return redirect($this->loginPath)->with('error', 'Admin');
        // }

        // if (Hash::check($request->password, $admin->password)) {
        //     Auth::guard('Admin')->login($admin);
        //     return redirect('/admin');
        // }

        // dd($check);
    }
}
