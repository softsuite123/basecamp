<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    //

    public function index(){
        return view('front.index');
    }
    public function report(){
        return view('front.report');
    }
    public function reportUser(){
        return view('front.report-user');
    }
    public function reportTodo(){
        return view('front.report-todo');
    }
    public function reportSchedule(){
        return view('front.report-schedule');
    }
    public function profile(){
        return view('front.profile');
    }
    public function notificationEdit(){
        return view('front.notification-edit');
    }
}
