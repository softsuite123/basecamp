<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Document;
use App\Models\File;
use App\Models\Folder;
use App\Models\GoogleDoc;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FileshareController extends Controller
{
    //
    public function index(Request $request){
        SpaceController::set($request->space , $request->id );

        return view('front.fileshare.vault');
    }




    // folder
    // create new
    public function addFolder(Request $request){

        // dd(uniqid());

        // Load Space
        $space = SpaceController::get();


        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);

        $newfolder = [
            "folder_id" => $request->folderid,
            "name"      => $request->name ,
            "user_id"   => Auth::user()->id,
            "space"     => $namespace,
            "space_id"  => $space->id,
            "code"      => uniqid()

        ];

        $folder = Folder::create($newfolder);

        $ajax = view('front.fileshare.partials.folder-template')->with('folder', $folder)->render();

        return response()->json([
            'status' => 200,
            'folder' => $folder,
            'ajax' => $ajax,

        ]);

        // return redirect()->back();

        // dd($request->all());
    }



    public function getfolder(Request $request){


        $folder = Folder::where('code' , $request->code)->with('children')->first();

        // dd($folder);

        return view('front.fileshare.folder-view')->with([
            'folder' => $folder
        ]);

    }

    public function deletefolder(Request $request){

        // dd($request->all());

        $folder = Folder::where('code' , $request->folder_code)->with('children')->first();


        if($folder){
            $folder->delete();
        }

        return response()->json([
            'code' => 200,


        ]);
    }


    // File --------------------------------------------------
    // create File


    public function addFile(Request $request){
        // dd($request->all());



        // Load Space
        $space = SpaceController::get();
        $space->load('user');


        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);


        if(isset($request->selectall)){
            $users = $space->user->pluck('id')->toArray();

        }
        else{
            $users = $request->users;
        }

        // dd($users);



        if($request->folder_id == 0){
            $folder_code = "root";
        }
        else{

            $folder = Folder::where('id' , $request->folder_id)->first();
            $folder_code = $folder->code;
        }

        if($files = $request->file('file')){

            // Define upload path
            $destinationPath = public_path('/vault'); // upload path

            // Upload Orginal Image
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);

            $file = File::create([
                "file_name" => $request->filename,
                "path" => $profileImage,
                "file_code" => uniqid(),
                "space" => $namespace ,
                "space_id" => $space->id,
                "folder_id" => $request->folder_id,
                "folder_code" => $folder_code,
                "user_id" => Auth::user()->id,
            ]);

            $users  = User::whereIn('id', $users )->get();

            foreach($users as $user){
                $user->files()->attach($file->id);
            }

            $ajax = view('front.fileshare.partials.file-template')->with('file', $file)->render();

            $thisuser= Auth::user();


            Activity::setActivity(
                "A new File is Added" ,
                "$thisuser->name added a file . ",
                "/fileshare/file/$file->file_code"
            );


            return response()->json([
                'status' => 200,
                'ajax' => $ajax,
            ]);

        }
    }

    public function deletefile(Request $request){

        // dd($request->all());

        $file = File::where('file_code' , $request->file_code)->first();


        if($file){
            $file->delete();
        }

        return response()->json([
            'code' => 200,


        ]);
    }

    public function getfile(Request $request){

        // dd('hit');


        $file = File::where('file_code' , $request->code)->with('users')->first();

        // dd($file);

        return view('front.fileshare.file-view')->with([
            'file' => $file
        ]);

    }

    public function removeFileUser(Request $request){
        $user  = User::where('id' , $request->userid)->first();
        $user->files()->detach($request->fileid);
        return redirect()->back();
    }

    public function addFileUser(Request $request){
        // Load Space
        $space = SpaceController::get();


        // Get Space namespace
        $namespace = get_class($space);

        $file = File::where('id' , $request->file_id)->first();

        if(isset($request->selectall)){
            $users = $space->user->pluck('id')->toArray();

        }
        else{
            $users = $request->users;
        }
        $users  = User::whereIn('id', $users )->get();

        foreach($users as $user){
            $user->files()->attach($file->id);
        }

        return redirect()->back();
    }

    function getsubfolder(Request $request){
        // Load Space
        $space = SpaceController::get();


        // Get Space namespace
        $namespace = get_class($space);

        $subfolder = Folder::where([
            'folder_id' => $request->folderid,
            'space' => $namespace,
            'space_id' => $space->id,
        ])->get();

        return response()->json(
            [
                "subfolders" => $subfolder
            ]
        );
    }


    public function copyfile(Request $request){
        // dd($request->all());

        $file = File::where("id" , $request->file_id)->first();
        $folder = Folder::where("id" , $request->folder_id)->first();
        // dd($file);
        $newfile = $file->replicate();
        $newfile->folder_id = $folder->id;
        $newfile->folder_code = $folder->code;
        $newfile->save();

        return  response()->json([
            "status" => 200,
            "file" => $newfile,
            "folder" => $folder
        ]);

    }

    public function movefile(Request $request){
        // dd($request->all());


        $file = File::where("id" , $request->file_id)->first();

            $folder = Folder::where("id" , $request->folder_id)->first();



        $file->folder_id = $folder->id;
        $file->folder_code = $folder->code;
        $file->save();

        return  response()->json([
            "status" => 200,
            "file" => $file,

            "folder" => $folder
        ]);

    }


    public function addDocument(Request $request){

        // Load Space
        $space = SpaceController::get();


        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);



        if(isset($request->selectall)){
            $users = $space->user->pluck('id')->toArray();

        }
        else{
            $users = $request->users;
        }


        if($request->folder_id == 0){
            $folder_code = "root";
        }
        else{

            $folder = Folder::where('id' , $request->folder_id)->first();
            $folder_code = $folder->code;
        }

        $document  = Document::create([
            'name' => $request->name,
            'code' => uniqid(),
            'document' => $request->document,
            'space' => $namespace,
            'space_id' => $space->id,
            'folder_code' => $folder_code,
            'folder_id' => $request->folder_id,
            'user_id' => Auth::user()->id,
        ]);

        $users  = User::whereIn('id', $users )->get();

        foreach($users as $user){
            $user->documents()->attach($document->id);
        }

        $ajax = view('front.fileshare.partials.document-template')->with('document', $document)->render();

        $thisuser = Auth::user();
        Activity::setActivity(
            "A New Document is Added" ,
            "$thisuser->name added a Document . ",
            "/fileshare/document/$document->code"
        );

        return response()->json([
            'status' => 200,
            'document' => $document,
            "ajax" => $ajax
        ]);

        // dd($request->all());

    }

    public function getdocument(Request $request){

        $document = Document::where('code' , $request->code)->first();

        return view('front.fileshare.document-view')->with([
            'document' => $document
        ]);

    }

    public function getiframe(Request $request){

        $document = Document::where('code' , $request->code)->first();

        return view('front.fileshare.partials.document-iframe')->with([
            'document' => $document
        ]);

    }

    public function updateDocument(Request $request){

        $document = Document::where('id' ,$request->id)->first();

        $document->name = $request->name;
        $document->document = $request->document;

        $document->save();

        return response()->json([
            "status" => 200,
            "document" => $document
        ]);
        // dd($document);






    }

    public function deleteDocument(Request $request){
        $document = Document::where('code' , $request->code)->first();


        if($document){
            $document->delete();
        }

        return response()->json([
            'code' => 200,


        ]);
    }


    public function copyDocument(Request $request){
        // dd($request->all());

        $file = Document::where("id" , $request->document_id)->first();
        $folder = Folder::where("id" , $request->folder_id)->first();
        // dd($file);
        $newfile = $file->replicate();
        $newfile->folder_id = $folder->id;
        $newfile->folder_code = $folder->code;
        $newfile->save();

        return  response()->json([
            "status" => 200,
            "file" => $newfile,
            "folder" => $folder
        ]);

    }


    public function moveDocument(Request $request){
        // dd($request->all());


        $file = Document::where("id" , $request->document_id)->first();

            $folder = Folder::where("id" , $request->folder_id)->first();



        $file->folder_id = $folder->id;
        $file->folder_code = $folder->code;
        $file->save();

        return  response()->json([
            "status" => 200,
            "file" => $file,

            "folder" => $folder
        ]);

    }


    public function removeDocumentUser(Request $request){
        $user  = User::where('id' , $request->userid)->first();
        $user->documents()->detach($request->documentid);
        return redirect()->back();
    }

    public function addDocumentUser(Request $request){

        // Load Space
        $space = SpaceController::get();


        // Get Space namespace
        $namespace = get_class($space);

        $document = Document::where('id' , $request->document_id)->first();

        if(isset($request->selectall)){
            $users = $space->user->pluck('id')->toArray();

        }
        else{
            $users = $request->users;
        }
        $users  = User::whereIn('id', $users )->get();

        foreach($users as $user){
            $user->documents()->attach($document->id);
        }

        return redirect()->back();
    }



    // google doc

    public function addGoogleDocument(Request $request){
        // dd($request->all());

         // Load Space
         $space = SpaceController::get();


         // Get Space namespace
         $namespace = get_class($space);
         $space_class = class_basename($space);



         if(isset($request->selectall)){
             $users = $space->user->pluck('id')->toArray();

         }
         else{
             $users = $request->users;
         }


         if($request->folder_id == 0){
             $folder_code = "root";
         }
         else{

             $folder = Folder::where('id' , $request->folder_id)->first();
             $folder_code = $folder->code;
         }

         $document  = GoogleDoc::create([
             'name' => $request->name,
             'code' => uniqid(),
             'link' => $request->link,
             'space' => $namespace,
             'space_id' => $space->id,
             'folder_code' => $folder_code,
             'folder_id' => $request->folder_id,
             'user_id' => Auth::user()->id,
         ]);

         $users  = User::whereIn('id', $users )->get();

         foreach($users as $user){
             $user->googledocs()->attach($document->id);
         }

         $ajax = view('front.fileshare.partials.googledoc-template')->with('document', $document)->render();
         $thisuser = Auth::user();
        Activity::setActivity(
            "A New Google Document is Added" ,
            "$thisuser->name added a Google Document . ",
            "/fileshare/document/$document->code"
        );


         return response()->json([
             'status' => 200,
             'document' => $document,
             "ajax" => $ajax
         ]);
    }

    public function getGoogleDoc(Request $request){
        // dd($request->code);

        $document = GoogleDoc::where('code' , $request->code)->first();

        return view('front.fileshare.googledoc-view')->with([
            'document' => $document
        ]);
    }

    public function deleteGoogleDocument(Request $request){

        $document = GoogleDoc::where('code' , $request->code)->first();


        if($document){
            $document->delete();
        }

        return response()->json([
            'code' => 200,


        ]);

    }

    public function copyGoogleDocument(Request $request){
        $file = GoogleDoc::where("id" , $request->document_id)->first();
        $folder = Folder::where("id" , $request->folder_id)->first();
        // dd($file);
        $newfile = $file->replicate();
        $newfile->folder_id = $folder->id;
        $newfile->folder_code = $folder->code;
        $newfile->save();

        return  response()->json([
            "status" => 200,
            "file" => $newfile,
            "folder" => $folder
        ]);
    }


    public function moveGoogleDocument(Request $request){
        $file = GoogleDoc::where("id" , $request->document_id)->first();

        $folder = Folder::where("id" , $request->folder_id)->first();



        $file->folder_id = $folder->id;
        $file->folder_code = $folder->code;
        $file->save();

        return  response()->json([
            "status" => 200,
            "file" => $file,

            "folder" => $folder
        ]);
    }

    public function removeGoogleDocumentUser(Request $request){
        $user  = User::where('id' , $request->userid)->first();
        $user->googledocs()->detach($request->documentid);
        return redirect()->back();
    }

    public function addGoogleDocumentUser(Request $request){
        // Load Space
        $space = SpaceController::get();


        // Get Space namespace
        $namespace = get_class($space);

        $document = GoogleDoc::where('id' , $request->document_id)->first();

        if(isset($request->selectall)){
            $users = $space->user->pluck('id')->toArray();

        }
        else{
            $users = $request->users;
        }
        $users  = User::whereIn('id', $users )->get();

        foreach($users as $user){
            $user->googledocs()->attach($document->id);
        }

        return redirect()->back();
    }



}
