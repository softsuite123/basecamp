<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlanAndPayment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {


        if(Auth::user()->payment_plan_id == null){
            return redirect()->to('/first/setup/plan');
        }

        if(Auth::user()->payment_status == 0){
            return redirect()->to('/first/setup/payment');

        }

        // if(Auth::user()->stripe_subscription == null){
        //     return redirect()->to('/first/setup/plan');


        // }

        if(Auth::user()->payment_status == -1){
            // return redirect()->to('/');
            $today = Carbon::now();
            // dd(Auth::user()->trial_ends <= $today);
            if(Auth::user()->trial_ends <= $today ){
                // return redirect()->to('/first/setup/plan');
                return redirect()->to('/first/setup/payment');

            }

        }

        return $next($request);
    }
}
