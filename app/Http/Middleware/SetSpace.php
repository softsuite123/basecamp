<?php

namespace App\Http\Middleware;

use App\Http\Controllers\SpaceController;
use Closure;
use Illuminate\Http\Request;

class SetSpace
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        // dd($request->all());

        if(isset($_GET['space'])){
            SpaceController::set($_GET['space'] , $_GET['space_id']  );
        }

        // unset($_GET['space'] );
        // unset($request->space_id);
        // $request->request->remove('space');

        return $next($request);
    }
}
