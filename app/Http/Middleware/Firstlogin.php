<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Firstlogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // dd(Auth::user()->first_login);

        if(Auth::user()->is_disabled == 1){
            return redirect()->route('login' , ['warning'=> 'Account is Disabled']);
        }

        if(Auth::user()->first_login == 1){
            return redirect('/first/setup/brand');
        }


        return $next($request);
    }
}
