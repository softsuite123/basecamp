<?php

namespace App\Models\Chat;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [
        'message' ,
        'with',
        'space_id',
        'space'
    ];




    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
