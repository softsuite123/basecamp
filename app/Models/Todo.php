<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'description',
        'space',
        'user_id',
        'space_id',
        'status',
        'todo_list_id',
        'due_date',
        'is_completed'
    ];


    public function User(){
        return $this->belongsToMany(User::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }

    public function Company(){
        return $this->belongsToMany(Company::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }


    // public function TodoList(){
    //     return $this->belongsToMany(TodoList::class)
    //     ->withTimestamps()
    //     ->withPivot(['type' , 'status']);



    // }


    public function TodoList($userid){
        return $this->belongsToMany(TodoList::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }


    public function activities(){
        return $this->hasMany(TodoActivity::class);
    }


}
