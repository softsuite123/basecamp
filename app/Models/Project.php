<?php

namespace App\Models;

use App\Http\Controllers\SelectionController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'description',
        'logo',
        'user_id',
        'company_id'
    ];


    public function user(){
        return $this->belongsToMany(User::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }



    public function company(){
        return $this->belongsTo(Company::class);
    }

    public static function  select($id = null){
        SelectionController::setProject($id);
    }

    public static function current($id = null){
        return SelectionController::getProject();
    }

    public function todoLists(){
        return $this->belongsToMany(TodoList::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }

}
