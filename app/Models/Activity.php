<?php

namespace App\Models;

use App\Http\Controllers\SpaceController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Activity extends Model
{
    use HasFactory;

    protected $fillable =[
        'title',
        'note',
        'user_id',
        'link',
        'space',
        'space_id',

    ];


    public static function setActivity($title , $note , $link = ''){

        $space = SpaceController::get();
        // Get Space namespace
        $namespace = get_class($space);
        $space_class = class_basename($space);
        $lowerspace= strtolower($space_class);



        $activity = self::create([
            "title" => $title,
            "note" => $note,
            'user_id' => Auth::user()->id,
            'space' => $namespace,
            'link' => $link == '' ? "" : "$link?space=$lowerspace&space_id=$space->id",
            'space_id' => $space->id,
        ]);

        $space->load('user');

        foreach($space->user as $user){
            $activity->user()->attach($user->id);
        }


    }

    public function user(){
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function author(){
        return $this->belongsTo(User::class , 'user_id');
    }


    // public function space($model){
    //     // dd($model);/
    //     // return $this->belongsToMany($model->space::class );
    // }



}
