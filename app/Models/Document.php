<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'code',
        'document',
        'space',
        'space_id',
        'user_id',
        'folder_id',
        'folder_code',
        'status'

    ];


    public function folder(){
        return $this->belongsTo(Folder::class);
    }

    public function owner(){
        return $this->belongsTo(User::class);
    }

    public function users(){

        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
