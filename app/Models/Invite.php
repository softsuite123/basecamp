<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'code',
        'email',
        'title',
        'space_id',
        'space',
        'ref_id',
        'personal_note',
        'status'
    ];
}
