<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TodoList extends Model
{
    use HasFactory;
    protected $fillable =[
        'name',
        'description',
        'space',
        'user_id',
    ];


    public function User(){
        return $this->belongsToMany(User::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }

    public function Company(){
        return $this->belongsToMany(Company::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }

    public function Team(){
        return $this->belongsToMany(Team::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }

    public function Project(){
        return $this->belongsToMany(Project::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }


    public function Todo(){
        return $this->belongsToMany(Todo::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }


    public function TodoFilter($id){
        return $this->belongsToMany(Todo::class)->wherePivot('todo_list_id' , $id)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }

}
