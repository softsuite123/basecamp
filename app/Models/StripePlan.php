<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StripePlan extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'detail',
        'price',
        'days',
        'product_code',
        'price_code',
        'status',
        'type',
        'trial_days'
    ];

}
