<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'value',
    ];



    public static function trial_days(){
        return (self::where('name', 'trial_days')->first())->value;
    }
}
