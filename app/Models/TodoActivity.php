<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TodoActivity extends Model
{
    use HasFactory;
    protected $fillable =[
        'note',
        'user_id',
        'todo_id',
        'space',
        'space_id'
    ];


    public function Todo(){
        return $this->belongsTo(Todo::class);
    }

}
