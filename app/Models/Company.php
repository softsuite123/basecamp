<?php

namespace App\Models;

use App\Http\Controllers\SelectionController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'description',
        'logo',
        'user_id',
        'color',
        'pin'
    ];


    public function user(){
        return $this->belongsToMany(User::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }


    public function companies(){
        return $this->hasMany(Company::class);
    }


    public static function  select($id = null){
        SelectionController::setCompany($id);
    }

    public static function current($id = null){
        return SelectionController::getCompany();
    }

    public function todos(){
        return $this->belongsToMany(Todo::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }

    public function todoLists(){
        return $this->belongsToMany(TodoList::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }



}
