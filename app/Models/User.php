<?php

namespace App\Models;

use App\Models\Chat\GroupMessage;
use App\Models\Chat\Message;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $guard = 'User';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'profile',
        'first_login',
        'invite_code',
        'stripe_account',
        'stripe_subscription'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function companies(){
        return $this->belongsToMany(Company::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status' , 'pin']);
    }

    public function activities(){
        return $this->belongsToMany(Activity::class);
    }

    public function teams(){
        return $this->belongsToMany(Team::class);
    }


    public function projects(){
        return $this->BelongsToMany(Project::class);
    }

    public function todos(){
        return $this->belongsToMany(Todo::class)
        ->withTimestamps()
        ->withPivot(['type' , 'status']);
    }


    public function files(){
        return $this->belongsToMany(File::class)->withTimestamps();
    }

    public function documents(){
        return $this->belongsToMany(Document::class)->withTimestamps();
    }

    public function googledocs(){
        return $this->belongsToMany(GoogleDoc::class)->withTimestamps();
    }



    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function group_messages()
    {
        return $this->hasMany(GroupMessage::class);
    }

}
