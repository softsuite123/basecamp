<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class File extends Model
{
    use HasFactory;

    protected $fillable =[
        'file_name',
        'file_code',
        'path',
        'space',
        'space_id',
        'folder_id',
        'folder_code',
        'user_id',
        'status'
    ];

    public function folder(){
        return $this->belongsTo(Folder::class);
    }

    public function owner(){
        return $this->belongsTo(User::class);
    }

    public function users(){

        return $this->belongsToMany(User::class)->withTimestamps();
    }




}
