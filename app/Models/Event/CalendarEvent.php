<?php

namespace App\Models\Event;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalendarEvent extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'description',
        'from',
        'to',
        'user_id',
        'space',
        'space_id',
        'status'

    ];
}
