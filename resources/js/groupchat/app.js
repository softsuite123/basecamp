require('../bootstrap');

// var Vue = require('vue');

// var elixir = require('laravel-elixir');

// require('laravel-elixir-vue-2');

import Vue from 'vue';



Vue.component('chat-messages', require('./components/ChatMessages.vue').default);
Vue.component('chat-form', require('./components/ChatForm.vue').default);



const app = new Vue({
    el: '#app',

    data: {
        messages: [],
        auth: {},
        selected_user : 0
    },

    created() {
        this.listen();
        this.getAuth();
        this.setuser();
        this.getseletedUser();
        this.fetchMessages();
        // this.changechat();



    },

    methods: {
        fetchMessages() {
            // this.getseletedUser();
            var chatwith = this.selected_user;
            axios.get(`/groupchat/messages`).then(response => {
                // console.log(response.data);
                // return;
                this.messages = response.data;
                let lastmessage = this.messages[this.messages.length - 1];
                // console.log(lastmessage);
                if(lastmessage){
                    $('.active_chat').find('p').html(lastmessage.message);
                }
            }).then(function(){
                $('.msg_history').scrollTop( $('.scrollwraper').height() );

                preloader(false);
            });

        },



        changechat(id){


            $('.messaging').attr("selecteduser" , $('active_chat').attr('user_id'));
            preloader(true);

            this.getseletedUser();
            this.selected_user = id;
            this.fetchMessages();
        },

        addMessage(message) {
            // let that= this;
            // console.log(message);
            // return;
            // var thismsg = this.messages;
            // let that = this;
            this.messages.push(message);
            $('.msg_history').scrollTop( $('.scrollwraper').height() );

            var chatwith = this.selected_user;
            $('.active_chat').find('p').html(message.message);

            axios.post(`/groupchat/messages`, message).then(response => {
              console.log(response.data);

            $('.msg_history').scrollTop( $('.scrollwraper').height() );

            });
        }
        ,
        listen() {



            Echo.private('chat').listen('MessageSent', (e) => {
                // this.messages.push({
                //     message: e.message.message,
                //     user: e.user
                // });
                this.fetchMessages();
                // http://192.168.18.72:8000/
                // var audio = new Audio('http://192.168.18.72:8000/assets/sound/message.mp3'); // path to file
                // audio.play();
                $('.msg_history').scrollTop( $('.scrollwraper').height() );

            });
        },

        getAuth() {
            axios.get('/auth/who').then(response => {
                this.auth = response.data.auth;
                // console.log(response.data.auth);
                $('.msg_history').scrollTop( $('.scrollwraper').height() );

            });

        },

        getseletedUser(){
            var chatwith =$('.active_chat').attr("user_id");
            this.selected_user = chatwith;
            console.log(chatwith);
        },

        setuser(){
            var firstuser = $('.chat_list').first();
            firstuser.addClass('active_chat');
            // console.log(firstuser.attr('userid'));
            $('.messaging').attr("selecteduser" , firstuser.attr('user_id'));
        }


    }
});

