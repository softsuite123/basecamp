<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item">
                <a class="nav-item-hold" href="{{url('/')}}/admin/dashboard">
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
                {{-- <div class="triangle"></div> --}}
            </li>
            {{-- <li class="nav-item" data-item="dashboard"><a class="nav-item-hold" href="{{url('/')}}/dashboard"><i class="nav-icon i-Bar-Chart"></i><span class="nav-text">Dashboard</span></a>
                <div class="triangle"></div>
            </li> --}}

            <li class="nav-item" >
                <a class="nav-item-hold" href="{{url('/')}}/admin/users">
                    <i class="nav-icon i-Administrator"></i>
                    <span class="nav-text">Users</span>
                </a>
                {{-- <div class="triangle"></div> --}}
            </li>

            {{-- <li class="nav-item" >
                <a class="nav-item-hold" href="{{url('/')}}/admin/plans">
                    <i class="nav-icon i-Split-Horizontal-2-Window"></i>
                    <span class="nav-text">Plans</span>
                </a>
                <div class="triangle"></div>
            </li> --}}

            <li class="nav-item" >
                <a class="nav-item-hold" href="{{url('/')}}/admin/stripe/plans">
                    <i class="nav-icon i-Split-Horizontal-2-Window"></i>
                    <span class="nav-text">Plans</span>
                </a>
                {{-- <div class="triangle"></div> --}}
            </li>

            </li>
        </ul>
    </div>
    <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <!-- Submenu Dashboards-->

        {{-- <ul class="childNav" data-parent="forms">
            <li class="nav-item"><a href="form.basic.html"><i class="nav-icon i-File-Clipboard-Text--Image"></i><span class="item-name">Basic Elements</span></a></li>
            <li class="nav-item"><a href="form.layouts.html"><i class="nav-icon i-Split-Vertical"></i><span class="item-name">Form Layouts</span></a></li>
            <li class="nav-item"><a href="form.input.group.html"><i class="nav-icon i-Receipt-4"></i><span class="item-name">Input Groups</span></a></li>
            <li class="nav-item"><a href="form.validation.html"><i class="nav-icon i-Close-Window"></i><span class="item-name">Form Validation</span></a></li>
            <li class="nav-item"><a href="smart.wizard.html"><i class="nav-icon i-Width-Window"></i><span class="item-name">Smart Wizard</span></a></li>
            <li class="nav-item"><a href="tag.input.html"><i class="nav-icon i-Tag-2"></i><span class="item-name">Tag Input</span></a></li>
            <li class="nav-item"><a href="editor.html"><i class="nav-icon i-Pen-2"></i><span class="item-name">Rich Editor</span></a></li>
        </ul> --}}

    </div>
    <div class="sidebar-overlay"></div>
</div>
<!-- =============== Left side End ================-->
