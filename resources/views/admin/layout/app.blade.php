<!DOCTYPE html>
<html lang="en" dir="">


<!-- Mirrored from demos.ui-lib.com/gull/html/layout1/dashboard4.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Dec 2020 09:02:19 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Dashboard</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <link href="{{url('/')}}/admin-assets/css/themes/lite-purple.min.css" rel="stylesheet" />
    <link href="{{url('/')}}/admin-assets/css/plugins/perfect-scrollbar.min.css" rel="stylesheet" />
    @yield('styles')
</head>

<body class="text-left">
    <div class="app-admin-wrap layout-sidebar-large">

        @include('admin.layout.partials.header')
        @include('admin.layout.partials.sidebar')

        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            @yield('content')
        </div>




    </div>

    <!-- ============ Search UI Start ============= -->

    <!-- ============ Search UI End ============= -->


    <script src="{{url('/')}}/admin-assets/js/plugins/jquery-3.3.1.min.js"></script>
    <script src="{{url('/')}}/admin-assets/js/plugins/bootstrap.bundle.min.js"></script>
    <script src="{{url('/')}}/admin-assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="{{url('/')}}/admin-assets/js/scripts/script.min.js"></script>
    <script src="{{url('/')}}/admin-assets/js/scripts/sidebar.large.script.min.js"></script>


    <script src="{{url('/')}}/admin-assets/js/plugins/spin.min.js"></script>
    <script src="{{url('/')}}/admin-assets/js/plugins/ladda.min.js"></script>
    <script src="{{url('/')}}/admin-assets/js/scripts/ladda.script.min.js"></script>

    {{-- <script src="../../dist-assets/js/plugins/sweetalert2.min.js"></script> --}}

    @yield('scripts')
</body>


<!-- Mirrored from demos.ui-lib.com/gull/html/layout1/dashboard4.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Dec 2020 09:02:27 GMT -->
</html>
