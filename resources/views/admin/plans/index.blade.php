


@extends('admin.layout.app')

@section('styles')


    <link rel="stylesheet" href="{{url('/')}}/admin-assets/css/plugins/sweetalert2.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/admin-assets/css/plugins/ladda-themeless.min.css" />


@endsection

@section('content')





    <div class="main-content">
        <div class="breadcrumb">
            <h1 class="mr-2">Plans</h1>
            <ul>
                <li><a href="#">Plans</a></li>
                <li></li>
            </ul>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addnewplan" > Add New Plan</button>
        </div>
        <div class="separator-breadcrumb border-top"></div>

        <!-- end of row-->
        <!-- end of main-content -->

        <div class="row" id="plans">


            @foreach (App\Models\PaymentPlan::get() as $plan )
                <div class="col-md-4 plancard ">

                    <div class="card mb-4">
                        <div class="card-header">
                            <span style="font-size:25px;">{{$plan->name}}</span>

                            <span class="badge badge-pill badge-outline-{{$plan->status == 1 ? 'success' : 'danger'}} p-2 m-1">{{$plan->status == 1 ? 'Enabled' : 'disabled'}}</span>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{$plan->price}}</h5>
                            <p class="card-text">{{$plan->detail}}</p>
                            <div class="row">
                                {{-- <a class="btn btn-outline-primary btn-rounded m-1" href="{{url('/')}}/admin/plans/toggle/plan">Disable</a> --}}
                                <button class="btn btn-outline-primary btn-rounded m-1" data-toggle="modal" data-target="#Editplan{{$plan->id}}"  >Edit</button>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal fade" id="Editplan{{$plan->id}}" tabindex="-1" role="dialog" aria-labelledby="addnewplan-2" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterTitle-2">Edit Plan</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                            <form method="POST" action="{{url('/')}}/admin/plans/update/{{$plan->id}}">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                      <label for="name">Plan Name</label>
                                      <input type="text" name="name" required class="form-control" placeholder="Plan Name" value="{{$plan->name}}" >

                                    </div>

                                    <div class="form-group">
                                        <label for="name">Plan price</label>
                                        <input type="number" name="price"  class="form-control" placeholder="Plan price" value="{{$plan->price}}" required>

                                    </div>

                                    <div class="form-group">
                                        <label for="name">Plan days</label>
                                        <input type="number" name="days"  class="form-control" placeholder="Plan days" value="{{$plan->days}}" required>

                                    </div>

                                    <div class="form-group">
                                        <label for="name">Plan Status</label>
                                        <select name="status" class="form-control" >
                                            <option {{$plan->status == 1 ? 'selected' : ''}} value="1">Active</option>
                                            <option {{$plan->status == 0 ? 'selected' : ''}} value="0">Disable</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="name">Plan Detail</label>
                                        {{-- <input type="text" name="name"  class="form-control" placeholder="Plan Name" > --}}
                                        <textarea name="detail" class="form-control"  cols="30" rows="10" placeholder="Detail">{{$plan->detail}}</textarea>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                    <button class="btn btn-primary ml-2" type="submit">Update Plan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            @endforeach


        </div>
    </div>



    {{-- modals --}}


    <div class="modal fade" id="addnewplan" tabindex="-1" role="dialog" aria-labelledby="addnewplan-2" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle-2">Add Plan</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <form method="POST" action="{{url('/')}}/admin/plans/new">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                          <label for="name">Plan Name</label>
                          <input type="text" name="name" required class="form-control" placeholder="Plan Name" >

                        </div>

                        <div class="form-group">
                            <label for="name">Plan price</label>
                            <input type="number" name="price"  class="form-control" placeholder="Plan price" required>

                        </div>
                        <div class="form-group">
                            <label for="name">Plan days</label>
                            <input type="number" name="days"  class="form-control" placeholder="Plan days"  required>

                        </div>

                        <div class="form-group">
                            <label for="name">Plan Status</label>
                            <select name="status" class="form-control" >
                                <option value="1">Active</option>
                                <option value="0">Disable</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="name">Plan Detail</label>
                            {{-- <input type="text" name="name"  class="form-control" placeholder="Plan Name" > --}}
                            <textarea name="detail" class="form-control"  cols="30" rows="10" placeholder="Detail"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary ml-2" type="submit">Save Plan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection

@section('scripts')

<script src="{{url('/')}}/admin-assets/js/plugins/sweetalert2.min.js"></script>


<script>
    $(document).ready(function(){
        $("#search-input").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#plans .plancard").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>


@endsection
