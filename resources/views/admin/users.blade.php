

@extends('admin.layout.app')

@section('styles')


    <link rel="stylesheet" href="{{url('/')}}/admin-assets/css/plugins/sweetalert2.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/admin-assets/css/plugins/ladda-themeless.min.css" />


@endsection

@section('content')




        <div class="main-content">
            <div class="breadcrumb">
                <h1 class="mr-2">Users</h1>
                <ul>
                    <li><a href="#">Users</a></li>
                    <li></li>
                </ul>
            </div>
            <div class="separator-breadcrumb border-top"></div>

            <!-- end of row-->
            <!-- end of main-content -->

            <div class="row" id="users">
                @foreach (App\Models\User::where('is_deleted', 0)->get() as $user )

                    <div class="col-md-4 user-block usercard" >
                        <div class="card card-profile-1 mb-4">
                            <div class="card-body text-center">
                                <div class="avatar box-shadow-2 mb-3"><img src="{{url('/')}}/admin-assets/images/faces/16.jpg" alt="" /></div>
                                <h5 class="m-0">{{$user->name}}</h5>
                                <p class="mt-0">{{$user->email}}</p>
                                <p>Plan : 0000</p>
                                {{-- <button class="btn btn-primary btn-rounded">Contact Jassica</button> --}}
                                <div class="row" style="width: fit-content; margin: 0 auto ;">

                                    <button class="btn  btn-primary ladda-button basic-ladda-button m-1 confirm-disable" user_id="{{$user->id}}" data-style="expand-right">
                                        <span class="ladda-label">{{$user->is_disabled == 0 ? 'Disable' : 'Enable'}}</span>
                                    </button>

                                    <button class="btn  btn-danger ladda-button basic-ladda-button m-1 confirm-del" user_id="{{$user->id}}" data-style="expand-right">
                                        <span class="ladda-label">Delete</span>
                                    </button>



                                </div>
                                <div class="card-socials-simple mt-4 disable-status" >{{$user->is_disabled == 1 ? 'Disabled' : 'Enabled'}}</div>
                            </div>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>






@endsection

@section('scripts')

<script src="{{url('/')}}/admin-assets/js/plugins/sweetalert2.min.js"></script>


<script>
    $('.confirm-del').on('click', function () {
        var userid = $(this).attr('user_id');
        var user_block = $(this).parents('.user-block');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success mr-5',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
            }).then(function () {

                $.ajax({
                    type: "POST",
                    url: `{{url('/')}}/admin/user/delete`,
                    data: {
                        userid : userid,
                        _token : "{{csrf_token()}}"
                    },
                    success: function (res){
                        console.log(res);
                        if(res.status == 200){
                            swal('Deleted!', 'User has been deleted.', 'success');
                            user_block.remove();

                        }
                    }

                });

            },
            function (dismiss) {
                // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                if (dismiss === 'cancel') {
                    swal('Cancelled', 'User is safe :)', 'error');
                }
            }
            );
    });


    $('.confirm-disable').on('click', function () {
        var userid = $(this).attr('user_id');
        var user_block = $(this).parents('.user-block');
        var btn = $(this);
        swal({
            title: 'Are you sure?',
            text: " ",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success mr-5',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
            }).then(function () {

                $.ajax({
                    type: "POST",
                    url: `{{url('/')}}/admin/user/disablestatus`,
                    data: {
                        userid : userid,
                        _token : "{{csrf_token()}}"
                    },
                    success: function (res){
                        console.log(res);
                        if(res.status == 200){
                            swal('Updated!', `User has been ${res.userstatus} .`, 'success');
                            user_block.find('.disable-status').html(res.userstatus);
                            btn.find('span').html(res.btnstatus);

                        }
                    }

                });

            },
            function (dismiss) {
                // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                if (dismiss === 'cancel') {
                    swal('Cancelled', 'User Status Retained', 'error');
                }
            }
            );
    });
</script>

<script>
    $(document).ready(function(){
        $("#search-input").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#users .usercard").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>


@endsection
