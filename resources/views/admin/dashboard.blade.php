

@extends('admin.layout.app')



@section('content')



        <div class="main-content">
            <div class="breadcrumb">
                <h1 class="mr-2"></h1>
                <ul>
                    <li><a href="#">Dashboard</a></li>
                    <li></li>
                </ul>
            </div>
            <div class="separator-breadcrumb border-top"></div>
            <div class="row mb-4">
                <div class="col-md-3 col-lg-3">
                    <div class="card mb-4 o-hidden">
                        <div class="card-body ul-card__widget-chart">
                            <div class="ul-widget__chart-info">
                                <h5 class="heading">Users</h5>
                                <div class="ul-widget__chart-number">
                                    <h2 class="t-font-boldest">{{App\Models\User::where('is_disabled' , 0)->count()}}</h2>
                                    <small class="text-muted">{{App\Models\User::where('is_disabled' , 1)->count()}}  Disabled User</small>
                                </div>
                            </div>
                            <div id="basicArea-chart"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3">
                    <div class="card mb-4 o-hidden">
                        <div class="card-body ul-card__widget-chart">
                            <div class="ul-widget__chart-info">
                                <h5 class="heading">Companies</h5>
                                <div class="ul-widget__chart-number">
                                    <h2 class="t-font-boldest">{{App\Models\Company::count()}}</h2>
                                    {{-- <small class="text-muted">46% compared to last year</small> --}}
                                </div>
                            </div>
                            <div id="basicArea-chart2"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3">
                    <div class="card mb-4 o-hidden">
                        <div class="card-body ul-card__widget-chart">
                            <div class="ul-widget__chart-info">
                                <h5 class="heading">Teams</h5>
                                <div class="ul-widget__chart-number">
                                    <h2 class="t-font-boldest">{{App\Models\Team::count()}}</h2>
                                    {{-- <small class="text-muted">46% compared to last year</small> --}}
                                </div>
                            </div>
                            <div id="basicArea-chart3"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3">
                    <div class="card mb-4 o-hidden">
                        <div class="card-body ul-card__widget-chart">
                            <div class="ul-widget__chart-info">
                                <h5 class="heading">Projects</h5>
                                <div class="ul-widget__chart-number">
                                    <h2 class="t-font-boldest">{{App\Models\Project::count()}}</h2>
                                    {{-- <small class="text-muted">46% compared to last year</small> --}}
                                </div>
                            </div>
                            <div id="basicArea-chart4"></div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- end of row-->
            <!-- end of main-content -->
        </div>





@endsection



@section('scripts')

<script src="{{url('/')}}/admin-assets/js/plugins/echarts.min.js"></script>
<script src="{{url('/')}}/admin-assets/js/scripts/echart.options.min.js"></script>
{{-- <script src="{{url('/')}}/admin-assets/js/plugins/datatables.min.js"></script> --}}
<script src="{{url('/')}}/admin-assets/js/scripts/dashboard.v4.script.min.js"></script>
<script src="{{url('/')}}/admin-assets/js/scripts/widgets-statistics.min.js"></script>
<script src="{{url('/')}}/admin-assets/js/plugins/apexcharts.min.js"></script>
<script src="{{url('/')}}/admin-assets/js/scripts/apexSparklineChart.script.min.js"></script>

@endsection
