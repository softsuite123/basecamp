@extends('firstlogin.layout.app')


<style>
    .card{
        width: 100% !important;
        border-radius: 12px !important; overflow:hidden;
        box-shadow: 1px 3px 9px 6px #00000025;

    }
    .card:hover{
        /* box-shadow: 1px 10px 10px black; */
        box-shadow: 1px 3px 9px 6px #0000004d;
    }
    .price{
        font-size:50px;
        text-align: center;
    }

    .heading{
        font-size:50px;
        text-align: center;
        margin-bottom: 15px;
    }
    .description{
        font-size:18px;
        color: gray;
        text-align: center;
        margin-top:30px;
    }
    .selectbtn{
        font-size:12px !important;
        margin: 0px auto;
        display:block !important;
        margin-top: 20px;
    }
</style>

@section('content')

{{-- {{dd(App\Models\Config::trial_days())}} --}}

<div class="container" style="    position: relative;top: 40%;transform: translateY(-50%);" >
    <div class="heading">
        Select Plan
    </div>
    <div class="row justify-content-center align-content-center">

        @foreach (App\Models\StripePlan::where('status' , 1)->get() as $plan )
            <div class="col-md-4">

                <div class="card" style="width:100% !important; ">
                    <div class="card-header" style="text-align:center ; font-size:22px; background-color: white;">
                        {{$plan->name}}
                    </div>

                    <div class="card-body">
                        <div class="price">
                            $ {{$plan->price}}
                        </div>
                        <div class="description" style="">
                            {{$plan->trial_days}} day Trial
                        </div>
                        <br>
                        <div class="description" style="min-height: 200px;">
                            {{$plan->detail}}
                        </div>
                        <a href="{{url('/')}}/first/setup/plan/{{$plan->id}}">

                            <button  type="button" class=" btn btn-success selectbtn">
                                Select Plan
                            </button>
                        </a>
                    </div>
                </div>
            </div>

        @endforeach








    </div>
</div>

{{-- <script src=""></script> --}}


@endsection
