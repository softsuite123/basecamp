@extends('firstlogin.layout.app')
@section('content')

<style>
    .btn--add-icon {
        background-image: url({{asset('assets/images/plus-6855215a0ee7305a315874c1a0a5b6cbed1a6950911201a79b03703d36f41ce7.svg')}});
    }

    .btn--overflow-icon {
        background-image: url({{asset('assets/images/overflow-51cd8be896d0981b46a99452c79d3cc3d447a5e8a883b55ad3ceb1080277c6e6.svg')}}

    );
    background-size: 60%;
    }

    .btn--overflow-icon-light {
        background-image: url({{asset('assets/images/overflow--light-082d0498a998dbf4bd46efd039dda17b20385ac427e758db3795b58da34d1f95.svg')}}

    );
    background-size: 50%;
    margin-top: -9px;
    }

    .btn--close-icon {
        background-image: url({{asset('assets/images/close-9965840adaf568bddbeb5283b93d137a7f7d1e051d1e2f0a539049a096e641d8.svg')}}

    );
    }

    .options-menu__action--pin {
        background-image: url({{asset('assets/images/pin-d00b09a9e192a7b25ba69b147be84d7f1dc76db202c15a87d65335d5bd8bf65f.svg')}}

    );
    }

    .options-menu__action--rename {
        background-image: url({{asset('assets/images/edit-30b083660fb108fc03ea808487594e0d19d0323c86b4765fb342ee16bdbfa2ad.svg')}}

    );
    }

    .options-menu__action--archive {
        background-image: url({{asset('assets/images/archive-4410379510d42bc58a03c0c4d9ad7850cc240d7e8aeca894f021cac56fe556a2.svg')}}

    );
    }



</style>

<style>
    .boxbox {
        background-color: rgb(255, 255, 255);
        border-radius: 9px;
        margin: 0 auto;
        position: absolute;
        top: 40%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 50%;
        /* height: 20px; */

    }
    .title{
        font-size: 32px;
        text-align: center;
        margin-bottom: 20px;
    }
    @media (max-width:700px){
        .boxbox{
            width: 90%;
        }
    }

</style>
<main id="main-content" class="loading__hide  u-hide-focus" data-appearing-on="" data-bucket-url="" data-bucket-id=""
    tabindex="-1">




    <div class="boxbox p-5" style="">
        <div class="title">
            Create Your First HQ
        </div>
        <form id="companyform" action="{{url('/')}}/first/setup/brand/add" method="POST"  >
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="form-group">
                <label for="">Company Name</label>
                <input type="text" name="name" id="" class="form-control inp" placeholder="Company Name"  required="true" >

            </div>
            <div class="form-group">
                <label for="">Description </label>
                <textarea name="description" id="" rows="4" class="form-control inp"></textarea>

            </div>

            <div class="form-group" style="margin-top:50px;">
                <button type="button" class="btn btn-success validate"  style="float: right;" > SAVE </button>
            </div>


            <div class="form-group" style="margin-top:50px; ">
                <button type="button" class="btn btn-default preload"  style="float: right; display:none;" >
                    <img src="{{url('/')}}/assets/images/preload1.gif" style="height:30px;width:auto;" alt="">
                </button>
            </div>





        </form>





    </div>



</main>

<script src="{{url('/')}}/assets/js/formvalidation.js"></script>

<script>


    $(document).ready(function() {

        $('.validate').click(function(e){
            // e.preventDefault();
            $(this).hide();
            $('.preload').show();
            // return;

            var validation = validateform('#companyform');
            console.log(validation);
            if(validation.errors < 1){
                $('#companyform').submit();
            }
            else{
                $(this).show();
                $('.preload').hide();
            }


        });
    });




</script>

@endsection
