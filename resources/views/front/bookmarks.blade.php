@extends('front.Master.app')
@section('content')
    <div class="container">
        <div class="box m-5">
            <div class="panel manage-panel panel--perma panel--perma-person push_quarter--top p-5">
                <article>
                    <header class="perma__header push--bottom text-center">
                        <img   class="avatar img--sized app-mobile__hide" data-turbolinks-permanent="true" data-current-person-avatar="true" id="my_assignments_avatar" src="{{asset('assets/images/blank-avatar.png')}}" width="64" height="64" title="name" alt="">
                        <h1 class="perma__title push_quarter--top flush--bottom app-mobile__hide">You haven’t bookmarked anything yet</h1>
                    </header>
                    <p class="push_half--top">Click the bookmark icon on any to-do list, doc, message, etc. in Teams Leader and it’ll show up here for easy reference.</p>
                    <img style="width: 100%" class="img--bordered" src="https://bc3-production-assets-cdn.basecamp-static.com/assets/blank_slates/how-to-bookmark-a0ce43d22497460d89fe6b9020095cf084a34178bad45ef8ea6abc013c1a26e7.gif">
                </article>
            </div>
        </div>
    </div>
@endsection
