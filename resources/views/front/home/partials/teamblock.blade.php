<section class="project-index__section project-index__section--teams hide-from-clients teamsection" style="margin-bottom:20px;"
    data-role="project_group content_filter_group" data-projects-display="card" role="region" aria-label="My Teams">
    <header class="centered">
        <h3 class="project-index__header break break--on-background push--top push_half--bottom">
            <span>Teams</span>
        </h3>
        <aside class="project-index__toolbar project-index__toolbar--new hide-from-clients" role="presentation"
            data-behavior="hide_when_content_filter_active">
            <span class="options-menu options-menu--add-project" data-purpose="team"
                data-behavior="expandable render_new_project_form_on_expand reveal_on_expand">
                <button name="button" type="button" title="Start a new team…"
                    class="options-menu__expansion-toggle btn btn--small btn--with-icon btn--add-icon"
                    data-toggle="modal" data-target="#addnewteam"
                    data-behavior="toggle_expansion_on_click">New</button>

            </span>
        </aside>
        <aside class="project-index__toolbar" data-behavior="hide_when_content_filter_active" role="presentation">
            <span class="options-menu" data-behavior="expandable">
                <button name="button" type="button" title="Show options…"
                    class="options-menu__expansion-toggle btn btn--small btn--icon btn--overflow-icon"
                    data-behavior="toggle_expansion_on_click">File…</button>
                <div class="options-menu__content expanded_content" data-behavior="collapse_on_clickoutside">
                    <a class="options-menu__action options-menu__action--selected" data-remote="true" rel="nofollow"
                        data-method="put"
                        href="/4987109/home/projects_display_settings?projects_display_setting%5Bsection%5D=team&amp;projects_display_setting%5Bvalue%5D=card">Show
                        teams as cards</a>
                    <a class="options-menu__action " data-remote="true" rel="nofollow" data-method="put"
                        href="/4987109/home/projects_display_settings?projects_display_setting%5Bsection%5D=team&amp;projects_display_setting%5Bvalue%5D=list">Show
                        a list of teams</a>
                </div>
            </span>
        </aside>
    </header>
    <div class="project-index__blank-slate push_half--top push_double--bottom"
        data-behavior="hide_when_content_filter_active">
        <p class="txt--medium txt--subtle" style="text-align:center;">A place for groups within your company or organization to share
            ideas, ask questions, and make announcements.</p>
    </div>

    <div class="row">



        @php

            $teams = (App\Models\User::where('id' , Auth::user()->id)->with('teams')->first())->teams;
            $teamCount = count($teams);
        @endphp
        @forelse ($teams as $team )



        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="card team-collection-card" teamid="{{$team->id}}" style="height:auto; width:100% !important ;">

                <div  class="view ">
                    @owner($team)
                        <div class="editor-toggle">
                            <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                        </div>

                    @endowner
                    <a href="{{url('/')}}/team/controlpanel?team={{$team->id}}" class="team-name team_name"> {{ $team->name }} </a>
                    <div class="team-description">
                        {{$team->description}}
                    </div>
                    <div class="users-in-team">

                        {{-- <div class="user owner" >
                            @if(!Auth::user()->logo)
                                <div class="profile-img">
                                    <img src="http://127.0.0.1:8000/assets/images/avatar-male.png" class="profile" alt="">
                                </div>
                            @else
                                <div class="profile-placeholder" title="{{Auth::user()->name}} | {{Auth::user()->email}}">
                                    <p>{{ substr(Auth::user()->name , 0 , 2) }}</p>
                                </div>
                            @endif
                        </div> --}}


                        @foreach ($team->user as $member )

                            <div class="user owner" >
                                @if($member->logo)
                                    <div class="profile-img">
                                        <img src="http://127.0.0.1:8000/assets/images/avatar-male.png" class="profile" alt="">
                                    </div>
                                @else
                                    <div class="profile-placeholder" title="{{$member->name}} | {{$member->email}}">
                                        <p>{{ substr($member->name , 0 , 2) }}</p>
                                    </div>
                                @endif
                            </div>
                        @endforeach



                        @if ($teamCount > 3)

                            <div class="user extra" >

                                <div class="profile-placeholder" style="background-color:gray; color:white;">
                                    <p>+{{$teamCount}}</p>
                                </div>

                            </div>
                        @endif

                    </div>

                </div>
                {{-- view ends --}}

                <div class="menu">
                    <div class="editor-close">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </div>

                    <div class="name team_name">{{$team->name}}</div>

                    <ul>
                        <li> <i class="fa fa-thumb-tack"></i> Pin This Team  </li>
                        <li class="rename"> <i class="fa fa-pencil  "></i> Rename  </li>
                        <li class="teamdelete" teamid="{{$team->id}}"> <i class="fa fa-trash"></i> Archive or Delete  </li>

                    </ul>
                </div>
                {{-- menu ends --}}


                <div class="editor" >
                    <div class="form-group">
                      <label for="">Rename</label>
                      <input type="text" name="name" id="" teamid="{{$team->id}}" class="form-control renameinput" placeholder="" value="{{$team->name}}" >

                    </div>
                    <div class="btngroup">
                        <div class="btn btn-default renamecancel" style="font-size:12px;"> Cancel </div>
                        <div class="btn btn-success renamesave" style="font-size:12px;"> Save </div>
                    </div>
                </div>

                {{-- editor ends --}}


            </div>


        </div>

        @empty

        <p style="text-align:center; width:100%;" >There Are No Teams</p>

        @endforelse







    </div>






{{-- add new Team  --}}

    <div class="modal fade" id="addnewteam" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add new Team</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>

            </div>
                <form action="{{url('/')}}/team/add" method="post" enctype="multipart/form-data" >
                    @csrf
                    <input type="hidden" name="company_id" value="{{App\Models\Company::current()->id}}">
                    <div class="modal-body">

                        <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" id="" class="form-control" placeholder="Name" required >

                        </div>

                        <div class="form-group">
                            <label for="">Description</label>
                            <input type="text" name="description" id="" class="form-control" required placeholder="Description" >

                        </div>

                        <div class="form-group">
                            <label for="">Team Logo</label>
                            <input type="file" name="logo" id="" class="form-control newteam-logoinput"  placeholder="Company Logo" >

                        </div>

                        <div class="newteam-logopreview">
                            <img src="" alt="" style="
                                height: 100px;
                                width: auto;"
                            >
                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary addnewcompany">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



</section>





<script>

    $baseurl = "{{url('/')}}";
    $token  = "{{csrf_token()}}";

    $(document).ready(function(){
        // newteam logo preview

        $('.newteam-logoinput').on('change' , function(){
            //console.log('hit');

            var fileinput = $('.newteam-logoinput');


            var file = $(fileinput)[0].files[0];
            var reader = new FileReader();


            reader.readAsDataURL(file);

            reader.onload = function (e) {

                $('.newteam-logopreview').find('img').attr('src', e.target.result);

            }
            // $('.logopreviewbox').show();
            // $('.uploadbox').hide();


        });

    });


</script>

<script src="{{url('/')}}/assets/js/home/teamblock.js"> </script>

