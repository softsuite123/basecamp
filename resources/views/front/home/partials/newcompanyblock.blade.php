<section class="project-index__section companysection" data-role="project_group content_filter_group">
    {{-- {{dd(App\Models\User::where('id' , Auth::user()->id)->with('companies')->first()) }} --}}
    {{-- {{dd((App\Models\User::where('id' , Auth::user()->id)->with('companies')->get()))}} --}}

    <header class="centered">
        <h3 class="project-index__header break break--on-background push--top push_half--bottom">
            <span>Company</span>
        </h3>
        <aside class="project-index__toolbar project-index__toolbar--new hide-from-clients"
            role="presentation" data-behavior="hide_when_content_filter_active">
            <span class="options-menu options-menu--add-project" data-purpose="team"
                data-behavior="expandable render_new_project_form_on_expand reveal_on_expand">
                {{-- <button name="button" type="button" title="Start a new Company"
                    data-toggle="modal" data-target="#addnewcompany"
                    class="options-menu__expansion-toggle btn btn--small btn--with-icon btn--add-icon companymodalbtn "
                    data-behavior="toggle_expansion_on_click">New</button> --}}

            </span>
        </aside>
        <aside class="project-index__toolbar" data-behavior="hide_when_content_filter_active"
            role="presentation">
            <span class="options-menu" data-behavior="expandable">
                <button name="button" type="button" title="All Companies"
                data-toggle="modal" data-target="#allcompanies"
                    class="options-menu__expansion-toggle btn btn--small btn--icon btn--overflow-icon"
                    data-behavior="toggle_expansion_on_click">File…</button>

            </span>
        </aside>
    </header>
    <div class="card-grid--projects">


        <div class="row">



            @php

                $companies = (App\Models\User::where('id' , Auth::user()->id)->with('companies')->first())->companies;
                $companyCount = count($companies);
                // $companyCount = count($companies);
            @endphp


            @forelse ($companies as $company )

            {{-- {{dd($company)}} --}}

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card company-collection-card" companyid="{{$company->id}}" style="height:auto; width:100% !important ;">

                    <div  class="view " style="background-color: {{$company->color}};">
                        @owner($company)
                            <div class="editor-toggle">
                                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                            </div>

                        @endowner



                            <div class="pin-icon" style="display: {{$company->pivot->pin == 1 ? 'block' : 'none'}};">
                                <img src="{{url('/')}}/assets/images/pin-icon.png" alt="">
                            </div>

                        <a href="{{url('/')}}/company/controlpanel?company={{$company->id}}" class="company_name company"> {{ $company->name }} </a>
                        <div class="company-description">
                            {{$company->description}}
                        </div>
                        <div class="users-in-company">

                            {{-- <div class="user owner" >
                                @if(!Auth::user()->logo)
                                    <div class="profile-img">
                                        <img src="http://127.0.0.1:8000/assets/images/avatar-male.png" class="profile" alt="">
                                    </div>
                                @else
                                    <div class="profile-placeholder" title="{{Auth::user()->name}} | {{Auth::user()->email}}">
                                        <p>{{ substr(Auth::user()->name , 0 , 2) }}</p>
                                    </div>
                                @endif
                            </div> --}}


                            @foreach ($company->user->slice(0, 3) as $member )

                                <div class="user owner" >
                                    @if($member->logo)
                                        <div class="profile-img">
                                            <img src="http://127.0.0.1:8000/assets/images/avatar-male.png" class="profile" alt="">
                                        </div>
                                    @else
                                        <div class="profile-placeholder" title="{{$member->name}} | {{$member->email}}">
                                            <p>{{ substr($member->name , 0 , 2) }}</p>
                                        </div>
                                    @endif
                                </div>
                            @endforeach



                            @if (count($company->user) > 3)

                                <div class="user extra" >

                                    <div class="profile-placeholder" style="background-color:gray; color:white;">
                                        <p>+{{ (count($company->user)) -3}}</p>
                                    </div>

                                </div>
                            @endif

                        </div>

                    </div>
                    {{-- view ends --}}

                    <div class="menu" data-simplebar >
                        <div class="editor-close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </div>



                        <div class="name company_name">{{$company->name}}</div>

                        <ul>
                            <li class="pin" pin="{{$company->pivot->pin == 1 ? 1 : 0}}"> <i class="fa fa-thumb-tack"></i> Pin This Company  </li>
                            <li style="display:flex;"> <i class="fa fa-crosshairs" style="padding-top:7px; "></i>
                                <div class="pl-2"> Color</div> <input type="color" class="colorpicker">
                            </li>
                            <li class="rename"> <i class="fa fa-pencil  "></i> Rename  </li>
                            <li class="companydelete" companyid="{{$company->id}}"> <i class="fa fa-trash"></i> Archive or Delete  </li>

                        </ul>


                    </div>
                    {{-- menu ends --}}


                    <div class="editor" >
                        <div class="form-group">
                          <label for="">Rename</label>
                          <input type="text" name="name" id="" companyid="{{$company->id}}" class="form-control renameinput" placeholder="" value="{{$company->name}}" >

                        </div>
                        <div class="btngroup">
                            <div class="btn btn-default renamecancel" style="font-size:12px;"> Cancel </div>
                            <div class="btn btn-success renamesave" style="font-size:12px;"> Save </div>
                        </div>
                    </div>

                    {{-- editor ends --}}


                </div>


            </div>

            @empty

            <p style="text-align:center; width:100%;" >There Are No Company</p>

            @endforelse


            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card company-collection-card"
                    style="height:162px; width:100% !important ; background:none; display:grid;align-items:center; border: 1px dashed gray;"
                    data-toggle="modal" data-target="#addnewcompany"
                    >
                    <div class="view" style="text-align: center;"> Add New Company</div>
                </div>
            </div>







        </div>
    </div>
</section>

{{-- add new Company  --}}

<div class="modal fade" id="addnewcompany" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Add new Company</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <form action="{{url('/')}}/company/add" method="post" enctype="multipart/form-data" >
                @csrf
                <div class="modal-body">

                    <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" name="name" id="" class="form-control" placeholder="Name" required >

                    </div>

                    <div class="form-group">
                        <label for="">Description</label>
                        <input type="text" name="description" id="" class="form-control" required placeholder="Description" >

                    </div>

                    <div class="form-group">
                        <label for="">Company Logo</label>
                        <input type="file" name="logo" id="" class="form-control newcompany-logoinput" required placeholder="Company Logo" >

                    </div>

                    <div class="newcompany-logopreview">
                        <img src="" alt="" style="
                            height: 100px;
                            width: auto;"
                        >
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary addnewcompany">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>




{{-- All Companies --}}

<div class="modal fade" id="allcompanies" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="width: 80%; max-width:80%;     margin: 0 auto;" >
        <div class="modal-content" style="height: 70vh;">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">All Companies</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>

                <div class="modal-body" style="background-color:#F7F3F1;">

                    <div class="row">



                        @foreach (App\Models\Company::where('user_id' , Auth::user()->id)->get() as $company )

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <a href="{{url('/')}}/company/controlpanel?company={{$company->id}}" class="card company-collection-card-modal" style="height:200px; width:100% !important ;">
                                <div class="company-name"> {{ $company->name }} </div>
                                <div class="company-description">
                                    {{$company->description}} @if($loop->first) (HQ) @endif
                                </div>
                                <div class="users-in-company">

                                    <div class="user owner" >
                                        @if(!Auth::user()->logo)
                                            <div class="profile-img">
                                                <img src="http://127.0.0.1:8000/assets/images/avatar-male.png" class="profile" alt="">
                                            </div>
                                        @else
                                            <div class="profile-placeholder">
                                                <p>{{ substr(Auth::user()->name , 0 , 2) }}</p>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="user extra" >

                                        <div class="profile-placeholder" style="background-color:gray; color:white;">
                                            <p>+4</p>
                                        </div>

                                    </div>

                                </div>
                            </a>


                        </div>


                        @endforeach







                    </div>


                    {{-- @forelse(App\Company::where('user' , Auth::user()->id)->get() as $company)




                    @endforelse --}}

                </div>
                <div class="modal-footer">

                </div>

        </div>
    </div>
</div>





{{-- scripts --}}

<script src="{{url('/')}}/assets/js/home/companyblock.js"> </script>


<script>
    $(document).ready(function(){
        $('body').on( 'click' , '.uploadbox',function(e){
            e.stopPropagation();
            e.preventDefault();

            $('#companylogoinput').trigger('click');
            console.log('hit');
        });


        $('.companylogoinput').on('change' , function(){
            //console.log('hit');

            var fileinput = $('.companylogoinput');


            var file = $(fileinput)[0].files[0];
            var reader = new FileReader();


            reader.readAsDataURL(file);

            reader.onload = function (e) {

                $('.logopreview').attr('src', e.target.result);
                // $(block).attr('state', 'active');
            }
            $('.logopreviewbox').show();
            $('.uploadbox').hide();
            // $('#logoSave').show();

        });




        // newcompany logo preview

        $('.newcompany-logoinput').on('change' , function(){
            //console.log('hit');

            var fileinput = $('.newcompany-logoinput');


            var file = $(fileinput)[0].files[0];
            var reader = new FileReader();


            reader.readAsDataURL(file);

            reader.onload = function (e) {

                $('.newcompany-logopreview').find('img').attr('src', e.target.result);

            }
            // $('.logopreviewbox').show();
            // $('.uploadbox').hide();


        });



        // hq logo update
        $('.hqlogoupdate').click(function(){

            var $check = $('.companylogoinput').val();
            var companyid = $(this).attr('companyid');
            if($check.length > 0){
                var formdata = new FormData($('#logoupdateform')[0]);
                // console.log(formdata.get('companylogoinput'));
                $.ajax({
                    url: `{{url('/')}}/company/update/logo/${companyid}`,
                    type : 'POST',
                    data : formdata,
                    processData: false,
                    contentType: false,
                    success : function(data){
                        // console.log(data);
                        if(data.status == 200){
                            $(".uploadbox").hide();

                            $(".logopreviewbox").hide();
                            $(".actuallogo").show();
                            $(".actuallogo img").attr('src',`{{url('/')}}/company/logo/${data.logo}`);

                        }
                    }
                });
            }
        });

        $('.hqlogocancel').click(function(){
            $(".uploadbox").show();
            $(".actuallogo").hide();
            $(".logopreviewbox").hide();
        });

        $('.settingbtn').click(function(){
            $(".uploadbox").toggle();
            $(".actuallogo").toggle();
            $(".logopreviewbox").hide();
        });


    });
</script>
