<section class="project-index__section companysection " data-role="project_group content_filter_group">
    {{-- {{dd(App\Models\User::where('id' , Auth::user()->id)->with('companies')->first()) }} --}}
    {{-- {{dd((App\Models\User::where('id' , Auth::user()->id)->with('companies')->get()))}} --}}

    <header class="centered">
        <h3 class="project-index__header break break--on-background push--top push_half--bottom">
            <span>Company</span>
        </h3>
        <aside class="project-index__toolbar project-index__toolbar--new hide-from-clients"
            role="presentation" data-behavior="hide_when_content_filter_active">
            <span class="options-menu options-menu--add-project" data-purpose="team"
                data-behavior="expandable render_new_project_form_on_expand reveal_on_expand">
                <button name="button" type="button" title="Start a new Company"
                    data-toggle="modal" data-target="#addnewcompany"
                    class="options-menu__expansion-toggle btn btn--small btn--with-icon btn--add-icon companymodalbtn "
                    data-behavior="toggle_expansion_on_click">New</button>

            </span>
        </aside>
        <aside class="project-index__toolbar" data-behavior="hide_when_content_filter_active"
            role="presentation">
            <span class="options-menu" data-behavior="expandable">
                <button name="button" type="button" title="All Companies"
                data-toggle="modal" data-target="#allcompanies"
                    class="options-menu__expansion-toggle btn btn--small btn--icon btn--overflow-icon"
                    data-behavior="toggle_expansion_on_click">File…</button>

            </span>
        </aside>
    </header>
    <div class="card-grid--projects">


        @php

        @endphp

        <article class="card card--project card--project-logo hide-from-clients" data-behavior="hide_when_content_filter_active">
            <div class="card__content flex--vertically-center">
                {{-- if logo is available --}}

                @if($company->logo)
                    <div class="uploadbox" style="display:none">
                        Upload Image

                    </div>
                    <div class="actuallogo" >
                        <img src="{{url('/')}}/company/logo/{{$company->logo}}" class="" alt="" style="">

                    </div>
                    @owner($company)
                    <div class="settingbtn">
                        <i class="fa fa-cog" ></i>
                    </div>
                    @endowner
                @else
                    {{-- else show upload box --}}
                    <div class="uploadbox">
                        Upload Image

                    </div>
                    <div class="actuallogo" style="display:none"> >
                        <img src="" class="" alt="" style="">

                    </div>



                @endif
                <form id="logoupdateform" action="{{url('/')}}/company/update/logo/{{$company->id}}" method="post" enctype="multipart/form-data" >
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="file" style="display:none;" name="companylogoinput" class="companylogoinput" id="companylogoinput" >
                </form>
                <div class="logopreviewbox" style="display:none;">
                    <img src="" class="logopreview" alt="" style="">
                    <div class="btngroup" style=" width:fit-content;">
                        <button class="btn btn-success hqlogoupdate" companyid="{{$company->id}}" style="font-size:12px;">
                            save
                        </button>
                        <button class="btn btn-danger hqlogocancel " style="font-size:12px;">
                            cancel
                        </button>
                    </div>
                </div>



            </div>
        </article>
        <article id="" class="card card--project card--project-company_hq mt-4" data-role="project_group_item project_card content_filterable" data-sortable-name="company HQ" data-sortable-group="C">
            <section class="card__slider" data-role="card_slider">
                <div class="card__slide card__slide-1" data-role="card_slide" data-step="1">
                    <a class="card__link" href="{{url('/')}}/company/controlpanel">
                        <div class="card__content">
                            <section>
                                <div class="card__filterable-content">
                                    <h2 class="card__title flush" title="" data-role="content_filter_text">

                                        {{$company->name}} HQ
                                    </h2>
                                    <p class="card__description flush" title="" data-role="content_filter_text">
                                        {{$company->description}}
                                    </p>
                                </div>
                            </section>
                            <section class="card__people avatar-group avatar-group--tiny" aria-hidden="true">
                                @foreach ($company->user as $user)

                                    <img title="{{$user->name}}" alt="name" class="avatar" width="64" height="64" src="{{asset('assets/images/blank-avatar.png')}}">

                                @endforeach
                            </section>
                        </div>
                    </a>
                </div>
                <div id="bucket_20855531_card_actions" class="card__slide card__slide-2" data-step="2" data-role="card_slide">
                </div>
                <div id="bucket_20855531_card_edit" class="card__slide card__slide-3" data-step="3" data-role="card_slide project_card_edit">
                </div>
            </section>
        </article>
    </div>
</section>

{{-- add new Company  --}}

<div class="modal fade" id="addnewcompany" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Add new Company</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <form action="{{url('/')}}/company/add" method="post" enctype="multipart/form-data" >
                @csrf
                <div class="modal-body">

                    <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" name="name" id="" class="form-control" placeholder="Name" required >

                    </div>

                    <div class="form-group">
                        <label for="">Description</label>
                        <input type="text" name="description" id="" class="form-control" required placeholder="Description" >

                    </div>

                    <div class="form-group">
                        <label for="">Company Logo</label>
                        <input type="file" name="logo" id="" class="form-control newcompany-logoinput" required placeholder="Company Logo" >

                    </div>

                    <div class="newcompany-logopreview">
                        <img src="" alt="" style="
                            height: 100px;
                            width: auto;"
                        >
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary addnewcompany">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>




{{-- All Companies --}}

<div class="modal fade" id="allcompanies" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="width: 80%; max-width:80%;     margin: 0 auto;" >
        <div class="modal-content" style="height: 70vh;">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">All Companies</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>

                <div class="modal-body" style="background-color:#F7F3F1;">

                    <div class="row">



                        @foreach (App\Models\Company::where('user_id' , Auth::user()->id)->get() as $company )

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <a href="{{url('/')}}/company/controlpanel?company={{$company->id}}" class="card company-collection-card" style="height:200px; width:100% !important ;">
                                <div class="company-name"> {{ $company->name }} </div>
                                <div class="company-description">
                                    {{$company->description}} @if($loop->first) (HQ) @endif
                                </div>
                                <div class="users-in-company">

                                    <div class="user owner" >
                                        @if(!Auth::user()->logo)
                                            <div class="profile-img">
                                                <img src="http://127.0.0.1:8000/assets/images/avatar-male.png" class="profile" alt="">
                                            </div>
                                        @else
                                            <div class="profile-placeholder">
                                                <p>{{ substr(Auth::user()->name , 0 , 2) }}</p>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="user extra" >

                                        <div class="profile-placeholder" style="background-color:gray; color:white;">
                                            <p>+4</p>
                                        </div>

                                    </div>

                                </div>
                            </a>


                        </div>


                        @endforeach







                    </div>


                    {{-- @forelse(App\Company::where('user' , Auth::user()->id)->get() as $company)




                    @endforelse --}}

                </div>
                <div class="modal-footer">

                </div>

        </div>
    </div>
</div>





{{-- scripts --}}

<script>
    $(document).ready(function(){
        $('body').on( 'click' , '.uploadbox',function(e){
            e.stopPropagation();
            e.preventDefault();

            $('#companylogoinput').trigger('click');
            console.log('hit');
        });


        $('.companylogoinput').on('change' , function(){
            //console.log('hit');

            var fileinput = $('.companylogoinput');


            var file = $(fileinput)[0].files[0];
            var reader = new FileReader();


            reader.readAsDataURL(file);

            reader.onload = function (e) {

                $('.logopreview').attr('src', e.target.result);
                // $(block).attr('state', 'active');
            }
            $('.logopreviewbox').show();
            $('.uploadbox').hide();
            // $('#logoSave').show();

        });




        // newcompany logo preview

        $('.newcompany-logoinput').on('change' , function(){
            //console.log('hit');

            var fileinput = $('.newcompany-logoinput');


            var file = $(fileinput)[0].files[0];
            var reader = new FileReader();


            reader.readAsDataURL(file);

            reader.onload = function (e) {

                $('.newcompany-logopreview').find('img').attr('src', e.target.result);

            }
            // $('.logopreviewbox').show();
            // $('.uploadbox').hide();


        });



        // hq logo update
        $('.hqlogoupdate').click(function(){

            var $check = $('.companylogoinput').val();
            var companyid = $(this).attr('companyid');
            if($check.length > 0){
                var formdata = new FormData($('#logoupdateform')[0]);
                // console.log(formdata.get('companylogoinput'));
                $.ajax({
                    url: `{{url('/')}}/company/update/logo/${companyid}`,
                    type : 'POST',
                    data : formdata,
                    processData: false,
                    contentType: false,
                    success : function(data){
                        // console.log(data);
                        if(data.status == 200){
                            $(".uploadbox").hide();

                            $(".logopreviewbox").hide();
                            $(".actuallogo").show();
                            $(".actuallogo img").attr('src',`{{url('/')}}/company/logo/${data.logo}`);

                        }
                    }
                });
            }
        });

        $('.hqlogocancel').click(function(){
            $(".uploadbox").show();
            $(".actuallogo").hide();
            $(".logopreviewbox").hide();
        });

        $('.settingbtn').click(function(){
            $(".uploadbox").toggle();
            $(".actuallogo").toggle();
            $(".logopreviewbox").hide();
        });


    });
</script>
