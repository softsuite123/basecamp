@extends('front.Master.app')
@section('content')



@php

    if(isset($_GET['company'])){
        App\Models\Company::select($_GET['company']);
    }

    // echo(json_encode(App\Models\Company::current()))
    $company =App\Models\Company::current();



@endphp

@include('front.home.partials.garbage')

<main id="main-content" class="loading__hide  u-hide-focus" data-appearing-on="" data-bucket-url="" data-bucket-id=""
    tabindex="-1">


    @include('front.Master.partials.sidebar')



    <div class="panel filterable-projects" data-behavior="filterable_projects">
        <bc-content-filter>
            <header class="centered" role="presentation" style="margin-top:35px;">

                <p class="metadata push_quarter--top txt--small hide_on_mobile"
                    data-behavior="hide_when_content_filter_active">
                    Press <span data-role="jump-menu-modifier-key">Ctrl</span>+J to quickly jump to a project or team
                    from anywhere.
                </p>
            </header>


            {{-- company block --}}

            @include('front.home.partials.newcompanyblock')



            {{-- Team Block --}}

            @include('front.home.partials.teamblock')



            <div class="flush--ends" data-role="unsubscribed_projects">
                <section data-role="project_group content_filter_group"
                    data-behavior="hide_unless_content_filter_match">
                    <header class="centered">
                        <h5 class="project-index__header project-index__header--small">
                            <span>Teams you don't follow</span>
                        </h5>
                    </header>
                    <div class="card-grid card-grid--projects">
                    </div>
                </section>
                <footer class="flush--top centered" data-behavior="hide_when_content_filter_active">
                    <span class="u-display-n">
                        <button name="button" type="submit" class="plain-btn decorated project-index__more"
                            data-behavior="show_unsubscribed_projects">
                            + <span data-role="unsubscribed_projects_count">0</span> more you don’t follow
                        </button> </span>
                </footer>
            </div>


            @include('front.home.partials.projectblock')





        </bc-content-filter>
    </div>
</main>


@endsection
