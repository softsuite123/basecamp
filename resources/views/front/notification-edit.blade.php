@extends('front.Master.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 mr-auto ml-auto mt-5">
                <div class="p-5" style="background-color: #fff; border-radius: 10px">
                    <div class="text-center p-5">
                        <h3>Notification settings and Work Can Wait</h3>
                    </div>
                    <form>
                        <div class="what pb-3" >
                            <h4>What?</h4>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="flexRadioDefault1" >
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        Notify me about everything
                                    </label><br>
                                    <small>This includes new messages and comments, to-dos assigned to you, when someone @mentions you, and Campfire chats and Pings you’re part of.</small>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="flexRadioDefault1">
                                    <label class="form-check-label" for="flexRadioDefault2">
                                        Only notify me when someone sends me a Ping or @mentions me
                                    </label><br>
                                    <small>Basecamp will only send you a notification when someone sends you a Ping or @mentions you anywhere. You can always check the Hey! and Campfire menus manually to see what else is new for you.</small>
                                </div>
                            </div>
                            <div class="how pt-4 pb-3" >
                                <h4>How?</h4>
                                <div class="form-check">
                                    <input class="form-check-input checked" type="checkbox" value="" id="flexCheckDefault">
                                    <label class="form-check-label" for="flexCheckDefault">
                                        Send me email notifications
                                    </label><br>
                                    <small>Note: To prevent your inbox from overflowing, Basecamp will bundle Pings together if they happen within a few minutes of each other. You won’t be emailed if you are actively participating in a Ping or Campfire chat in the Basecamp app or on your computer.
                                    </small>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input checked" type="checkbox" value="" id="flexCheckChecked" checked>
                                    <label class="form-check-label" for="flexCheckChecked">
                                        Pop up notifications on my computer when Basecamp is open
                                    </label><br>
                                    <small>Pop up notifications are disabled by your browser. Change your browser settings to allow them.<br>
                                        Note: To prevent you from being annoyingly overnotified, you won’t get pop-up alerts about a particular message, to-do, or chat if you’re looking right at it.</small>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked1">
                                    <label class="form-check-label" for="flexCheckDefault">
                                        Show the number of unread items
                                    </label><br>
                                    <small>You’ll see counts for each new Ping, Campfire, and Hey! notification when Basecamp is open in your browser.</small>
                                </div>
                                <div class="what pb-3 pt-4" >
                                    <h4>When?</h4>
                                    <div class="form-group">
                                        <div class="form-check" >
                                            <input onchange= "Reminder(0)" class="form-check-input" type="radio" name="flexRadioDefault2">
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                Always! 24/7/365 no matter what.
                                            </label>
                                        </div>
                                        <div class="form-check" >
                                            <input onchange="Reminder(1)"  class="form-check-input" type="radio" name="flexRadioDefault2">
                                            <label class="form-check-label" for="flexRadioDefault2">
                                                Work Can Wait! Only during my work hours…
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-11 ml-auto pt-5">

                                        <div id="reminder" >
                                            <div class="row">
                                                    <div>
                                                        <select style="border-radius: 25px" class="form-control">
                                                            <option>--time--</option>
                                                        </select>
                                                    </div>
                                                    <h5 class="pl-2 pr-2 pt-2">to</h5>
                                                    <div>
                                                        <select style="border-radius: 25px" class="form-control">
                                                            <option>--time--</option>
                                                        </select>
                                                    </div>
                                            </div>
                                            <div class="row pt-4"  >
                                                <div class="selectable-buttons selectable-buttons--with-equal-widths" data-behavior="selectable_buttons">
                                                    <button type="button" id="mo" onclick="days('#mo')" class="selectable-buttons__button btn btn--small btn--primary" data-role="selectable_button" data-value="1">Mo</button>
                                                    <button type="button" id="tu" onclick="days('#tu')" class="selectable-buttons__button btn btn--small btn--primary" data-role="selectable_button" data-value="2">Tu</button>
                                                    <button type="button" id="we" onclick="days('#we')" class="selectable-buttons__button btn btn--small btn--primary" data-role="selectable_button" data-value="3">We</button>
                                                    <button type="button" id="thu" onclick="days('#thu')" class="selectable-buttons__button btn btn--small btn--primary" data-role="selectable_button" data-value="4">Th</button>
                                                    <button type="button" id="fri" onclick="days('#fri')" class="selectable-buttons__button btn btn--small btn--primary" data-role="selectable_button" data-value="5">Fr</button>
                                                    <button type="button" id="sa" onclick="days('#sa')" class="selectable-buttons__button btn btn--small" data-role="selectable_button" data-value="6" style="outline: none;">Sa</button>
                                                    <button type="button" id="su" onclick="days('#su')" class="selectable-buttons__button btn btn--small" data-role="selectable_button" data-value="0" style="outline: none;">Su</button>
                                                </div>
                                            </div>
                                            <div class="row pt-4">
                                                <div class="form-check">
                                                    <input class="form-check-input checked" type="checkbox" value="" id="flexCheckChecked" checked>
                                                    <label class="form-check-label" for="flexCheckChecked">
                                                        Catch me up if anything happened after hours
                                                    </label><br>
                                                    <small>We’ll send you an email or push notification summarizing everything that happened while you were away.</small>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <button type="submit" class="btn btn-primary mt-4">Save my changes</button>
                                </div>
                            </div>
                        </div>
                    </form>
        </div>
    </div>
@endsection
