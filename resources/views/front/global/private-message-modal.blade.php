
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<div class="modal fade" id="dmuser"  role="dialog" aria-labelledby="dmuser" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="    max-width: 70%;top: -30%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Send A Private Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="addeventform">

                <div class="modal-body">

                    <div class="form-group">
                        <label for="">Select User</label>
                        <select name="user" class="form-control" id="dmuserselect" style="width:100%;">
                            @foreach (Auth::user()->companies()->with('user')->get() as $company )

                                <optgroup label="{{$company->name}}">
                                    @foreach ($company->user as $user )
                                        <option space="{{strtolower(class_basename($company))}}" space-id="{{$company->id}}" value="{{$user->id}}">{{$user->name}}  </option>

                                    @endforeach
                                </optgroup>

                            @endforeach
                        </select>

                    </div>






                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary "
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary dmuserroute" route="">Message</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    var baseurl  = "{{url('/')}}";
    $(document).ready(function() {
        // $('#dmuserselect').select2();

        setchatroute();

        $('#dmuserselect').change(function() {
            setchatroute();
        });

        function setchatroute(){
            var dmselect = $('#dmuserselect');
            var userid = dmselect.val();
            var space = dmselect.find(":selected").attr('space');
            var spaceid = dmselect.find(":selected").attr('space-id');
            // console.log(spaceid , space, userid);
            $('.dmuserroute').attr("route",`${baseurl}/chat?space=${space}&space_id=${spaceid}&user_id=${userid}`);

        }


    });


</script>
