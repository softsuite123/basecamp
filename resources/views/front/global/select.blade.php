{{-- style --}}
<link rel="stylesheet" href="{{url('/')}}/assets/css/globalselect.css">

{{-- {{dd((App\Models\User::where('id' , Auth::user()->id)->with('companies')->first()))}} --}}

{{-- template --}}
<div class="global-select-modal">


    <div class="body">
        <div class="header">
            <input type="text" class="searchinp global-select-input" placeholder="Select or Switch to Another Company ">
        </div>
        <div class="content">
            <div class="collection-group">
                <div class="title">Companies</div>
                <div class="collection" id="companylist-select">
                    @forelse ( (App\Models\User::where('id' , Auth::user()->id)->with('companies')->first())->companies as $company )

                    <div class="searchablerow @if($loop->first)  @endif" redirect="{{url('/')}}/home?company={{$company->id}}">
                        <div class="icon">
                            @if($company->logo)
                            <img src="{{url('/')}}/company/logo/{{$company->logo}}" alt="" class="img">
                            @else
                            <i class="fa fa-cloud" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div class="detail">
                            {{$company->name}}
                        </div>
                    </div>

                    @empty



                    @endforelse
                </div>
            </div>

            {{-- <div class="collection-group">
                <div class="title">Teams</div>
                <div class="collection" id="teamlist-select">
                    @forelse ( App\Models\Team::where('user_id' , Auth::user()->id)->where('company_id' , App\Models\Company::current()->id)->get() as $team )

                        <div class="searchablerow @if($loop->first)  @endif" redirect="{{url('/')}}/home?company={{$team->id}}">
            <div class="icon">
                @if($team->logo)
                <img src="{{url('/')}}/company/logo/{{$team->logo}}" alt="" class="img">
                @else
                <i class="fa fa-cloud" aria-hidden="true"></i>
                @endif
            </div>
            <div class="detail">
                {{$team->name}}
            </div>
        </div>

        @empty



        @endforelse
    </div>
</div> --}}
</div>
</div>


</div>


{{-- script --}}
<script>
    $(document).ready(function () {
        $('.global-select-input').keyup(function () {
            var value = $(this).val().toLowerCase();
            // console.log(searchtext);
            $("#companylist-select .searchablerow").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $("#teamlist-select .searchablerow").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });

        $('.searchablerow').click(function () {

            var thislink = $(this).attr('redirect');
            window.location.href = thislink;

        });




    });







    function openglobalselect() {
        $('.global-select-modal').fadeIn('fast');
        $('.global-select-input').focus();
    }

    function closeglobalselect() {
        $('.global-select-modal').fadeOut('fast');
        $('.global-select-input').blur();


    }

</script>
