
{{-- style --}}
<link rel="stylesheet" href="{{url('/')}}/assets/css/globalsearch.css">


{{-- template --}}
<div class="global-search-modal">


    <div class="body" >
        <div class="closebtn" onclick="closeglobalsearch()"><i class="fa fa-times"></i></div>
        <div class="header">
            <input type="text" class="searchinp global-search-input" placeholder="Select or Switch to Another Company , Project or Team">
        </div>
        <div class="content">
            <div class="collection-group">
                <div class="title">Companies</div>
                <div class="collection" id="companylist-search">

                    {{-- {{dd((App\Models\User::where('id' , Auth::user()->id)->with('companies')->first())->companies)}} --}}
                    @forelse ( (App\Models\User::where('id' , Auth::user()->id)->with('companies')->first())->companies as $company )

                        <div class="searchablerow @if($loop->first)  @endif" redirect="{{url('/')}}/company/controlpanel?company={{$company->id}}">
                            <div class="icon">
                                @if($company->logo)
                                    <img src="{{url('/')}}/company/logo/{{$company->logo}}" alt="" class="img">
                                @else
                                    <i class="fa fa-cloud" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="detail">
                                {{$company->name}}
                            </div>
                        </div>

                    @empty



                    @endforelse
                </div>
            </div>

            <div class="collection-group">
                <div class="title">Teams</div>
                <div class="collection" id="teamlist-search">
                    @forelse ( App\Models\Team::where('user_id' , Auth::user()->id)->where('company_id' , App\Models\Company::current()->id)->get() as $team )

                        <div class="searchablerow @if($loop->first)  @endif" redirect="{{url('/')}}/team/controlpanel?team={{$team->id}}">
                            <div class="icon">
                                @if($team->logo)
                                    <img src="{{url('/')}}/company/logo/{{$team->logo}}" alt="" class="img">
                                @else
                                    <i class="fa fa-cloud" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="detail">
                                {{$team->name}}
                            </div>
                        </div>

                    @empty



                    @endforelse
                </div>
            </div>
        </div>
    </div>


</div>



{{-- script --}}
<script>
    $(document).ready(function(){
        $('.global-search-input').keyup(function(){
            var value = $(this).val().toLowerCase();
            // console.log(searchtext);
            $("#companylist-search .searchablerow").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $("#teamlist-search .searchablerow").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });

        $('.global-search-modal .searchablerow').click(function(){

            var thislink = $(this).attr('redirect');
            window.location.href = thislink;

        });




    });







    function openglobalsearch(){
        $('.global-search-modal').fadeIn('fast');
        $('.global-search-input').focus();
    }

    function closeglobalsearch(){
        $('.global-search-modal').fadeOut('fast');
        $('.global-search-input').blur();


    }


</script>
