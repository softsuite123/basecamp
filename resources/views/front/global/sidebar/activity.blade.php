@extends('front.Master.app')
@section('content')


@include('front.Master.partials.sidebar')




@php

// dd('hell');

$space = App\Http\Controllers\SpaceController::get();
$space->load('user');
// dd($space);
$namespace = get_class($space);
$space_url = strtolower(class_basename($space));

// dd($slingleft);

@endphp

<link rel="stylesheet" href="{{url('/')}}/assets/css/invitetocompany.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/fileshare.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/toast.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/activity-page.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />




<div class="actionpanel">
    <a href="{{url('/')}}/{{$space_url}}/controlpanel" class="link">
        <i class="fa fa-th-large"></i>
    </a>
    {{-- <div class="name">{{$space->name}}</div> --}}
</div>
<div class="panelcard">
    <div class="header">





        <div class="title" style="width: 100%;">
            Latest Activities
            {{-- {{$list->name}} --}}
        </div>


    </div>


    <div class="body">
        <div class="activity-box">
            <div class="sling sling-left">

                @foreach ($slingleft as $activity )

                    @php
                        // dd($activity);
                        $space = $activity->space::where('id' , $activity->space_id)->first();
                    @endphp

                    <div class="activity-holder">
                        <div  class="space">
                            {{$space->name}}
                        </div>
                        <div class="divider"></div>
                        <div class="activity">
                            <div class="icon">
                                <div class="userpresenter">
                                    {{substr($activity->author->name, 0, 1)}}
                                </div>
                            </div>
                            <a href="{{url('/')}}{{$activity->link}}" class="info">
                                <div class="date">
                                    {{$activity->created_at->diffForHumans()}}
                                </div>
                                <div class="title">
                                    {{$activity->title}}
                                </div>
                                <div class="description">
                                    {{$activity->note}}

                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach





            </div>
            <div class="sling sling-right">

                @foreach ($slingright as $activity )

                    @php
                        // dd($activity);
                        $space = $activity->space::where('id' , $activity->space_id)->first();
                    @endphp

                    <div class="activity-holder">
                        <div class="space">
                            {{$space->name}}
                        </div>
                        <div class="divider"></div>
                        <div class="activity">
                            <div class="icon">
                                <div class="userpresenter">
                                    {{substr($activity->author->name, 0, 1)}}
                                </div>
                            </div>
                            <a href="{{url('/')}}{{$activity->link}}" class="info">
                                <div class="date">
                                    {{$activity->created_at->diffForHumans()}}
                                </div>
                                <div class="title">
                                    {{$activity->title}}
                                </div>
                                <div class="description">
                                    {{$activity->note}}

                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>

        </div>



    </div>






</div>


{{-- modals --}}


{{-- Add folder --}}


<div class="modal fade" id="addfolder" tabindex="-1" role="dialog" aria-labelledby="addfolder" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Folder</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="addfolderform">

                <div class="modal-body">

                    <div class="form-group">
                        <label for=""></label>
                        <input type="text" name="name" id="" class="form-control inp" placeholder="Folder Name"
                            required="true">
                    </div>


                    <input type="text" style="display:none;" class="inp" name="folder_id"
                        value="{{isset($_GET['folderid']) ? $_GET['folderid'] : 0 }}">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeaddfolderform"
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary addfolderbtn">Add Folder</button>
                </div>
            </form>
        </div>
    </div>
</div>










<script src="{{url('/')}}/assets/js/formvalidation.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{url('/')}}/assets/js/global/preloader.js"></script>
<script src="{{url('/')}}/assets/js/global/toast.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- <script type="text/javascript">

    document.cookie = "AC-C=ac-c;expires=Fri, 31 Dec 9999 23:59:59 GMT;path=/;HttpOnly;SameSite=None";
  </script> --}}




@endsection
