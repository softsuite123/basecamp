@extends('front.Master.app')
@section('content')

@include('front.Master.partials.sidebar')



@php


$space = App\Http\Controllers\SpaceController::get();
$space->load('user');
// dd($space);
$namespace = get_class($space);
$space_class = class_basename($space);
// dd($space_class);
$space_url = strtolower(class_basename($space));

$company = $space ;

@endphp

<link rel="stylesheet" href="{{url('/')}}/assets/css/invitetocompany.css">


<div class="actionpanel" >
    <a href="{{url('/')}}/{{$space_url}}/controlpanel" class="link">
        <i class="fa fa-th-large"></i>
    </a>
    <div class="name">{{$space->name}}</div>
</div>
<div class="panelcard">
    <div class="header">
        <button class="btn btn-success addbtn show-addnewform">
            <i class="fa fa-plus"></i> Add People
        </button>
        <div class="title">
            People in {{$company->name}}
        </div>
    </div>

    <form id='newPersonForm'>

        <div class="addnewperson">
            <div class="head">
                <div class="close hide-addnewform"><i class="fa fa-times"></i></div>
            </div>
            <div class="steps">
                <div class="step step1">
                    <div class="title">1. Who Do you want to add?</div>
                    <div class="body">
                        <div class="newpersonvoid">

                        </div>
                        <button type="button" class="btn btn-success addanother add-newpersonform">Add Another Person</button>
                    </div>
                </div>

                <div class="step step2">
                    <div class="title">2. Add a personal note to the invitation email (optional)</div>
                    <div class="body">
                        <input class="form-control personalNote" type="text" name="personal_note"
                            placeholder="Add a personal note to the email">
                    </div>
                </div>

                <div class="step step3">
                    <div class="title">3. Preview and send email</div>
                    <div class="body">
                        <div class="btnrow">
                            <button class="btn btn-success " data-toggle="modal" data-target="#emailpreview">
                                <i class="fa fa-search"></i> Preview this email
                            </button>
                            <button type="button" class="btn btn-success sendnow"> Send Now </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

    <div class="userslist">
        <div class="coastline"></div>
        <div class="title">{{$company->name}} </div>

        @php
        $companyowner_id = $company->user_id;
        $owner = App\Models\User::where('id' , $companyowner_id)->first();
        @endphp
        {{-- Owner --}}
        <div class="user">
            <div class="profile">
                <img src="{{url('/')}}/assets/images/avatar-male.png" alt="" class="img">
            </div>
            <div class="info">
                <div class="name">{{$owner->name}}</div>
                <div class="designation">Owner at {{$company->name}}</div>
                <div class="email">{{$owner->email}}</div>
                @owner($company)
                <div class="actions">
                    <button class="btn btn-primary action">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button class="btn btn-danger action">
                        <i class="fa fa-trash"></i>
                    </button>
                    <button class="btn btn-success action">
                        <i class="fa fa-share-alt"></i>
                    </button>
                </div>
                @endowner
            </div>
        </div>



        <div class="coastline"></div>
        <div class="title">Members of {{$company->name}} </div>

        @php
            $company_users = $company->user;
            // dd($company_users);
        @endphp

        @foreach ($company_users as $member)
            @if ($member->id != $companyowner_id)
                {{-- Company Member --}}
                <div class="user">
                    <div class="profile">
                        <img src="{{url('/')}}/assets/images/avatar-male.png" alt="" class="img">
                    </div>
                    <div class="info">
                        <div class="name">{{$member->name}}</div>
                        <div class="designation"> {{$company->name}}</div>
                        <div class="email">{{$member->email}}</div>
                        <div class="actions">
                            <button class="btn btn-primary action">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button class="btn btn-danger action">
                                <i class="fa fa-trash"></i>
                            </button>
                            <button class="btn btn-success action">
                                <i class="fa fa-share-alt"></i>
                            </button>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach




    </div>


</div>



<!-- Modal -->
<div class="modal fade" id="emailpreview" tabindex="-1" role="dialog" aria-labelledby="emailpreview" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered widemodal" role="document" style="">
        <div class="modal-content">
            {{-- <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> --}}
            <div class="modal-body" style="padding:20px; ">
                <div class="closemodal"><button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button></div>
                <div class="mailinfo">

                    <table>
                        <tr>
                            <td>From :</td>
                            <td> {{Auth::user()->name}}</td>
                        </tr>
                        <tr>
                            <td>Subject :</td>
                            <td> {{Auth::user()->name}} invited you to their Team Leaders!</td>
                        </tr>
                        <tr>
                            <td>To :</td>
                            <td> Recipients</td>
                        </tr>
                        <tr>
                            <td>Reply-To :</td>
                            <td> {{Auth::user()->email}}</td>
                        </tr>
                    </table>
                </div>
                <hr>

                <div class="mail-view">
                    <div class="title">{{Auth::user()->name}} invited you to their Team Leaders!</div>
                    <div class="mail-note">
                        <div class="note-title">{{Auth::user()->name}} added a note:</div>
                        <div class="note personal-note-preview">
                            (If you add a note , it will go right here.)
                        </div>
                    </div>
                    <div class="mail-join">
                        <div class="join-note">
                            Team Leaders is a place that helps everyone stay organized and on the same page. It’s really
                            straightforward and easy! To join {{Auth::user()->name}}, click this button:
                        </div>
                        <div class="btn btn-success join-button">
                            Join {{Auth::user()->name}} in Team Leaders
                        </div>
                        <div class="reply-note">
                            If you have any questions, just email Usama or reply to this message.
                        </div>
                    </div>
                </div>


            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div> --}}
        </div>
    </div>
</div>



<script src="{{url('/')}}/assets/js/formvalidation.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{url('/')}}/assets/js/global/preloader.js"></script>


<script>
    var $invited_users = {};
    $(document).ready(function () {


        console.log($invited_users);


        $('.show-addnewform').click(function () {
            $('.addnewperson').toggle();
        });

        $('.hide-addnewform').click(function () {
            $('.addnewperson').hide();
        });

        // Add New Person Refrance and Card
        $('.add-newpersonform').click(function () {
            var personid = uniqueString(8);

            var newperson = {
                id: personid,
                name: "",
                email: "",
                company: "",
                title: ""
            };

            $invited_users[personid] = newperson;

            $('.newpersonvoid').append(`
                    <div class="newpersonform" person-id="${personid}">
                        <div class="header">
                            <div class="remove remove-newpersonform"><i class="fa fa-times"></i></div>
                        </div>
                        <div class="form-group">
                            <label for="">Name :</label>
                            <input type="text"  name="name" id="" class="form-control personinput inp" required="true" placeholder="">

                        </div>

                        <div class="form-group">
                            <label for="">Email :</label>
                            <input type="email" name="email" id="" class="form-control personinput inp" required="true" placeholder="">

                        </div>

                        <div class="form-group">
                            <label for="">Title (optional):</label>
                            <input type="text" name="title" id="" class="form-control personinput inp " placeholder="">

                        </div>

                        <div class="form-group">
                            <label for="">Company:</label>

                            <select class="form-control personinput inp" required="true" name="company">
                                @foreach ($namespace::where('user_id' , Auth::user()->id)->get() as $company )
                                    <option value="{{$company->id}}"> {{$company->name}} </option>
                                @endforeach
                            </select>

                        </div>

                    </div>

                `);
        });


        function uniqueString(length) {
            var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var result = '';
            for (var i = 0; i < length; i++) {
                result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
            }
            return result;
        }


        // Remove person block
        $('body').on('click', '.remove-newpersonform', function () {
            var id = $(this).parent().parent().attr('person-id');

            delete $invited_users[id];
            $(this).parent().parent().remove();
        });

        // Update Person Refrance object
        $('body').on('change', '.personinput', function () {
            var parent = $(this).parent().parent();
            var id = parent.attr('person-id');


            parent.find('.inp').each(function(){
                var name = $(this).attr('name');
                var val = $(this).val();
                $invited_users[id][name] = val;

            });
        });

        // Update Personal Note
        $('input[name=personal_note]').change(function () {
            var note = $(this).val();
            $('.personal-note-preview').html(note);
        });



        // validate form
        $('.sendnow').click(function () {
            preloader(true);
            var validation = validateform('#newPersonForm');
            var personalNote = $('input[name="personal_note"]').val();
            console.log(validation);
            if(validation.errors == 0){
                var data = {
                    users : $invited_users,
                    personal_note : personalNote,

                }
                data['_token'] = `{{csrf_token()}}`;
                $.ajax({
                    url: `{{url('/')}}/invite/add`,
                    type: 'POST',
                    data: data,
                    success: function (data){
                        console.log(data);
                        if(data.status == 200){
                            preloader(false);

                            swal("Great", "Invitation Sent !", "success");

                            window.location.href = "{{url('/')}}/{{$space_url}}/controlpanel?{{$namespace}}={{$space->id}}"
                        }
                    }

                });
            }









        });


    });

</script>


@endsection
