<div class="mail-view" style="margin-top:15px; width:450px; margin:0 auto; border:2px solid gray; border-radius:14px; padding: 20px;">
    <div class="title" style="font-size: 24px;font-weight: bold;margin-bottom: 10px;">
        {{Auth::user()->name}} invited you to their Team Leader!
    </div>
    <div class="mail-note" style="margin-bottom: 20px;">
        <div class="note-title" style="font-size: 20px;margin-bottom: 10px;" >
            {{Auth::user()->name}} added a note:
        </div>
        @if($personal_note != '' )
        <div class="note personal-note-preview" style="
                font-size: 18px;
                color:black;
                background-color: #ffffaf;
                border: 1px solid #c7c700;
                padding: 10px;
                /* width: 100%; */
                border-radius:6px;
            ">
            {{@$personal_note}}
        </div>
        @endif
    </div>
    <div class="mail-join" style="margin: 15px 0px;">
        <div class="join-note" style="font-size: 20px;">
            Teams Leader is a place that helps everyone stay organized and on the same page. It’s really
            straightforward and easy! To join {{Auth::user()->name}}, click this button:
        </div>

        @if(isset($processed))
            <a href="{{url('/')}}/" class="btn btn-success join-button" style="
                    margin-top:30px;
                    background-color:#2da562;
                    color: white;
                    display: block;
                    width:fit-content;
                    padding: 10px 20px;
                    border-radius: 25px;

                ">
                Join {{Auth::user()->name}} in Team Leaders
            </a>
        @else
            <a href="{{url('/')}}/invite/accept/{{@$code}}" class="btn btn-success join-button" style="
                    margin-top:30px;
                    background-color:#2da562;
                    color: white;
                    display: block;
                    width:fit-content;
                    padding: 10px 20px;
                    border-radius: 25px;

                ">
                Join {{Auth::user()->name}} in Team Leaders
            </a>

        @endif
        <div class="reply-note" style="font-size: 18px;margin-top:15px;">
            If you have any questions, just email Usama or reply to this message.
        </div>
    </div>
</div>
