@extends('front.Master.app')
@section('content')


@include('front.Master.partials.sidebar')


@php

if(isset($_GET['company'])){
App\Models\Company::select($_GET['company']);
}

$company =App\Models\Company::current();
@endphp

<link rel="stylesheet" href="{{url('/')}}/assets/css/controlpanel.css">



<div class="controlpanel">
    <div class="header">
        <div class="menu">

        </div>
        <div class="meta">
            <div class="title">
                {{$company->name}}
            </div>
            <div class="description">
                {{$company->description}}
            </div>
        </div>
        <div class="people">
            <div class="joined">

                {{-- {{dd($company)}} --}}

                @foreach (($company->user)->slice(0, 5) as $member)
                <div class="perk" title="{{$member->name}}">
                    <p> {{substr($member->name , 0 , 2)}} </p>
                </div>
                @endforeach

            </div>
            <div class="addnew">
                <a href="{{url('/')}}/invite/company/{{$company->id}}">Add/remove People</a>
            </div>
        </div>
    </div>

    <div class="body">
        <div class="task-grid">
            <a href="{{url('/')}}/todo/company/{{$company->id}}" class="grid-item">
                <div class="cardtitle">
                    <div class="title">
                        Todo's
                    </div>

                </div>
                <div class="body">
                    <div class="placeholder">
                        <div class="icon">
                            <img src="{{url('/')}}/assets/images/tick.png" alt="" class="img">
                        </div>
                        <div class="description">
                            Make lists of work that needs to get done, assign items, set due dates, and discuss.

                        </div>
                    </div>
                </div>
            </a>
            <a href="{{url('/')}}/schedule/company/{{$company->id}}" class="grid-item">
                <div class="cardtitle">
                    <div class="title">
                        Schedule
                    </div>

                </div>
                <div class="body">
                    <div class="placeholder">
                        <div class="icon">
                            <img src="{{url('/')}}/assets/images/schedule.png" alt="" class="img">
                        </div>
                        <div class="description">
                            Set important dates on a shared schedule. Subscribe to events in Google Cal, iCal, or
                            Outlook.
                        </div>
                    </div>
                </div>
            </a>
            <a href="{{url('/')}}/fileshare/company/{{$company->id}}" class="grid-item">
                <div class="cardtitle">
                    <div class="title">
                        Docs & Files
                    </div>

                </div>
                <div class="body">
                    <div class="placeholder">
                        <div class="icon">
                            <img src="{{url('/')}}/assets/images/file2.png" alt="" class="img">
                        </div>
                        <div class="description">
                            Share docs, files, images, and spreadsheets. Organize in folders so they’re easy to find.
                        </div>
                    </div>
                </div>
            </a>

            <a href="{{url('/')}}/chat/company/{{$company->id}}" class="grid-item">
                <div class="cardtitle">
                    <div class="title">
                        Message Board
                    </div>

                </div>
                <div class="body">
                    <div class="placeholder">
                        <div class="icon">
                            <img src="{{url('/')}}/assets/images/chat-single.png" alt="" class="img">
                        </div>
                        <div class="description">
                            Chat casually with the group, ask random questions, and share stuff without ceremony.
                        </div>
                    </div>
                </div>
            </a>

            <a href="{{url('/')}}/groupchat/company/{{$company->id}}" class="grid-item">
                <div class="cardtitle">
                    <div class="title">
                        Campfire
                    </div>

                </div>
                <div class="body">
                    <div class="placeholder">
                        <div class="icon">
                            <img src="{{url('/')}}/assets/images/chat.png" alt="" class="img">
                        </div>
                        <div class="description">
                            Create recurring questions so you don't have to pester your team about what’s going on.
                        </div>
                    </div>
                </div>
            </a>

            <div class="grid-item">
                <div class="cardtitle">
                    <div class="title">
                        Automatic Check-ins
                    </div>

                </div>
                <div class="body">
                    <div class="placeholder">
                        <div class="icon">
                            <img src="{{url('/')}}/assets/images/question-mark.png" alt="" class="img">
                        </div>
                        <div class="description">
                            Share docs, files, images, and spreadsheets. Organize in folders so they’re easy to find.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

{{--
<script>
    $(document).ready(function () {
        $('.task-grid').sortable();
        $('.task-grid').disableSelection();
    });

</script> --}}




@endsection
