@extends('front.Master.app')
@section('content')


@include('front.Master.partials.sidebar')




@php

$space = App\Http\Controllers\SpaceController::get();
$space->load('user');
// dd($space);
$namespace = get_class($space);
$space_url = strtolower(class_basename($space));



@endphp

<link rel="stylesheet" href="{{url('/')}}/assets/css/invitetocompany.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/fileshare.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/toast.css">

<link href='{{url('/')}}/fullcalendar/lib/main.css' rel='stylesheet' />





<div class="actionpanel">
    <a href="{{url('/')}}/{{$space_url}}/controlpanel" class="link">
        <i class="fa fa-th-large"></i>
    </a>
    <div class="name">{{$space->name}}</div>
</div>


<div class="panelcard" id="app" style="">

    <div class="header">
        {{-- <div class="btn btn-success addbtn " style="background-color: #2dca81 !important; width:120px;">
            <i class="fa fa-plus"></i> Add
        </div> --}}


        <div class="select-menu">

            <div class="item  adddoctrigger" data-toggle="modal" data-target="#adddoc">
                <i class="fa fa-file"></i>
                <label for="">Make Document</label>
            </div>

            <div class="item "  data-toggle="modal" data-target="#addfolder">
                <i class="fa fa-folder"></i>
                <label for="">Make Folder</label>
            </div>

            <div class="item "  data-toggle="modal" data-target="#addfile">
                <i class="fa fa-upload"></i>
                <label for="">Upload File </label>
            </div>

            <div class="item  ">
                <i class="fa fa-cloud-download"></i>
                <label for="">Google Drive </label>
            </div>





        </div>


        <div class="title" style="width: 100%;">
            Schedule Events
            {{-- {{$list->name}} --}}
        </div>

        {{-- <select class="form-control" name="todo-type" id="todo-type" style="width:fit-content;">
            <option value="all-todos">All</option>
            <option value="my-todos">My Todos</option>
        </select> --}}
    </div>

    <div class="body">
        <div id="calendar">

        </div>

        <button style="display:none;" id="triggermodal" data-toggle="modal" data-target="#addevent">Add Event </button>



    </div>



</div>


{{-- modals --}}


{{-- Add event --}}


<div class="modal fade" id="addevent" tabindex="-1" role="dialog" aria-labelledby="addevent" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Folder</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="addeventform">

                <div class="modal-body">

                    <div class="form-group">
                        <label for="">Event Name</label>
                        <input type="text" name="name" id="" class="form-control inp" placeholder="Event Name"
                            required="true">
                    </div>

                    <div class="form-group">
                        <label for="">Event Description</label>
                        <input type="text" name="description" id="" class="form-control inp" placeholder="Event Description"
                            required="true">
                    </div>


                    <div class="form-group">
                        <label for="">Event Date</label>
                        <input type="date" name="from" id="" class="form-control inp" placeholder=""
                            >
                    </div>


                    <div class="form-group">
                        <label for="">Event End</label>
                        <input type="date" name="to" id="" class="form-control inp" placeholder=""
                           >
                    </div>




                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeaddeventform"
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary addeventbtn">Add Event</button>
                </div>
            </form>
        </div>
    </div>
</div>




{{-- Remove or edit Event --}}


<div class="modal fade" id="eventdetail" tabindex="-1" role="dialog" aria-labelledby="eventdetail" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Event Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="eventdetailform">

                <div class="modal-body">



                    <div class="card" style="width: 80% !important;">
                        <div class="card-body">
                            <h5 class="card-title event-title"></h5>
                            <h6 class="card-subtitle mb-2 text-muted event-date-from"></h6>
                            <h6 class="card-subtitle mb-2 text-muted event-date-to"></h6>
                            <p class="card-text event-description"></p>
                            {{-- <a href="#" class="card-link">Card link</a>
                            <a href="#" class="card-link">Another link</a> --}}
                        </div>
                    </div>

                    {{-- <p>Remove this Event ?</p> --}}



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary "
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger delete" event-id="">Delete Event</button>
                </div>
            </form>
        </div>
    </div>
</div>




<script src="{{url('/')}}/assets/js/formvalidation.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{url('/')}}/assets/js/global/preloader.js"></script>
<script src="{{url('/')}}/assets/js/global/toast.js"></script>
<script src='{{url('/')}}/fullcalendar/lib/main.js'></script>


<script>
    var c_start = '';
    var c_end = '';
    var thisevents = <?php echo(json_encode($events)); ?> ;

    document.addEventListener('DOMContentLoaded', function() {

        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            // plugins: [ interactionPlugin ],
            events: thisevents,
            initialView: 'dayGridMonth',
            editable: true,
            selectable: true,
            selectMirror: true,
            dayMaxEventRows: true, // for all non-TimeGrid views
            views: {
                timeGrid: {
                dayMaxEventRows: 3 // adjust to 6 only for timeGridWeek/timeGridDay
                }
            },
            select: function (start, end, jsEvent, view) {

                console.log(start.startStr , start.endStr );
                s_start = start.startStr;
                s_end = start.endStr;

                $('#triggermodal').trigger('click');
                var modal = $('#addevent');
                modal.find('[name=from]').val(s_start);
                modal.find('[name=to]').val(s_end);


            },
            eventDrop: function(event){
                console.log(event.event.title);
                var eventid = event.event.id;
                preloader(true);

                $.ajax({
                    type : "POST",
                    url  : `{{url('/')}}/schedule/event/update`,
                    data : {
                        eventid : eventid,
                        from : event.event.startStr,
                        to : event.event.endStr,
                        _token  : "{{csrf_token()}}",

                    },
                    success: function(res){
                        preloader(false);

                        console.log(res);
                        if(res.status == 200){
                            toast({
                                message: `${res.message}`,
                                type : 'success',
                                icon: 'fa fa-check'
                            });
                        }
                        if(res.status == 403 ){
                            toast({
                                message: `${res.message}`,
                                type : 'warning',
                                icon: 'fa fa-times'
                            });
                        }
                        if(res.status == 404){
                            toast({
                                message: `${res.message}`,
                                type : 'warning',
                                icon: 'fa fa-times'
                            });
                        }
                    }
                });


                // event.event.remove();
            },
            eventClick: function(event){
                console.log(event.event);
                $('#eventdetail').modal('show');
                var detailmodal = $('#eventdetail');
                detailmodal.find('.event-title').html(event.event.title);
                detailmodal.find('.event-date-from').html("From : " +event.event.startStr );
                detailmodal.find('.event-date-to').html("To : " + event.event.endStr);
                detailmodal.find('.event-description').html(event.event.extendedProps.description);
                detailmodal.find('.delete').attr("event-id" , event.event.id);

            },
            selectOverlap: function(event) {
                return ! event.block;
            },

        });
        calendar.render();

        $('.addeventbtn').click(function(){
            preloader(true);
            $('.closeaddeventform').trigger('click');

            var validation  = validateform('#addeventform');
            console.log(validation);
            validation.data._token = "{{csrf_token()}}";
            if(validation.errors = 1){

                $.ajax({
                    type: "POST",
                    url: "{{url('/')}}/schedule/event/store",
                    data: validation.data,
                    success: function(res){
                        console.log(res);
                        if(res.status == 200){
                            preloader(false);

                            console.log('log');
                            $('#addeventform')[0].reset();
                            calendar.addEvent({
                                title: validation.data.name,
                                start: validation.data.from,
                                end : validation.data.to,
                                allDay: true
                            });
                        }
                    }
                });


            }
        });


        $('#eventdetail').on('click', '.delete' , function(){
            preloader(true);
            $('#eventdetail').modal('hide');
            var eventid = $(this).attr('event-id');
            var event = calendar.getEventById(eventid);
            // console.log(event);
            // console.log(eventid);


            $.ajax({
                type : "POST",
                url  : `{{url('/')}}/schedule/event/delete`,
                data : {
                    eventid : eventid,
                    _token  : "{{csrf_token()}}",

                },
                success: function(res){
                    preloader(false);

                    console.log(res);
                    if(res.status == 200){
                        event.remove();
                        toast({
                            message: `${res.message}`,
                            type : 'success',
                            icon: 'fa fa-check'
                        });
                    }
                    if(res.status == 403 ){
                        toast({
                            message: `${res.message}`,
                            type : 'warning',
                            icon: 'fa fa-times'
                        });
                    }
                    if(res.status == 404){
                        toast({
                            message: `${res.message}`,
                            type : 'warning',
                            icon: 'fa fa-times'
                        });
                    }
                }
            });


        });

    });

</script>

<script>
    $(document).ready(function(){

        // $('.addeventbtn').click(function(){
        //     var validation  = validateform('#addeventform');
        //     console.log(validation);
        //     if(validation.errors < 1){

        //         calendar.addEvent({
        //             title: validation.name,
        //             start: validation.from,
        //             end : validation.to,
        //             allDay: true
        //         });
        //     }
        // });

    });


</script>



@endsection
