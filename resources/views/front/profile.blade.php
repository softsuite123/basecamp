@extends('front.Master.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-5 mr-auto ml-auto mt-5">
                <div class="pl-5 pr-5 pb-5" style="background-color: #fff; border-radius: 10px">
                    <form>
                        <div class="file-field pb-5">
                            <div class="mb-4 text-center">
                                <img style="width: 30%" src="{{asset('assets/images/blank-avatar.png')}}"
                                     class="rounded-circle z-depth-1-half avatar-pic" alt="example placeholder avatar">
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="btn btn-mdb-color btn-rounded float-left">
                                    <input type="file">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName">Name</label>
                            <input type="text" class="form-control" id="" aria-describedby="" placeholder="name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputTitle">Title at company</label>
                            <input type="text" class="form-control" id="" placeholder="job-title">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputTitle">Short bio or current status</label>
                            <input type="text" class="form-control" id="" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputTimeZone">Time zone</label>
                            <select class="form-control">
                                <option>--Select TimeZone--</option>
                            </select>
                        </div>
                        <button style="width: 100%; background-color: #2D69D9; color: #fff" type="submit" class="btn mt-4 btn-primary">Save my changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
