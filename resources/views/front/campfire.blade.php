@extends('front.Master.app')
@section('content')
    <div class="container">
        @include('front.partials.bucket-head')
        <div class="box ml-auto mr-auto p-5">
            <div class="text-center pb-3">
                <h4 class="pb-5">Campfire</h4>
                <h3 class="pb-3">Gather ’round the Campfire</h3>

                <p class="pb-3">This is the best place to hash things out quickly, throw links and images back and forth, think out loud, share news, riff, and chat in real-time with your team.</p>
            </div>
            <form>
                <div class="input-group mb-3">
                    <div class="input-group-prepend" style="width: 12%">
                        <button type="button" class="btn btn-outline-secondary"> <input style="width: 115%" type="file"></button>
                    </div>
                    <input type="text" class="form-control" aria-label="Text input with segmented dropdown button">
                </div>
            </form>
        </div>

    </div>

@endsection
