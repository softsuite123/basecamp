@extends('front.Master.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12 mr-auto ml-auto" >
                <div class="panel manage-panel panel--project box">

                    <header class="project-header project-header--for-home centered u-position-context">

                        <div class="project-header__details pt-5">

                            <h1 class="project-header__name--overview" data-bridge-header="">
                                <span class="project-header__name-text">Rebranding a business</span>
                            </h1>

                            <h4 class="project-header__description normal">

                            </h4>
                        </div>

                        <div class="project-avatars centered push_half--top push_quarter--bottom">
                            <div class="project-avatars__edit">
                                <a class="project-avatars__edit-button btn btn--small" data-bridge-menu-action="" data-bridge-action-type="people" data-hide-from-clients="true" title="Add or remove people…" href="/4987109/projects/20855546/people/users/edit">
                                    <span class="project-avatars__edit-text--full">Invite some people</span>
                                    <span class="project-avatars__edit-text--short">Add/remove</span>
                                </a>    </div>

                            <div class="project-avatars__overflow">
                                <div class="project-avatars__list avatar-group avatar-group--small" aria-label="People on this project">


                                    <img src="{{asset('assets/images/blank-avatar.png')}}" width="64" height="64">
                                </div>
                            </div>
                        </div>



                    </header>

                    <section class="project-dock centered">
                        <div class="card-grid card-grid--6-up" data-behavior="sortable_cards">

                            <div class="row">
                                <article id="recording_3472426320" class="card card--message_board card--app card--docked" data-behavior="card sortable_card_handle" data-controller="recording-color-picker" data-url="/4987109/buckets/20855546/message_boards/3472426320" data-position-url="/4987109/buckets/20855546/recordings/3472426320/position" data-filing-url="/4987109/buckets/20855546/recordings/3472426320/filing" data-rich-avatar-suppressed="true" data-lightbox-suppressed="true">
                                    <a class="card__link" href="{{route('front.message-board')}}"><div class="card__content">
                                            <header class="card__header centered">
                                                <h1 class="card__heading flush txt--truncate">
                                                    <span class="card__title">Message Board</span>
                                                </h1>
                                            </header>

                                            <section class="card__body card__body--blank push_half--top">

                                                <div class="card__blank-slate text-center">
                                                    <div class="card__icon card__icon--messages">
                                                        <img style="width: 30%" src="{{asset('assets/images/message-bg-3362b580bdf21b3b47eed9b7ca9de3c3e279e52a9627bc1646b843442cdacea5.svg')}}">
                                                    </div>
                                                    <div class="card__description align--center pt-2">
                                                        <p class="flush">
                                                            Post announcements, pitch ideas, progress updates, etc. and keep feedback on-topic.
                                                        </p>
                                                    </div>
                                                </div>

                                                <div class="card__body-content">
                                                </div>
                                            </section>
                                        </div></a><div class="card__drag-handle u-display-n" data-behavior="sortable_card_handle"></div></article>

                                <article id="recording_3472426321" class="card card--todoset card--app card--docked" data-behavior="card sortable_card_handle" data-controller="recording-color-picker" data-url="/4987109/buckets/20855546/todosets/3472426321" data-position-url="/4987109/buckets/20855546/recordings/3472426321/position" data-filing-url="/4987109/buckets/20855546/recordings/3472426321/filing" data-rich-avatar-suppressed="true" data-lightbox-suppressed="true">
                                    <a class="card__link" href="{{route('front.to-do')}}"><div class="card__content">
                                            <header class="card__header centered">
                                                <h1 class="card__heading flush txt--truncate">
                                                    <span class="card__title">To-dos</span>
                                                </h1>
                                            </header>

                                            <section class="card__body card__body--blank  push_half--top">
                                                <div class="card__blank-slate text-center">
                                                    <div class="card__icon card__icon--messages">
                                                        <img style="width: 30%" src="{{asset('assets/images/todo-bg-89bef7864666747be6cfb329bb4c4018c0d75a87fa7da93f2c7d5dd1ac4fac29.svg')}}">
                                                    </div>
                                                    <div class="card__description align--center pt-2">
                                                        <p class="flush">
                                                            Make lists of work that needs to get done, assign items, set due dates, and discuss.
                                                        </p>
                                                    </div>
                                                </div>

                                                <div class="card__body-content">

                                                    <object>
                                                    </object>
                                                </div>
                                            </section>
                                        </div></a><div class="card__drag-handle u-display-n" data-behavior="sortable_card_handle"></div></article>
                                <article id="recording_3472426322" class="card card--vault card--app card--docked" data-behavior="card sortable_card_handle" data-controller="recording-color-picker" data-url="/4987109/buckets/20855546/vaults/3472426322" data-position-url="/4987109/buckets/20855546/recordings/3472426322/position" data-filing-url="/4987109/buckets/20855546/recordings/3472426322/filing" data-rich-avatar-suppressed="true" data-lightbox-suppressed="true">
                                    <a class="card__link" href="{{route('front.docs-files')}}"><div class="card__content">
                                            <header class="card__header centered">
                                                <h1 class="card__heading flush txt--truncate">
                                                    <span class="card__title">Docs &amp; Files</span>
                                                </h1>
                                            </header>

                                            <section class="card__body card__body--blank centered push_half--top">
                                                <div class="card__blank-slate text-center">
                                                    <div class="card__icon card__icon--messages">
                                                        <img style="width: 30%" src="{{asset('assets/images/document-bg-d01e203dbb9d4c8550c004fcc534c5e1fff6086c65864f0e81d612b49f4294a0.svg')}}">
                                                    </div>
                                                    <div class="card__description align--center pt-2">
                                                        <p class="flush">
                                                            Share docs, files, images, and spreadsheets. Organize in folders so they’re easy to find.
                                                        </p>
                                                    </div>
                                                </div>

                                                <div class="card__body-content">
                                                    <object>
                                                        <div class="card-grid card-grid--miniaturized" data-behavior="sortable_cards_disabled">
                                                        </div>
                                                    </object>
                                                </div>
                                            </section>
                                        </div></a><div class="card__drag-handle u-display-n" data-behavior="sortable_card_handle"></div></article>
                                <article id="recording_3472426323" class="card card--chat card--app card--docked" data-behavior="card sortable_card_handle" data-controller="recording-color-picker" data-url="/4987109/buckets/20855546/chats/3472426323" data-position-url="/4987109/buckets/20855546/recordings/3472426323/position" data-filing-url="/4987109/buckets/20855546/recordings/3472426323/filing" data-rich-avatar-suppressed="true" data-lightbox-suppressed="true">
                                    <a class="card__link" href="{{route('front.campfire')}}"><div class="card__content">
                                            <header class="card__header centered">
                                                <h1 class="card__heading flush txt--truncate">
                                                    <span class="card__title">Campfire</span>
                                                </h1>
                                            </header>

                                            <section class="card__body card__body--blank">
                                                <div class="card__blank-slate text-center">
                                                    <div class="card__icon card__icon--messages ">
                                                        <img style="width: 30%" src="{{asset('assets/images/chat-bg-e2534f29e044a125769b0b5d315755dbcb5955604fac2eb2667b3d6ea622d2c5.svg')}}">
                                                    </div>
                                                    <div class="card__description align--center pt-2">
                                                        <p class="flush">
                                                            Chat casually with the group, ask random questions, and share stuff without ceremony.
                                                        </p>
                                                    </div>
                                                </div>

                                                <div class="card__body-content push_half--top">
                                                    <div class="chat__room">
                                                    </div>
                                                </div>
                                            </section>
                                        </div></a><div class="card__drag-handle u-display-n" data-behavior="sortable_card_handle"></div></article>
                                <article id="recording_3472426324" class="card card--schedule card--app card--docked" data-behavior="card sortable_card_handle" data-controller="recording-color-picker" data-url="/4987109/buckets/20855546/schedules/3472426324" data-position-url="/4987109/buckets/20855546/recordings/3472426324/position" data-filing-url="/4987109/buckets/20855546/recordings/3472426324/filing" data-rich-avatar-suppressed="true" data-lightbox-suppressed="true">
                                    <a class="card__link" href="{{route('front.schedule')}}"><div class="card__content">
                                            <header class="card__header centered">
                                                <h1 class="card__heading flush txt--truncate">
                                                    <span class="card__title">Schedule</span>
                                                </h1>
                                            </header>

                                            <section class="card__body push_half--top card__body--blank" id="schedule_card_recording_3472426324" data-turbolinks-permanent="" data-controller="schedule-card" data-schedule-card-months-to-load="36" data-schedule-card-limit="8" data-schedule-card-single-event-class="schedule-card__agenda-view--single" data-schedule-card-overdue-todos-class="card__body--overdue-todos" data-schedule-card-blank-slate-class="card__body--blank" data-schedule-card-url="/4987109/buckets/20855546/schedules/3472426324/card">

                                                <div class="card__blank-slate text-center">
                                                    <div class="card__icon card__icon--messages ">
                                                        <img style="width: 30%" src="{{asset('assets/images/event-bg-86f8d497d3d1373900e41ad78b06e7bdb7cd3a0560b2f630521c0d28dc5644ce.svg')}}">
                                                    </div>
                                                    <div class="card__description align--center pt-2">
                                                        <p class="card__blank-slate-text--schedule flush">
                                                            Set important dates on a shared schedule. Subscribe to events in Google Cal, iCal, or Outlook.
                                                        </p>

                                                        <p class="card__blank-slate-text--overdue-todos flush">
                                                            There are no upcoming events on the Schedule, but you’ve got <strong class="txt--highlight" data-target="schedule-card.overdueTodosText">0 overdue to-do</strong>
                                                        </p>
                                                    </div>
                                                </div>


                                                <div class="card__body-content">
                                                    <section class="schedule-card__agenda-view card__body push_quarter--top" data-target="schedule-card.agenda">


                                                    </section>
                                                </div>
                                            </section>

                                        </div></a><div class="card__drag-handle u-display-n" data-behavior="sortable_card_handle"></div></article>
                                <article id="recording_3472426325" class="card card--questionnaire card--app card--docked" data-behavior="card sortable_card_handle" data-controller="recording-color-picker" data-url="/4987109/buckets/20855546/questionnaires/3472426325" data-position-url="/4987109/buckets/20855546/recordings/3472426325/position" data-filing-url="/4987109/buckets/20855546/recordings/3472426325/filing" data-rich-avatar-suppressed="true" data-lightbox-suppressed="true">
                                    <a class="card__link" href=""><div class="card__content">

                                        </div>
                                        <header class="card__header centered">
                                            <h1 class="card__heading flush txt--truncate">
                                                <span class="card__title">Automatic Check-ins</span>
                                            </h1>
                                        </header>

                                        <section class="card__body card__body--blank centered push_half--top">
                                            <div class="card__blank-slate">
                                                <div class="card__icon card__icon--messages text-center">
                                                    <img style="width: 23%" src="{{asset('assets/images/question-bg-22b02e57924978ed53d1c46934115df02c44feafb4e20f869f94bc23417e4b7d.svg')}}">
                                                </div>
                                                <div class="card__description align--center pt-2">
                                                    <p class="flush">
                                                        Create recurring questions so you don't have to pester your team about what’s going on.
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="card__body-content">
                                            </div>
                                        </section>
                                    </div></a><div class="card__drag-handle u-display-n" data-behavior="sortable_card_handle"></div></article>

                        </div>

                        <div class="push--top hide-from-clients ">
                            <a class="decorated decorated--matched" href="/4987109/buckets/20855546/dock/edit">Change tools (add Email Forwards)</a>

                        </div>
                    </section>


                    <h2 class="latest-activity__project-headline break push_one_and_a_half--top centered">
                        <span>Project Activity</span>
                    </h2>
                    <section class="latest-activity latest-activity--project app-mobile__pad_bottom" data-role="project-activity">
                        <bc-timeline>
                            <bc-infinite-page page="1" direction="down" trigger="bottom" class="" reached-infinity="">
                                <bc-grouped-dates data-infinite-page-container="">
                                    <header class="date_divider" data-grouped-dates-divider="" data-datetime="2021-02-13">
                                        <h3 class="break centered">
                                            <span>Saturday, February 13</span>
                                        </h3>
                                    </header>
                                    <div class="latest-activity__full-width">
                                        <article class="activity-item">
                                            <div class="activity-item__summary">
                                                <div class="activity-item__what centered pb-5">
                                                    <img width="64" height="53" class="img--sized" src="https://bc3-production-assets-cdn.basecamp-static.com/assets/logos/paperclip-logo-349bf403c234be9d67457f2825054e138f6d54affe467717f3da909cab741cba.svg">
                                                    <h3 class="flush">Welcome to Rebranding a business!</h3>

                                                    <div class="metadata">
                                                        name created this <time datetime="2021-02-13T06:28:41Z" data-local="time" data-format="%B %d, %Y" title="February 13, 2021 at 11:28am PST" data-localized="">February 13, 2021</time>
                                                    </div>

                                                    <p>
                                                        This is the place to do everything related to this project — make plans, discuss progress, and&nbsp;get&nbsp;work&nbsp;done.
                                                    </p>

                                                    <p>
                                                        <a class="decorated" target="_blank" href="https://basecamp.com/3/tools">Learn more about the tools above</a> •
                                                        <a class="decorated" target="_blank" href="https://basecamp.com/support">Need a hand with anything? We’re standing by!</a>
                                                    </p>

                                                    <p>Happy Basecamping!</p>
                                                </div>
                                            </div>
                                        </article>
                                    </div>

                                </bc-grouped-dates>
                            </bc-infinite-page>    </bc-timeline>
                    </section>

                </div>
            </div>
        </div>
    </div>
@endsection
