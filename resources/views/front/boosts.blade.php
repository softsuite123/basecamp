@extends('front.Master.app')
@section('content')
    <div class="container">
        <div class="box m-5">
            <div class="p-5 panel manage-panel panel--perma panel--perma-person app-ios__transparent" data-readable-identifier="">
                <header class="perma__header text-center perma__header--borderless push--bottom app-ios__hide">
                    <img  data-rich-avatar-url="/4987109people/34152775/rich_avatar" data-current-person-avatar="true" src="{{asset('assets/images/blank-avatar.png')}}" width="64" height="64" title="name" alt="">

                    <h1 class="mt-5 perma__title">
      <span class="u-position-context">
          There are no <img class="boosts-report__icon" src="https://bc3-production-assets-cdn.basecamp-static.com/assets/icons/boost-report-c70740d11b844a3540e85b6e23858e7468ee31badf7956346254e61fbfd8cb3f.svg"> Boosts just yet.
      </span>
                    </h1>
                </header>

                <section>
                    <div class="align--center text-center app-ios__hide">
                        <div class="boosts-report__date align--center flush--top push_half--bottom"></div>

                        <p>
                            When you post something to Basecamp, others can send you boosts (little notes of encouragement) to show that they enjoyed your post.
                            We’ll let you know when it happens.
                        </p>

                        <p>By the way — try posting this gif for guaranteed boosts!</p>

                        <img style="width: 50%" src="https://bc3-production-assets-cdn.basecamp-static.com/assets/blank_slates/confused_cat-387360b61f7b4aae549763f09f9967f60800518fb4a4abb54b23a935b94f8ccd.gif">
                    </div>

                    <div class="centered app-ios__show app-ios__blank_slate pt-5">
                        <p>When you post something to Basecamp, others can send you boosts (little notes of encouragement) to show that they enjoyed your post. Check back here to see them.</p>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
