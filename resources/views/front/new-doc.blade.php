@extends('front.Master.app')
@section('content')
    <div class="container">
        <div style="width: 90%; margin-bottom: 0px !important;" class="mr-auto ml-auto box m-5 pt-2 pb-2 ">
            <div class="row justify-content-center">
                <img class="mr-2" onclick="showBuckets()" id="show-bucket" style="width: 2%" src="{{asset('assets/images/tools-light-baf1f21b71388cd268e0415e8618b2d63b8f99461e58470cbeb2e61dcba60ca4.svg')}}">
                <a href="{{route('front.project-manage')}}"><h6 style="padding-top: 20px;text-decoration: underline; color: #2D69D9">Rebranding a business</h6></a>
            </div>
            <div id="buckets" style="display: none" class="row justify-content-center">
                <div class="bucket text-center">
                    <h4 style="padding: 3px" class="txt-size">Message Board</h4>
                    <img style="width: auto; padding: 2px" src="{{asset('assets/images/message-bg-3362b580bdf21b3b47eed9b7ca9de3c3e279e52a9627bc1646b843442cdacea5.svg')}}">
                </div>
                <div class="bucket text-center">
                    <h4 style="padding: 3px" class="txt-size">To-dos</h4>
                    <img style="width: auto; padding: 2px" src="{{asset('assets/images/todo-bg-89bef7864666747be6cfb329bb4c4018c0d75a87fa7da93f2c7d5dd1ac4fac29.svg')}}">
                </div>
                <div class="bucket text-center">
                    <h4 style="padding: 3px" class="txt-size">Docs & Files</h4>
                    <img style="width: auto; padding: 2px" src="{{asset('assets/images/document-bg-d01e203dbb9d4c8550c004fcc534c5e1fff6086c65864f0e81d612b49f4294a0.svg')}}">
                </div>
                <div class="bucket text-center">
                    <h4 style="padding: 3px" class="txt-size">Camp Fire</h4>
                    <img style="width: auto; padding: 2px" src="{{asset('assets/images/chat-bg-e2534f29e044a125769b0b5d315755dbcb5955604fac2eb2667b3d6ea622d2c5.svg')}}">
                </div>
                <div class="bucket text-center">
                    <h4 style="padding: 3px" class="txt-size">Schedule</h4>
                    <img style="width: auto; padding: 2px" src="{{asset('assets/images/event-bg-86f8d497d3d1373900e41ad78b06e7bdb7cd3a0560b2f630521c0d28dc5644ce.svg')}}">
                </div>
                <div class="bucket text-center">
                    <h4 style="padding: 3px" class="txt-size">Chech-Ins</h4>
                    <img style="width: auto; padding: 2px" src="{{asset('assets/images/question-bg-22b02e57924978ed53d1c46934115df02c44feafb4e20f869f94bc23417e4b7d.svg')}}">
                </div>
            </div>
        </div>
        <div class="box ml-auto mr-auto p-5">
            <form>
                <div class="form-group">
                    <textarea class="form-control bold-input" id="exampleFormControlTextarea1" placeholder="Type a title..."></textarea>
                </div>
                <div class="form-group">
                    <textarea name="editor1"></textarea>
                    <script>
                        CKEDITOR.replace( 'editor1' );
                    </script>
                </div>
                <div class="row">
                    <button class="btn btn-primary ml-5 message-draft-btn">Save as a Draft</button>
                    <button class="btn btn-primary ml-3">Post this document</button>
                </div>
            </form>
        </div>

    </div>

@endsection
