@extends('front.Master.app')
@section('content')
    <div class="container">
        @include('front.partials.bucket-head')
        <div class="box ml-auto mr-auto p-5">
            <form>
                <div class="form-group">
                    <select style="width: auto; border-radius: 25px" class="form-control p-1" id="exampleFormControlSelect1">
                        <option>Pick a category (optional)</option>
                    </select>
                </div>
                <div class="form-group">
                    <textarea class="form-control bold-input" id="exampleFormControlTextarea1" placeholder="Type a title..."></textarea>
                </div>
                <div class="form-group">
                    <textarea name="editor1"></textarea>
                    <script>
                        CKEDITOR.replace( 'editor1' );
                    </script>
                </div>
                <div class="row">
                    <button class="btn btn-primary ml-5 message-draft-btn">Save as a Draft</button>
                    <button class="btn btn-primary ml-3">Post this message</button>
                </div>
            </form>
        </div>

    </div>

@endsection
