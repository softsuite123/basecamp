@extends('front.Master.app')
@section('content')


@include('front.Master.partials.sidebar')




@php

$space = App\Http\Controllers\SpaceController::get();
$space->load('user');
// dd($space);
$namespace = get_class($space);
$space_url = strtolower(class_basename($space));



@endphp

<link rel="stylesheet" href="{{url('/')}}/assets/css/invitetocompany.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/fileshare.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/toast.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/chat.css">

<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="{{url('/')}}/assets/css/jquery.lsxemojipicker.css" rel="stylesheet">






<div class="actionpanel">
    <a href="{{url('/')}}/{{$space_url}}/controlpanel" class="link">
        <i class="fa fa-th-large"></i>
    </a>
    <div class="name">{{$space->name}} </div>
</div>
<div class="panelcard" id="app" style="padding: 0px;">

{{--

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="contact-list">

                </div>
            </div>
            <div class="col-md-8 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Chats</div>

                    <div class="panel-body">
                        <chat-messages :messages="messages"></chat-messages>
                    </div>
                    <div class="panel-footer">
                        <chat-form v-on:messagesent="addMessage" :user="{{ Auth::user() }}"></chat-form>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="chat-new">

        <div class="messaging" selecteduser="" >
            <div class="inbox_msg">
                {{-- <div class="inbox_people">
                    <div class="headind_srch">
                        <div class="recent_heading">
                            <h4>Chat</h4>
                        </div>
                        <div class="srch_bar">
                            <div class="stylish-input-group">
                                <input type="text" class="search-bar search-bar-input " placeholder="Search">
                                <span class="input-group-addon">
                                    <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                </span> </div>
                        </div>
                    </div>
                    <div class="inbox_chat">

                        @foreach ($space->user as $member)

                            @if($member->id != Auth::user()->id)

                                @php
                                    $member->load('messages');
                                    $lastmsg = $member->messages->where('space' , $namespace )->where('space_id' , $space->id)->whereIn(
                                        'with' , [$member->id , Auth::user()->id],
                                        )->whereIn('user_id'  , [$member->id , Auth::user()->id] )->last();
                                        // dd($messages);
                                @endphp

                                <div class="chat_list " @click="changechat({{$member->id}})" user_id="{{$member->id}}"  >
                                    <div class="chat_people">
                                        <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png"
                                                alt=""> </div>
                                        <div class="chat_ib">
                                            <h5>{{$member->name}}

                                            </h5>
                                            <p>{{@$lastmsg->message}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        @endforeach



                    </div>
                </div> --}}
                <div class="mesgs"  style="width:100%;">
                    <chat-messages :messages="messages" :auth="auth"></chat-messages>

                    <chat-form v-on:messagesent="addMessage" :user="{{ Auth::user() }}"></chat-form>



                </div>
            </div>




        </div>
    </div>



</div>




<script src="{{url('/')}}/assets/js/formvalidation.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{url('/')}}/assets/js/global/preloader.js"></script>
<script src="{{url('/')}}/assets/js/global/toast.js"></script>

<script src="https://twemoji.maxcdn.com/2/twemoji.min.js"></script>

<script>
    $(document).ready(function(){
        var firstuser = $('.chat_list').first();
        firstuser.addClass('active_chat');
        // console.log(firstuser.attr('userid'));
        $('.messaging').attr("selecteduser" , firstuser.attr('user_id'));


        $('.chat_list').click(function(){
            $('.chat_list').removeClass('active_chat');
            $(this).addClass('active_chat');

            $('.messaging').attr("selecteduser" , $(this).attr('user_id'));

            // changechat();

        });

        // search
        $(".search-bar-input").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".inbox_chat div").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



<script src="{{url('/')}}/assets/js/jquery.lsxemojipicker.js"></script>
    <script>
        // $('.emojipicker').lsxEmojiPicker({
        //     closeOnSelect: true,
        //     twemoji: true,
        //     onSelect: function(emoji){
        //         console.log(emoji);
        //         $('body').find('.chatinp').val(emoji.value);
        //     }
        // });
	</script>
<script src="{{url('/')}}/js/groupchat.js"></script>



@endsection
