<div class="leet-sidebar leet-sidebar-show" style="">

    <div class="menu-item {{\Request::is('home') ? 'active' : ''}}" route="{{url("/home")}}">
        <div class="icon">
            <img src="{{url('/')}}/assets/images/home.png" alt="Home">
        </div>
        <div class="label">Home</div>
    </div>

    <div class="menu-item" route="/">
        <div class="icon">
            <img src="{{url('/')}}/assets/images/notification.png" alt="Home">
        </div>
        <div class="label">Pings</div>
    </div>

    <div class="menu-item" onclick="openglobalsearch()">
        <div class="icon">
            <img src="{{url('/')}}/assets/images/search.png" alt="Home">
        </div>
        <div class="label">Search</div>
    </div>

    <div class="menu-item" data-toggle="modal" data-target="#dmuser">
        <div class="icon">
            <img src="{{url('/')}}/assets/images/drawer.png" alt="Home">
        </div>
        <div class="label">Hey!</div>
    </div>


    <div class="menu-item {{\Request::is('activity') ? 'active' : ''}}" route="/activity">
        <div class="icon">
            <img src="{{url('/')}}/assets/images/activity.png" alt="Activity">
        </div>
        <div class="label">Activity</div>
    </div>

    @php
        $stripe = \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $session = \Stripe\BillingPortal\Session::create([
            'customer' => Auth::user()->stripe_account,
            'return_url' => url('/').'/home',
        ]);


    @endphp

    <div class="menu-item " route="{{$session->url}}">
        <div class="icon">
            <img src="{{url('/')}}/assets/images/payment-icon.png" alt="Activity">
        </div>
        <div class="label">Account</div>
    </div>


    <div class="menu-item" route="/logout">
        <div class="icon">
            <img src="{{url('/')}}/assets/images/logout-icon.png" alt="Logout">
        </div>
        <div class="label">Logout</div>
    </div>






</div>


<script>
    $(document).ready(function(){

        if($(window).width() >1080){
            $('.leet-sidebar').removeClass('leet-sidebar-show');

        }
        else{
            $('.leet-sidebar').addClass('leet-sidebar-show');

        }

        $(window).resize(function(){
            // your code here
            if(this.width() >1080){
                $('.leet-sidebar').removeClass('leet-sidebar-show');

            }
            else{
                $('.leet-sidebar').addClass('leet-sidebar-show');

            }
        });


        $('.sidebar-toggle').click(function(){
            $('.leet-sidebar').toggleClass('leet-sidebar-show');
        });

    })

</script>
