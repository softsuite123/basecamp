@extends('front.Master.app')
@section('content')
    @include('front.partials.report-head')
        <div class="container box">
            <div class="text-center pt-5">
                <h4>Upcoming Dates</h4>
                <hr class="p-1" width="80%" style="border-width: 3px; border-color: #212529">
            </div>
            <div class="mr-auto ml-auto" style="width: 100%">
                <div class="pb-5 row">
                        <div class="ml-auto" id="calendarContainer"></div>
                        <div class="mr-auto" id="organizerContainer" style="margin-left: 8px;"></div>

                    </div>
                </div>
            </div>
        </div>
@endsection
