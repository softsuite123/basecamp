@extends('front.Master.app')
@section('content')
    <div class="container">
        @include('front.partials.bucket-head')
        <div class="box ml-auto mr-auto p-5">
            <div class="text-center pb-3">
                <h2 class="pb-5">Schedule</h2>
            </div>

        <div class="mr-auto ml-auto" style="width: 100%">
            <div class="pb-5 row">
                <div class="ml-auto" id="calendarContainer"></div>
                <div class="mr-auto" id="organizerContainer" style="margin-left: 8px;"></div>

            </div>
        </div>
        </div>
    </div>

@endsection
