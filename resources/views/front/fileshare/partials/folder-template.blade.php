<div class="grid-item folder" folderid="{{$folder->id}}" foldercode="{{$folder->code}}" >
    <div class="cardtitle">
        <div class="setting-icon">
            <i class="fa fa-cog"></i>
        </div>


        <div class="menu">

            @owner($folder)
            <div class="item delete ">
                <i class="fa fa-trash"></i>
                <label for="">Delete</label>
            </div>
            @endowner


{{--
            <div class="item">
                <i class="fa fa-clone"></i>
                <label for="">Copy</label>
            </div>

            <div class="item">
                <i class="fa fa-arrows"></i>
                <label for="">Move</label>
            </div> --}}


        </div>

        <div href="#" class="title">
            <a href="{{url('/')}}/fileshare/folder/{{$folder->code}}"> {{$folder->name}} </a>
        </div>



    </div>
    <div class="body">
        <div class="placeholder">
            <div class="folder-icon" route="{{url('/')}}/fileshare/folder/{{$folder->code}}">
                <i class="fa fa-folder" > </i>
            </div>
        </div>
    </div>
</div>
