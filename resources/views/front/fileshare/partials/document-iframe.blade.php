<style>
    body,html{
        margin: 0px;
        padding: 0px;
        box-sizing: border-box;
    }

    .document-display{
        border: 1px solid lightgray;
        border-radius:10px;
        /* width : 100vw; */
        padding: 20px;
        /* min-height : 400px; */
        margin-top: 25px;
        transform: scale(0.6);


    }
    .document-preview{
        width: 100%;
        display :grid;
        align-items : center;

    }
</style>

<div class="document-display">
    <div class="document-preview">
        {!! $document->document !!}
    </div>
</div>
