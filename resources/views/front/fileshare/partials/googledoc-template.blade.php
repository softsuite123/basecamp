<div class="grid-item google-doc" documentid="{{$document->id}}" documentcode="{{$document->code}}" >
    <div class="cardtitle">
        <div class="setting-icon">
            <i class="fa fa-cog"></i>
        </div>


        <div class="menu">

            @owner($document)
            <div class="item delete ">
                <i class="fa fa-trash"></i>
                <label for="">Delete</label>
            </div>
            @endowner


{{--
            <div class="item">
                <i class="fa fa-clone"></i>
                <label for="">Copy</label>
            </div>

            <div class="item">
                <i class="fa fa-arrows"></i>
                <label for="">Move</label>
            </div> --}}


        </div>

        <div href="#" class="title">
            <a href="{{url('/')}}/fileshare/google-doc/{{$document->code}}"> {{$document->name}} </a>
        </div>



    </div>
    <div class="body">
        <div class="placeholder">
            <div class="file-view">

                {{-- <iframe src="{{$document->link}}" style="width:100%; height:100%;" frameborder="0"></iframe> --}}

                <img  src="{{url('/')}}/assets/images/google-drive-icon.png" class="image-thumb" style="height:70px;width:auto;margin:0 auto;" alt="">
            </div>
        </div>
    </div>
</div>
