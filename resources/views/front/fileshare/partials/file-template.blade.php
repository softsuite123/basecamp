<div class="grid-item file" fileid="{{$file->id}}" filecode="{{$file->file_code}}" >
    <div class="cardtitle">
        <div class="setting-icon">
            <i class="fa fa-cog"></i>
        </div>


        <div class="menu">

            @owner($file)
            <div class="item delete ">
                <i class="fa fa-trash"></i>
                <label for="">Delete</label>
            </div>
            @endowner


{{--
            <div class="item">
                <i class="fa fa-clone"></i>
                <label for="">Copy</label>
            </div>

            <div class="item">
                <i class="fa fa-arrows"></i>
                <label for="">Move</label>
            </div> --}}


        </div>

        <div href="#" class="title">
            <a href="{{url('/')}}/fileshare/file/{{$file->file_code}}"> {{$file->file_name}} </a>
        </div>



    </div>
    <div class="body">
        <div class="placeholder">
            <div class="file-view">

                <img src="{{url('/')}}/vault/{{$file->path}}" class="image-thumb" alt="">
            </div>
        </div>
    </div>
</div>
