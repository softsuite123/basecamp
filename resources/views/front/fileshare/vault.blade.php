@extends('front.Master.app')
@section('content')


@include('front.Master.partials.sidebar')




@php

$space = App\Http\Controllers\SpaceController::get();
$space->load('user');
// dd($space);
$namespace = get_class($space);
$space_url = strtolower(class_basename($space));



@endphp

<link rel="stylesheet" href="{{url('/')}}/assets/css/invitetocompany.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/fileshare.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/toast.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />




<div class="actionpanel">
    <a href="{{url('/')}}/{{$space_url}}/controlpanel" class="link">
        <i class="fa fa-th-large"></i>
    </a>
    <div class="name">{{$space->name}}</div>
</div>
<div class="panelcard">
    <div class="header">
        <div class="btn btn-success addbtn " style="background-color: #2dca81 !important; width:120px;">
            <i class="fa fa-plus"></i> Add
        </div>


        <div class="select-menu">

            <div class="item  adddoctrigger" data-toggle="modal" data-target="#adddoc">
                <i class="fa fa-file"></i>
                <label for="">Make Document</label>
            </div>

            <div class="item " data-toggle="modal" data-target="#addfolder">
                <i class="fa fa-folder"></i>
                <label for="">Make Folder</label>
            </div>

            <div class="item " data-toggle="modal" data-target="#addfile">
                <i class="fa fa-upload"></i>
                <label for="">Upload File </label>
            </div>

            <div class="item  " data-toggle="modal" data-target="#googledoc">
                <i class="fa fa-cloud-download"></i>
                <label for="">Google Drive </label>
            </div>





        </div>


        <div class="title">
            Files & Docs
            {{-- {{$list->name}} --}}
        </div>

        {{-- <select class="form-control" name="todo-type" id="todo-type" style="width:fit-content;">
            <option value="all-todos">All</option>
            <option value="my-todos">My Todos</option>
        </select> --}}
    </div>


    <div class="body">



        <div class="task-grid vault ">




            @foreach(\App\Models\Folder::where(['space_id' => $space->id , 'space' => $namespace , 'folder_id' =>0])->get() as $folder)
                @include('front.fileshare.partials.folder-template' , ['folder' => $folder])
            @endforeach

            @foreach(\App\Models\File::where(['space_id' => $space->id , 'space' => $namespace , 'folder_id' =>0])->get() as $file)
                @hasAccess($file)
                    @include('front.fileshare.partials.file-template' , ['file' => $file])
                @endhasAccess
            @endforeach


            @foreach(\App\Models\Document::where(['space_id' => $space->id , 'space' => $namespace , 'folder_id' =>0])->get() as $document)
                @hasAccess($document)
                    @include('front.fileshare.partials.document-template' , ['document' => $document])
                @endhasAccess()
            @endforeach

            @foreach(\App\Models\GoogleDoc::where(['space_id' => $space->id , 'space' => $namespace , 'folder_id' =>0])->get() as $document)
                @hasAccess($document)
                    @include('front.fileshare.partials.googledoc-template' , ['document' => $document])
                @endhasAccess()
            @endforeach









        </div>













    </div>






</div>


{{-- modals --}}


{{-- Add folder --}}


<div class="modal fade" id="addfolder" tabindex="-1" role="dialog" aria-labelledby="addfolder" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Folder</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="addfolderform">

                <div class="modal-body">

                    <div class="form-group">
                        <label for=""></label>
                        <input type="text" name="name" id="" class="form-control inp" placeholder="Folder Name"
                            required="true">
                    </div>


                    <input type="text" style="display:none;" class="inp" name="folder_id"
                        value="{{isset($_GET['folderid']) ? $_GET['folderid'] : 0 }}">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeaddfolderform"
                        data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary addfolderbtn">Add Folder</button>
                </div>
            </form>
        </div>
    </div>
</div>



{{-- Add file --}}


<div class="modal fade" id="addfile" role="dialog" aria-labelledby="addfile" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="addfileform" enctype="multipart/form-data" method="Post">

                <div class="modal-body">

                    <div class="form-group">
                        <label label for="">File Name</label>
                        <input type="text" name="filename" id="" class="form-control inp" placeholder="File Name"
                            required="true">

                    </div>


                    <div class="form-group">
                        <label label for="">Upload File</label>
                        <input type="file" name="file" id="" class="form-control inp filetoupload" placeholder="file"
                            required="true">
                        <div class="file-preview" style="padding: 20px;">

                        </div>
                    </div>

                    <div class="form-group">
                        <label label for="">Select Who Can Access</label>
                        <select name="users[]" id="" class="form-control user-access" style="width:100%;" multiple>
                            @foreach ($space->user as $user)
                            <option value="{{$user->id}}"> {{$user->name}} ({{$user->email}})</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label label for="fileselectall">Select All</label>
                        <input type="checkbox" id="fileselectall" name="selectall">
                    </div>



                    <input type="text" style="display:none;" class="inp" name="folder_id"
                        value="{{isset($_GET['folderid']) ? $_GET['folderid'] : 0 }}">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeaddfileform" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary addfilebtn">Add File</button>
                </div>
            </form>
        </div>
    </div>
</div>



{{-- Add Google document --}}


<div class="modal fade" id="googledoc" role="dialog" aria-labelledby="googledoc" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Google Doc</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="addgoogledocform" enctype="multipart/form-data" method="Post">
                @csrf
                <div class="modal-body">

                    <div class="form-group">
                        <label label for="">File Name</label>
                        <input type="text" name="name" id="" class="form-control inp" placeholder="File Name"
                            required="true">

                    </div>


                    <div class="form-group">
                        <label label for="">Document Link</label>
                        <input type="text" name="link" id="" class="form-control inp " placeholder="Document link"
                            required="true">

                    </div>

                    <div class="form-group">
                        <label label for="">Select Who Can Access</label>
                        <select name="users[]" id="" class="form-control user-access" style="width:100%;" multiple>
                            @foreach ($space->user as $user)
                            <option value="{{$user->id}}"> {{$user->name}} ({{$user->email}})</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label label for="fileselectall">Select All</label>
                        <input type="checkbox" id="fileselectall" name="selectall">
                    </div>



                    <input type="text" style="display:none;" class="inp" name="folder_id"
                        value="{{isset($_GET['folderid']) ? $_GET['folderid'] : 0 }}">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closegoogleadddocform" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary addgoogledocbtn">Add Document</button>
                </div>
            </form>
        </div>
    </div>
</div>





{{-- Add Doc --}}


<div class="modal fade" id="adddoc" role="dialog" aria-labelledby="adddoc" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 70%; margin: 0 auto;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Document</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="adddocform" enctype="multipart/form-data" method="Post">

                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name of Document</label>
                        <input type="text" name="name" id="name" class="form-control inp" required="true"
                            placeholder="Name">

                    </div>

                    <div class="form-group">
                        <label label for="">Select Who Can Access</label>
                        <select name="users[]" id="" class="form-control user-access" style="width:100%;" multiple>
                            @foreach ($space->user as $user)
                            <option value="{{$user->id}}"> {{$user->name}} ({{$user->email}})</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label label for="fileselectall">Select All</label>
                        <input type="checkbox" id="fileselectall" name="selectall">
                    </div>

                    <div class="form-group">
                        {{-- <label label for="">File Name</label> --}}
                        <textarea name="document" class="inp" id="document-editor" rows="50">

                        </textarea>
                    </div>



                    <input type="text" style="display:none;" class="inp" name="folder_id"
                        value="{{isset($_GET['folderid']) ? $_GET['folderid'] : 0 }}">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closegoogleadddocform" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary adddocbtn">Add Document</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="{{url('/')}}/assets/js/formvalidation.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{url('/')}}/assets/js/global/preloader.js"></script>
<script src="{{url('/')}}/assets/js/global/toast.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- <script type="text/javascript">

    document.cookie = "AC-C=ac-c;expires=Fri, 31 Dec 9999 23:59:59 GMT;path=/;HttpOnly;SameSite=None";
  </script> --}}

<script src="https://cdn.tiny.cloud/1/4e17hcsz7xaiwpc4kl341dgdyg51t6izulk882z4k4af675c/tinymce/5/tinymce.min.js">
</script>


<script>
    tinymce.init({
        selector: '#document-editor',
        plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        toolbar_mode: 'floating',
    });
    $('document').ready(function () {
        $(document).ready(function () {
            $('.user-access').select2();
        });

        // $('.adddoctrigger').click(function () {
        //     tinymce.init({
        //         selector: 'document-editor',
        //         plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        //         toolbar_mode: 'floating',
        //     });

        // });

        // $('#document-editor').on('hide.bs.modal', function () {
        //     // scope the selector to the modal so you remove any editor on the page underneath.
        //     tinymce.remove('#document-editor textarea');
        // });



        // add Google Document ------------------------
        $('.addgoogledocbtn').click(function () {

            var validation = validateform('#addgoogledocform');
            tinymce.triggerSave();
            var fd = new FormData($('#addgoogledocform')[0]);
            // console.log(validation);
            // return;

            if (validation.errors != 0) {
                toast({
                    message: "Document Name is required!",
                    type: 'warning',
                    icon: 'fa fa-times'
                });
                return;
            }

            $('.closegoogleadddocform').trigger('click');

            preloader(true);

            $.ajax({
                type: "POST",
                url: "{{url('/')}}/fileshare/google-doc/new",
                data: fd,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    console.log(res);
                    // return ;
                    preloader(false);
                    // return;
                    if (res.status == 200) {

                        $('.vault').append(res.ajax);

                        toast({
                            message: `${validation.data.name} added successfully!`,
                            type: 'success',
                            icon: 'fa fa-check'
                        });

                    }
                }
            });
        });

        // delete  Google Document
        $('.google-doc .delete').click(function () {
            var parent = $(this).parent().parent().parent();
            var documentid = parent.attr('documentid');
            var documentcode = parent.attr('documentcode');

            // console.log(parent.html());
            // return;

            swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this document!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        preloader(true);

                        $.ajax({
                            type: "POST",
                            url: `{{url('/')}}/fileshare/google-doc/delete`,
                            data: {
                                code: documentcode,
                                _token: "{{csrf_token()}}",

                            },
                            success: function (res) {
                                console.log(res);
                                if (res.code == 200) {
                                    preloader(false);
                                    $(`.google-doc[documentcode=${documentcode}]`).remove();

                                    toast({
                                        message: `Document deleted successfully!`,
                                        type: 'success',
                                        icon: 'fa fa-check'
                                    });

                                    // swal("Poof! Your file has been deleted!", {
                                    //     icon: "success",
                                    // });
                                }
                            }

                        });



                    } else {
                        swal("Your file is safe!");
                    }
                });

        });




        // add Document ------------------------
        $('.adddocbtn').click(function () {

            var validation = validateform('#adddocform');
            tinymce.triggerSave();
            var fd = new FormData($('#adddocform')[0]);
            // console.log(validation);
            // return;

            if (validation.errors != 0) {
                toast({
                    message: "Document Name is required!",
                    type: 'warning',
                    icon: 'fa fa-times'
                });
                return;
            }

            $('.closeadddocform').trigger('click');

            preloader(true);

            $.ajax({
                type: "POST",
                url: "{{url('/')}}/fileshare/document/new",
                data: fd,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    console.log(res);
                    preloader(false);
                    // return;
                    if (res.status == 200) {

                        $('.vault').append(res.ajax);

                        toast({
                            message: `${validation.data.name} added successfully!`,
                            type: 'success',
                            icon: 'fa fa-check'
                        });

                    }
                }
            });
        });

        // delete Document
        $('.document .delete').click(function () {
            var parent = $(this).parent().parent().parent();
            var documentid = parent.attr('documentid');
            var documentcode = parent.attr('documentcode');

            // console.log(parent.html());
            // return;

            swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this document!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        preloader(true);

                        $.ajax({
                            type: "POST",
                            url: `{{url('/')}}/fileshare/document/delete`,
                            data: {
                                code: documentcode,
                                _token: "{{csrf_token()}}",

                            },
                            success: function (res) {
                                console.log(res);
                                if (res.code == 200) {
                                    preloader(false);
                                    $(`.document[documentcode=${documentcode}]`)
                                    .remove();

                                    toast({
                                        message: `Document deleted successfully!`,
                                        type: 'success',
                                        icon: 'fa fa-check'
                                    });

                                    // swal("Poof! Your file has been deleted!", {
                                    //     icon: "success",
                                    // });
                                }
                            }

                        });



                    } else {
                        swal("Your file is safe!");
                    }
                });

        });


        // add folder ------------------------
        $('.addfolderbtn').click(function () {

            var validation = validateform('#addfolderform');
            // console.log(validation);
            // return;

            if (validation.errors != 0) {
                toast({
                    message: "Folder Name is required!",
                    type: 'warning',
                    icon: 'fa fa-times'
                });
                return;
            }

            $('.closeaddfolderform').trigger('click');

            preloader(true);

            $.ajax({
                type: "POST",
                url: "{{url('/')}}/fileshare/folder/new",
                data: {
                    name: validation.data.name,
                    folderid: validation.data.folder_id,
                    "_token": "{{csrf_token()}}"
                },
                success: function (res) {
                    console.log(res);
                    preloader(false);
                    // return;
                    if (res.status == 200) {

                        $('.vault').append(res.ajax);

                        toast({
                            message: `${validation.data.name} added successfully!`,
                            type: 'success',
                            icon: 'fa fa-check'
                        });

                    }
                }
            });
        });

        // delete
        $('.folder .delete').click(function () {
            var parent = $(this).parent().parent().parent();
            var folderid = parent.attr('folderid');
            var foldercode = parent.attr('foldercode');

            // console.log(parent.html());
            // return;

            swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this file!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        preloader(true);

                        $.ajax({
                            type: "POST",
                            url: `{{url('/')}}/fileshare/folder/delete`,
                            data: {
                                folder_code: foldercode,
                                _token: "{{csrf_token()}}",

                            },
                            success: function (res) {
                                console.log(res);
                                if (res.code == 200) {
                                    preloader(false);
                                    $(`.folder[foldercode=${foldercode}]`).remove();

                                    toast({
                                        message: `Folder deleted successfully!`,
                                        type: 'success',
                                        icon: 'fa fa-check'
                                    });

                                    // swal("Poof! Your file has been deleted!", {
                                    //     icon: "success",
                                    // });
                                }
                            }

                        });



                    } else {
                        swal("Your file is safe!");
                    }
                });

        });

        //  upload file ------------------------------
        $('.filetoupload').change(function () {
            // var file= $(this).files[0];
            var filepreview = $('.file-preview');
            // var preview = document.querySelector('#preview');
            var files = document.querySelector('input[type=file]').files;

            function readAndPreview(file) {

                // Make sure `file.name` matches our extensions criteria
                if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
                    var reader = new FileReader();

                    reader.addEventListener("load", function () {
                        var image = new Image();
                        // image.height = 200;
                        image.title = file.name;
                        image.src = this.result;
                        // preview.appendChild( image );
                        filepreview.append(image);
                    }, false);

                    reader.readAsDataURL(file);
                }

            }

            if (files) {
                [].forEach.call(files, readAndPreview);
            }


        });


        $('.addfilebtn').click(function () {


            var validation = validateform('#addfileform');
            console.log(validation);
            var fd = new FormData($('#addfileform')[0]);
            // console.log();
            // var file = fd.get('file');
            fd.set('_token', "{{csrf_token()}}");

            // return;

            if (validation.errors != 0) {
                toast({
                    message: "Folder Name is required!",
                    type: 'warning',
                    icon: 'fa fa-cross'
                });
                return;
            }

            $('.closeaddfileform').trigger('click');

            preloader(true);

            $.ajax({
                type: "POST",
                url: "{{url('/')}}/fileshare/file/new",
                // contentType: "application/x-www-form-urlencoded",
                cache: false,
                processData: false,
                contentType: false,
                data: fd,
                success: function (res) {
                    console.log(res);
                    preloader(false);

                    // return;
                    if (res.status == 200) {

                        $('.vault').append(res.ajax);

                        toast({
                            message: `${validation.data.filename} added successfully!`,
                            type: 'success',
                            icon: 'fa fa-check'
                        });

                    }
                }
            });

        });

        // delete file
        $('.file .delete').click(function () {
            var parent = $(this).parent().parent().parent();
            var fileid = parent.attr('fileid');
            var filecode = parent.attr('filecode');

            // console.log(parent.html());
            // return;

            swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this file!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {

                        preloader(true);

                        $.ajax({
                            type: "POST",
                            url: `{{url('/')}}/fileshare/file/delete`,
                            data: {
                                file_code: filecode,
                                _token: "{{csrf_token()}}",

                            },
                            success: function (res) {
                                console.log(res);
                                if (res.code == 200) {
                                    preloader(false);
                                    $(`.file[filecode=${filecode}]`).remove();

                                    toast({
                                        message: `File deleted successfully!`,
                                        type: 'success',
                                        icon: 'fa fa-check'
                                    });

                                    // swal("Poof! Your file has been deleted!", {
                                    //     icon: "success",
                                    // });
                                }
                            }

                        });



                    } else {
                        swal("Your file is safe!");
                    }
                });

        });





    });

</script>


@endsection
