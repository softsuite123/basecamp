@extends('front.Master.app')
@section('content')


@include('front.Master.partials.sidebar')




@php

$space = App\Http\Controllers\SpaceController::get();
$space->load('user');
$namespace = get_class($space);
$space_url = strtolower(class_basename($space));
// dd($file);
// $folderparent = $folder ;

@endphp

<link rel="stylesheet" href="{{url('/')}}/assets/css/invitetocompany.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/fileshare.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/toast.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />



<div class="actionpanel">
    <a href="{{url('/')}}/{{$space_url}}/controlpanel" class="link">
        <i class="fa fa-th-large"></i>
    </a>
    <div class="name">{{$space->name}}</div>
</div>
<div class="panelcard">
    <div class="header">

        @hasAccess($file)
            <div class="btn btn-success addbtn " style="background-color: #ffffff !important; color: black;width:120px;">
                <i class="fa fa-cog"></i> Setting
            </div>
        @endhasAccess


        <div class="select-menu">

            @owner($file)

            <div class="item  " data-toggle="modal" data-target="#copy">
                <i class="fa fa-clone"></i>
                <label for="">Copy</label>
            </div>

            <div class="item "  data-toggle="modal" data-target="#move">
                <i class="fa fa-arrows"></i>
                <label for="">Move</label>
            </div>

            <div class="item "  data-toggle="modal" data-target="#adduseraccess">
                <i class="fa fa-user"></i>
                <label for="">Add Access</label>
            </div>
            @endowner

            <a class="item " download href="{{url('/')}}/vault/{{$file->path}}" >
                <i class="fa fa-download"></i>
                <label for="">Download</label>
            </a>

            @owner($file)

            <div class="item filedelete"  >
                <i class="fa fa-trash"></i>
                <label for="">Delete </label>
            </div>

            @endowner







        </div>


        <div class="title">

            {{$file->file_name}}
        </div>

        {{-- <select class="form-control" name="todo-type" id="todo-type" style="width:fit-content;">
            <option value="all-todos">All</option>
            <option value="my-todos">My Todos</option>
        </select> --}}
    </div>


    <div class="body">



        <div class="file-display">
            <div class="file-preview">
                <img src="{{url('/')}}/vault/{{$file->path}}" route="{{url('/')}}/vault/{{$file->path}}" alt="" class="image">
            </div>
        </div>


         {{-- User List --}}

        @php

        @endphp

        <div class="userslist">







            <div class="coastline"></div>
            <div class="title">Access To ({{$file->file_name}}) </div>

            @php
                $file->load('users');
                // $company_users = $file->users;
                // dd($file);
            @endphp

            @foreach ($file->users as $member)

                    {{-- Company Member --}}
                    <div class="user">
                        <div class="profile">
                            <img src="{{url('/')}}/assets/images/avatar-male.png" alt="" class="img">
                        </div>
                        <div class="info">
                            <div class="name">{{$member->name}}</div>
                            <div class="designation"> @if($file->user_id == $member->id) Owner @endif </div>
                            <div class="email">{{$member->email}}</div>
                            <div class="actions">
                                @if ( Auth::user()->id == $file->user_id   )

                                    {{-- <button class="btn btn-primary action">
                                        <i class="fa fa-pencil"></i>
                                    </button> --}}
                                    <a href="{{url('/')}}/fileshare/file/removeuser/{{$file->id}}/{{$member->id}}">

                                        <button class="btn btn-danger action ">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </a>
                                    {{-- <button class="btn btn-success action">
                                        <i class="fa fa-share-alt"></i>
                                    </button> --}}
                                @endif
                            </div>
                        </div>
                    </div>

            @endforeach




        </div>


    </div>






</div>



{{-- Copy --}}


<div class="modal fade" id="copy" tabindex="-1" role="dialog" aria-labelledby="copy"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Copy To</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="addfileform" enctype="multipart/form-data" method="Post">

                <div class="modal-body">

                    <div class="form-group">
                        <label label for="">Folder  </label>
                        <button type="button" class="btn-prefolder" > Go back </button>


                    </div>



                    <div class="copy-folder-row folder-row" prefolder="0">
                        {{-- <div class="folder " folder-id="0">
                            <div class="icon"><i class="fa fa-folder"></i></div>
                            <div class="name">Root</div>
                        </div> --}}
                        @foreach (App\Models\Folder::where(['folder_id' => 0 , 'space' => $namespace , 'space_id'=>$space->id])->with('children')->get() as $folder)

                            <div class="folder " folder-id="{{$folder->id}}">
                                <div class="icon"><i class="fa fa-folder"></i></div>
                                <div class="name">{{ $folder->name }}</div>
                            </div>
                        @endforeach
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeaddfileform" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary copyfile">Copy</button>
                </div>
            </form>
        </div>
    </div>
</div>



{{-- move --}}

<div class="modal fade" id="move" tabindex="-1" role="dialog" aria-labelledby="move"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Move To</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="addfileform" enctype="multipart/form-data" method="Post">

                <div class="modal-body">

                    <div class="form-group">
                        <label label for="">Folder  </label>
                        <button type="button" class="btn-prefolder" > Go back </button>


                    </div>



                    <div class="move-folder-row folder-row" prefolder="0">
                        {{-- <div class="folder " folder-id="0">
                            <div class="icon"><i class="fa fa-folder"></i></div>
                            <div class="name">Root</div>
                        </div> --}}
                        @foreach (App\Models\Folder::where(['folder_id' => 0 , 'space' => $namespace , 'space_id'=>$space->id])->with('children')->get() as $folder)

                            <div class="folder " folder-id="{{$folder->id}}">
                                <div class="icon"><i class="fa fa-folder"></i></div>
                                <div class="name">{{ $folder->name }}</div>
                            </div>
                        @endforeach
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeaddfileform" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary movefile">Move</button>
                </div>
            </form>
        </div>
    </div>
</div>



{{-- Add user access --}}


<div class="modal fade" id="adduseraccess"  role="dialog" aria-labelledby="adduseraccess"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add User Access</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('/')}}/fileshare/file/adduser" id="addfileform" enctype="multipart/form-data" method="Post">

                <div class="modal-body">


                    @csrf

                    <input type="hidden" name="file_id" value="{{$file->id}}">

                    <div class="form-group">
                        <label label for="">Select Who Can Access</label>
                        <select name="users[]" id="" class="form-control user-access" style="width:100%;" multiple>
                            @foreach ($space->user as $user)
                                @php
                                    $file_users = $file->users->pluck('id')->toArray();
                                @endphp
                                @if(in_array( $user->id , $file_users ))
                                        {{-- <option selected value="{{$user->id}}"> {{$user->name}} ({{$user->email}})</option> --}}
                                    @else
                                        <option value="{{$user->id}}"> {{$user->name}} ({{$user->email}})</option>

                                @endif
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label label for="fileselectall">Select All</label>
                        <input type="checkbox" id="fileselectall" name="selectall">
                    </div>





                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeaddfileform" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary addfilebtn">Add Access</button>
                </div>
            </form>
        </div>
    </div>
</div>





<script src="{{url('/')}}/assets/js/formvalidation.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{url('/')}}/assets/js/global/preloader.js"></script>
<script src="{{url('/')}}/assets/js/global/toast.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>




<script>
    $(document).ready(function(){

        $(document).ready(function() {
            $('.user-access').select2();
        });

        // delete file
        $('.filedelete').click(function (){
            // var parent = $(this).parent().parent().parent();
            // var fileid = parent.attr('fileid');
            var filecode = "{{$file->file_code}}";

            // console.log(parent.html());
            // return;

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {

                    preloader(true);

                    $.ajax({
                        type: "POST",
                        url: `{{url('/')}}/fileshare/file/delete`,
                        data : {
                            file_code : filecode,
                            _token : "{{csrf_token()}}",

                        },
                        success : function(res){
                            console.log(res);
                            if(res.code == 200){
                                // preloader(false);
                                // $(`.file[filecode=${filecode}]`).remove();



                                toast({
                                    message: `File deleted successfully!`,
                                    type: 'success',
                                    icon: 'fa fa-check'
                                });

                                window.history.back();

                                // swal("Poof! Your file has been deleted!", {
                                //     icon: "success",
                                // });
                            }
                        }

                    });



                } else {
                    swal("Your file is safe!");
                }
            });

        });

        // copy

        $('.btn-prefolder').click(function(){
            var folder_id = $('.copy-folder-row').attr('folder-id');
            getsubfolder('copy-folder-row',folder_id);

        });

        $('.copyfile').click(function(){
            var selectedfolder = $('body .copy-folder-row ').find('.selected');
            var folder_id = selectedfolder.attr('folder-id');
            preloader(true);
            $.ajax({
                url : `{{url('/')}}/fileshare/copy`,
                type : 'POST',
                data: {
                    folder_id : folder_id,
                    file_id : "{{$file->id}}",
                    "_token" : "{{csrf_token()}}"
                },
                success: function(res){
                    // console.log(res);
                    preloader(false);
                    if(res.status == 200){
                        toast({
                            message: `File Copied !`,
                            type: 'success',
                            icon: 'fa fa-tick'
                        });
                        window.location.href = `{{url('/')}}/fileshare/folder/${res.folder.code}`;


                    }
                    else{
                        toast({
                            message: `Can not copy this file!`,
                            type: 'warning',
                            icon: 'fa fa-times'
                        });
                    }
                }
            });
            // console.log(folder_id + "copy this");
        });


        $('.copy-folder-row .folder').first().addClass('selected');



        // move select folder
        $(document).find('.copy-folder-row').on('click' , '.folder' ,function(){
            var folder = $(this);
            var folder_id = $(this).attr('folder-id');
            $('.copy-folder-row ').find('.folder').removeClass('selected');
            folder.addClass('selected');
            // console.log(folder_id);

        });

        // get subfolder
        $(document).find('.copy-folder-row').on('dblclick' , '.folder' ,function(){
            var selectedfolder = $('body .copy-folder-row ').find('.selected');
            var folder_id = selectedfolder.attr('folder-id');
            getsubfolder( "copy-folder-row" ,folder_id);

        });




        // move

        $('.btn-prefolder').click(function(){
            var folder_id = $('.move-folder-row').attr('folder-id');
            getsubfolder('move-folder-row',folder_id);

        });
        $('.move-folder-row .folder').first().addClass('selected');


        // movefile

        $('.movefile').click(function(){
            var selectedfolder = $('body .move-folder-row ').find('.selected');
            var folder_id = selectedfolder.attr('folder-id');
            preloader(true);
            $.ajax({
                url : `{{url('/')}}/fileshare/move`,
                type : 'POST',
                data: {
                    folder_id : folder_id,
                    file_id : "{{$file->id}}",
                    "_token" : "{{csrf_token()}}"
                },
                success: function(res){
                    // console.log(res);
                    preloader(false);
                    if(res.status == 200){
                        toast({
                            message: `File Moved !`,
                            type: 'success',
                            icon: 'fa fa-tick'
                        });
                        window.location.href = `{{url('/')}}/fileshare/folder/${res.folder.code}`;


                    }
                    else{
                        toast({
                            message: `Can not move this file!`,
                            type: 'warning',
                            icon: 'fa fa-times'
                        });
                    }
                }
            });
            // console.log(folder_id + "copy this");
        });

        // move select folder
        $(document).find('.move-folder-row').on('click' , '.folder' ,function(){
            var folder = $(this);
            var folder_id = $(this).attr('folder-id');
            $('.move-folder-row ').find('.folder').removeClass('selected');
            folder.addClass('selected');
            // console.log(folder_id);

        });

        // get subfolder
        $(document).find('.move-folder-row').on('dblclick' , '.folder' ,function(){
            var selectedfolder = $('body .move-folder-row ').find('.selected');
            var folder_id = selectedfolder.attr('folder-id');
            getsubfolder( "move-folder-row", folder_id);

        });





    });

    function getsubfolder( renderbox ,folder_id){


            var rowholder = $(`.${renderbox}`);
            // console.log(folder_id);
            $.ajax({
                url: `{{url('/')}}/fileshare/getsubfolder/${folder_id}`,
                type: "GET",
                success: function(res){
                    // console.log(res);
                    if(res.subfolders.length > 0){
                        rowholder.html('');
                        res.subfolders.forEach(function(folder){
                            rowholder.append(`
                                <div class="folder" folder-id="${folder.id}">
                                    <div class="icon"><i class="fa fa-folder"></i></div>
                                    <div class="name">${ folder.name }</div>
                                </div>

                            `);
                            rowholder.attr('prefolder' , folder.folder_id);
                        });
                        $('.folder-row .folder').first().addClass('selected');
                    }
                    else{
                        toast({
                            message: `No Sub Folders Found!`,
                            type: 'warning',
                            icon: 'fa fa-times'
                        });
                    }
                }
            });
        }

</script>


@endsection
