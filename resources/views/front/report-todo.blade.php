@extends('front.Master.app')
@section('content')
    @include('front.partials.report-head')
    <div class="container box">
        <div class="text-center pt-5">
            <h4>Whose assignments would you like to see?</h4>
        </div>
        <div class="pr-5 pl-5 pt-3 input-group mb-5 mr-auto ml-auto" style="width: 70%">
            <input type="text" class="form-control" placeholder="Jump to a person" aria-label="" aria-describedby="basic-addon1">
        </div>
        <div class=" ">
            <div class="pb-5" data-role="content_filter_group">

                <a class="people-roster__person push_quarter--bottom" data-role="content_filterable" href="#">
                    <div class="row ml-auto" style="width: 83%">
                                <img style="width: 5%" title="name, job-title at company" alt="name" class="avatar" width="40" height="40" src="{{asset('assets/images/blank-avatar.png')}}">
                            </span>
                        <div class="people-roster__person-details pt-2 ">
                            <h4 class="flush txt-size" data-role="content_filter_text" style="font-weight: 700">
                                <span class="decorated app-ios__undecorated">My Assignments</span>

                            </h4>
                            <div class="people-roster__person-metadata" data-role="content_filter_text">
                                <p class="txt-size"> name, job-title at company</p>
                                <span class="u-display-n">n</span>
                            </div>
                        </div>
                    </div>
                </a>

            </div>
        </div>
    </div>
@endsection
