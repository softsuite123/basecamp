@extends('front.Master.app')
@section('content')
    <style>
        .options-menu__action--document {
            background-image: url({{asset('assets/images/document-e7d16acacc3106d98c20c05af5734a9aa275e1acd583a295fb5e24ed107bce0d.svg')}});
            background-size: auto;
            background-position-x: 0.9rem;
        }
        .options-menu__action--folder {
            background-image: url({{asset('assets/images/folder-afdc59bec341907293d55c4896c3e197cf06e4ba957e7c1f1dcd41bd328064b8.svg')}});
            background-size: auto;
            background-position-x: 0.9rem;
        }
        .options-menu__action--upload {
            background-image: url({{asset('assets/images/upload-7534610785936c3aaa9e6ed3ef92e06bbf0ebe82a7216bd03e1d007b21854877.svg')}});
            background-size: auto;
            background-position-x: 0.9rem;
        }
        .options-menu__action--google-drive {
            background-image: url({{asset('assets/images/google-drive-82f72a7b4cd709489a8db336e79587956d59569a73fcce7da55bddb650ba8a0f.svg')}});
            background-size: auto;
            background-position-x: 0.9rem;
        }
        .options-menu__action--dropbox {
            background-image: url({{asset('assets/images/dropbox-3f67f0de7c1ca08eb4398215da4aa383d797dfe3bd47481ef97d1398cb420eba.svg')}});
            background-size: auto;
            background-position-x: 0.9rem;
        }
        .options-menu__action--box {
            background-image: url({{asset('assets/images/box-c14aee91da9bc38b25fd7c6c9ceb1bfdaeaf951c75b49cee6989917fed0141ff.svg')}});
            background-size: auto;
            background-position-x: 0.9rem;
        }
        .options-menu__action--one-drive {
            background-image: url({{asset('assets/images/one-drive-bdbdba43fe3cc770f79526943c5e3611990fbbdbdbd2e583f57c70014fa9e316.svg')}});
            background-size: auto;
            background-position-x: 0.9rem;
        }
    </style>
    <div class="container">
        @include('front.partials.bucket-head')
        <div class="box ml-auto mr-auto p-5">
            <div class="row">
                <div class="doc-btn">
                    <button onclick="optionsMenu()" class="btn btn-primary">+ New...</button>
                        <div id="docs-option" style="display: none" class="options-menu__content options-menu__content--align-left expanded_content app-ios__web-header-new txt-size">
                            <a class="options-menu__action options-menu__action--document" data-bridge-action="new" data-bridge-action-type="add" data-bridge-action-icon="document" href="{{route('front.new-doc')}}">Start a new doc</a>

                            <a class="options-menu__action options-menu__action--folder" data-bridge-action="new" data-bridge-action-type="add" data-bridge-action-icon="folder" data-behavior="add_new_folder collapse_on_click" href="#">Make a new folder</a>

                            <input type="file" >

                            <h5 class="push--top push_quarter--bottom break txt--subtle"><span>Or link to files from…</span></h5>

                            <a title="Add a file from Google Drive" class="options-menu__action options-menu__action--google-drive" data-bridge-action="new" data-bridge-action-type="add" data-bridge-action-icon="service-google-drive" href="#">Google Drive</a>

                            <a title="Add a file from Dropbox" class="options-menu__action options-menu__action--dropbox" data-bridge-action="new" data-bridge-action-type="add" data-bridge-action-icon="service-dropbox" href="#">Dropbox</a>

                            <a title="Add a file from Box" class="options-menu__action options-menu__action--box" data-bridge-action="new" data-bridge-action-type="add" data-bridge-action-icon="service-box" href="#">Box</a>

                            <a title="Add a file from OneDrive" class="options-menu__action options-menu__action--one-drive" data-bridge-action="new" data-bridge-action-type="add" data-bridge-action-icon="service-one-drive" href="#">OneDrive</a>
                        </div>
                </div>
                <div class="mr-auto ml-auto">
                    <h3>Docs & Files</h3>
                </div>
                <div>
                    <button class="btn pb-3"> <img style="width: auto" src="{{asset('assets/images/overflow-51cd8be896d0981b46a99452c79d3cc3d447a5e8a883b55ad3ceb1080277c6e6.svg')}}"> </button>
                </div>

            </div>
        </div>

    </div>

@endsection
