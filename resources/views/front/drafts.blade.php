@extends('front.Master.app')
@section('content')
    <div class="container">
        <div class="box m-5">
            <div class="panel manage-panel panel--perma panel--perma-person p-5">
                <section>
                    <header class="perma__header push--bottom text-center">
                        <img  class="avatar img--sized push_quarter--bottom app-mobile__hide" data-turbolinks-permanent="true" data-current-person-avatar="true" id="my_assignments_avatar" src="{{asset('assets/images/blank-avatar.png')}}" width="64" height="64" title="name" alt="">
                        <h1 class="perma__title app-mobile__hide">Your Drafts</h1>
                    </header>

                    <div class="align--center app-ios__hide">
                        <h3 class="flush--ends u-hide-on-desktop app-mobile__pad_top">You don’t have any drafts right now.</h3>
                        <p class="push_half--top">Any time you write something in Basecamp (like a message or document) and save it as a draft, it’ll show up here. Your saved drafts won’t be visible to anyone else.</p>
                        <img style="width: 100%" class="img--bordered" src="https://bc3-production-assets-cdn.basecamp-static.com/assets/blank_slates/save_as_draft-211a454d81b21fb6dca2f8b533cbe669c8f0b5e5a85befcabe053c97c565ee94.png">
                    </div>
                </section>

                <div class="centered app-ios__show app-ios__blank_slate app-ios__blank_slate__drafts pt-5">

                    <h1>No drafts yet!</h1>
                    <p>Any time you write something in Basecamp (like a message or document) and save it as a draft, it’ll show up here. Your saved drafts won’t be visible to anyone else.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
