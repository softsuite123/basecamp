@extends('front.Master.app')
@section('content')
@include('front.partials.report-head')
    <div class="container box" >
        <div class="row">
            <div class="email-btn">
                <button class="btn ">Emailing a daily summary</button>
            </div>

        </div>
        <div class=" text-center">
            <h3>Latest Activity</h3>
            <h5 class="pt-3">Saturday, February 14</h5>
        </div>
        <div class="row">
            <div class="col-md-6 p-5">
                <h4 class="latest-activity__project txt-size" data-role="bucket">
                    <a href="{{route('front.project-manage')}}" style="color: #877457">Rebranding a business</a>
                </h4>
                <hr width="98%">
                <article class="activity-item ml-4" data-timeline-item="">

                    <div class="activity-item__summary">
                        <div class="activity-item__timestamp metadata">
                            <time datetime="2021-02-13T06:28:42Z" data-local="time" data-format="%l:%M%P" title="February 13, 2021 at 11:28am PST" data-localized="">11:28am</time>
                        </div>

                        <div class="row">
                                <img style="width: 5%" title="name, job-title at company" alt="name" class="avatar" width="64"  src="{{asset('assets/images/blank-avatar.png')}}">



                            <h4 class="activity-item__what txt-size mt-2 ">
                                name started a new project called <br>
                                <a class="decorated" href="{{route('front.project-manage')}}">Rebranding a business</a>
                            </h4>
                        </div>

                    </div>


                    <a class="activity-item__link" href="#"> </a>
                </article>
            </div>

            <div class="col-md-6 p-5">
                <h4 class="latest-activity__project txt-size" data-role="bucket">
                    <a href="#" style="color: #877457">Rebranding a business</a>
                </h4>
                <hr width="98%">
                <article class="activity-item ml-4" data-timeline-item="">

                    <div class="activity-item__summary">
                        <div class="activity-item__timestamp metadata">
                            <time datetime="2021-02-13T06:28:42Z" data-local="time" data-format="%l:%M%P" title="February 13, 2021 at 11:28am PST" data-localized="">11:28am</time>
                        </div>

                        <div class="row">
                                <img style="width: 5%" title="name, job-title at company" alt="name" class="avatar" width="64" height="64" src="{{asset('assets/images/blank-avatar.png')}}">



                            <h4 class="activity-item__what txt-size mt-2 ">
                                name started a new project called <br>
                                <a class="decorated" href="#">Rebranding a business</a>
                            </h4>
                        </div>

                    </div>


                    <a class="activity-item__link" href="#"> </a>
                </article>
            </div>
        </div>
    </div>
@endsection
