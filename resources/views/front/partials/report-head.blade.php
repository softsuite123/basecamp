<div class="container">
    <div class="row justify-content-center" style="width: 100%;">
        <div class="report-menu push_half--ends row" data-controller="toggle-class" data-toggle-class-class-name="report-menu--expanded">

            <div style="margin-left: auto" class="report-menu__group pt-3">
                <a class="report-menu__item report-menu__item--selected col-md-4" href="{{route('front.report')}}">
                    <div class="pl-3 pr-3 pt-1 pb-2  content-type-icon  content-type-icon--activity-report content-type-icon--for-report-menu">
                        All the latest activity
                    </div>

                </a>      <a class="report-menu__item col-md-4 " href="{{route('front.report.user')}}">
                    <div class="pl-3 pt-1 pr-3 pb-2 content-type-icon content-type-icon--person-report content-type-icon--for-report-menu">
                        Someone’s activity
                    </div>

                </a>    </div>
            <div class="report-menu__group pt-3">
                <a class="report-menu__item  col-md-4" href="{{route('front.report.todo')}}">
                    <div class="pl-3 pr-3 pt-1 pb-2 ml-2 content-type-icon content-type-icon--assignment content-type-icon--for-report-menu">
                        Someone’s assignments
                    </div>

                </a>      <a class="report-menu__item col-md-4 " href="/4987109/reports/todos/progress">
                    <div class="pl-3 pr-3 pt-1 ml-2 pb-2 content-type-icon content-type-icon--todo content-type-icon--for-report-menu">
                        To-dos added &amp; completed
                    </div>

                </a>    </div>
            <div style="margin-right: auto" class="report-menu__group pt-3">
                <a class="report-menu__item col-md-4" href="/4987109/reports/todos/overdue">
                    <div class="pl-3 pr-3 pt-1 pb-2  ml-2 content-type-icon content-type-icon--reports content-type-icon--for-report-menu">
                        Overdue to-dos
                    </div>

                </a>      <a class="report-menu__item col-md-4 " href="{{route('front.report.schedule')}}">
                    <div class="pl-3 pr-3 pt-1 ml-2 pb-2 content-type-icon content-type-icon--schedule content-type-icon--for-report-menu">
                        Upcoming dates
                    </div>

                </a>    </div>
        </div>
    </div>
</div>
