<div class="topheader">


    <div class="sidebar-toggle">
        <i class="fa fa-bars"></i>
    </div>

    <div class="logo" style="height: 69%;">
        <img src="{{url('/')}}/assets/images/teamsleader-logo.png" alt="" class="img">
    </div>

    <div class="searchbarholder">
        <input type="text" placeholder="Search..." onclick="openglobalsearch()" class="searchbar">
    </div>
</div>
