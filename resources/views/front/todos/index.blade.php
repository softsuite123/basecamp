@extends('front.Master.app')
@section('content')


@include('front.Master.partials.sidebar')


@php

    $space = App\Http\Controllers\SpaceController::get();
    // dd($space);
    //
    $space->load('user');
    $space->load('todoLists');
    $space_url = strtolower(class_basename($space));
@endphp

<link rel="stylesheet" href="{{url('/')}}/assets/css/invitetocompany.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/todo.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/toast.css">



<div class="actionpanel" >
    <a href="{{url('/')}}/{{$space_url}}/controlpanel" class="link">
        <i class="fa fa-th-large"></i>
    </a>
    <div class="name">{{$space->name}}</div>
</div>
<div class="panelcard">
    <div class="header">
        <a href="{{url('/')}}/todo/list/new" class="btn btn-success addbtn " style="background-color: #2dca81 !important;">
            <i class="fa fa-plus"></i> Add Todo List
        </a>
        <div class="title">
            Todo List
        </div>
    </div>


    <div class="body">


        <div class="task-grid">

            {{-- {{dd($space)}} --}}

            @foreach ($space->todoLists as $list )

                <div class="grid-item TodoList " todoList="{{$list->id}}">
                    <div class="cardtitle">
                        <div class="setting-icon">
                            <i class="fa fa-cog" ></i>
                        </div>


                        <div class="menu">
                            <div class="item delete " listid="{{$list->id}}">
                                <i class="fa fa-trash"></i>
                                <label for="">Delete</label>
                            </div>


                            <div class="item">
                                <i class="fa fa-archive"></i>
                                <label for="">Archive</label>
                            </div>

                            <div class="item">
                                <i class="fa fa-clone"></i>
                                <label for="">Copy</label>
                            </div>

                            <div class="item">
                                <i class="fa fa-arrows"></i>
                                <label for="">Move</label>
                            </div>


                        </div>

                        <div  class="title" >
                           <a href="{{url('/')}}/todo/list/{{$list->id}}"> {{$list->name}} </a>
                        </div>

                    </div>
                    <div class="body">
                        <div class="placeholder">
                            <div class="icon">
                                <img src="{{url('/')}}/assets/images/tick.png" alt="" class="img">
                            </div>
                            <div class="description">
                                {!! $list->description !!}

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach






        </div>



        {{-- <div class="to-do-form" style="margin-top:20px;">
            <form>
                <div class="form-group">
                    <input type="text" class="form-control m-3 to-do-list" placeholder="Name this list..." style="width: 97%">
                </div>
                <div class="form-group p-5">
                    <textarea name="editor1"></textarea>
                    <script>
                        CKEDITOR.replace( 'editor1' );
                    </script>
                </div>

                <button type="submit" class="btn btn-primary txt-size ml-5 mb-3">Add this list</button>
            </form>
        </div> --}}
    </div>






</div>




<script src="{{url('/')}}/assets/js/formvalidation.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{url('/')}}/assets/js/global/preloader.js"></script>
<script src="{{url('/')}}/assets/js/global/toast.js"></script>



<script>
    $('document').ready(function(){
        $('.delete').click(function(){
            var listid = $(this).attr('listid');

            $.ajax({
                type: "Delete",
                url: "{{url('/')}}/todo/list",
                data: {
                    listid:listid,
                    "_token" : "{{csrf_token()}}"
                },
                success: function(res){
                    console.log(res);
                    if(res.code == 200){
                        $(`[todoList=${listid}]`).remove();
                        toast({
                            message: `${res.message}`,
                            type : 'warning',
                            icon: 'fa fa-trash'
                        });

                    }
                }
            });
        });
    });
</script>


@endsection
