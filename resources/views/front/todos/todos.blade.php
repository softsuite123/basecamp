@extends('front.Master.app')
@section('content')


@include('front.Master.partials.sidebar')




@php

    $space = App\Http\Controllers\SpaceController::get();
    // dd($space);
    //
    $space->load('user');
    $space->load('todoLists');
    // $space->load('todos')->where('user_id' , Auth::user()->id);
    // dd($space->todos);


    $space_url = strtolower(class_basename($space));

    $list = App\Models\TodoList::where('id' ,$todo_list_id)->with('Todo' , function($q){
        $q->orderBy('created_at', 'DESC');
    })->first();
    $todos = $list->Todo;




@endphp

<link rel="stylesheet" href="{{url('/')}}/assets/css/invitetocompany.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/todo.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/toast.css">



<div class="actionpanel" >
    <a href="{{url('/')}}/{{$space_url}}/controlpanel" class="link">
        <i class="fa fa-th-large"></i>
    </a>
    <div class="name">{{$space->name}}</div>
</div>
<div class="panelcard">
    <div class="header">
        <a href="{{url('/')}}/todo/add/{{$list->id}}" class="btn btn-success addbtn " style="background-color: #2dca81 !important; width:120px;">
            <i class="fa fa-plus"></i> Add Todo
        </a>
        <div class="title">
            {{$list->name}}
        </div>

        <select class="form-control" name="todo-type" id="todo-type" style="width:fit-content;">
            <option value="all-todos">All</option>
            <option value="my-todos">My Todos</option>
        </select>
    </div>


    <div class="body">


        {{-- {{dd($list)}} --}}
        <div class="task-grid all-todos todo-block">

            {{-- {{dd($space)}} --}}

            @foreach ($todos as $todo )

                {{-- {{dd($todo)}} --}}

                <div class="grid-item TodoList {{$todo->is_completed == 1 ? 'todo-checked' : 'todo-unchecked' }}" todo="{{$todo->id}}">
                    <div class="cardtitle">
                        <div class="setting-icon">
                            <i class="fa fa-cog" ></i>
                        </div>


                        <div class="menu">

                            <div class="item status-toggle action-checked " todoid="{{$todo->id}}" >
                                <i class="fa fa-check"></i>
                                <label for="">Mark Done</label>
                            </div>

                            <div class="item status-toggle action-unchecked " todoid="{{$todo->id}}">
                                <i class="fa fa-times"></i>
                                <label for="">Mark Undone</label>
                            </div>

                            <div class="item delete " todoid="{{$todo->id}}">
                                <i class="fa fa-trash"></i>
                                <label for="">Delete</label>
                            </div>


                            <div class="item">
                                <i class="fa fa-archive"></i>
                                <label for="">Archive</label>
                            </div>

                            <div class="item">
                                <i class="fa fa-clone"></i>
                                <label for="">Copy</label>
                            </div>

                            <div class="item">
                                <i class="fa fa-arrows"></i>
                                <label for="">Move</label>
                            </div>


                        </div>

                        <div href="#" class="title" >
                           <a href="{{url('/')}}/todo/show/{{$list->id}}/{{$todo->id}}"> {{$todo->name}} </a>
                        </div>



                    </div>
                    <div class="body">
                        <div class="placeholder">
                            <div class="timeago">
                                {{$todo->created_at->diffForHumans()}}
                            </div>
                            <div class="icon icon-checked">
                                <img src="{{url('/')}}/assets/images/tick.png" alt="" class="img">
                            </div>
                            <div class="icon icon-unchecked">
                                <img src="{{url('/')}}/assets/images/cross.png" alt="" class="img">
                            </div>
                            <div class="description">
                                {!! $todo->description !!}


                            </div>
                            <div class="duedate">
                                {{Carbon\Carbon::parse( $todo->due_date)->diffForHumans()}}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach






        </div>



        <div class="task-grid my-todos todo-block" style="display: none;">

            {{-- {{dd($list)}} --}}


            @foreach ($todos as $list )

                @php
                    $userids= [];
                    foreach($list->User as $user){
                        array_push($userids, $user->id);
                    }
                    $userids = array_unique($userids);
                    // $users = (array)$users;
                    // dd($users);

                @endphp

            {{-- {{dd($list->User)}} --}}

                @foreach ($userids as $id)

                    @if ($id == Auth::user()->id)
                    <div class="grid-item TodoList {{$todo->is_completed == 1 ? 'todo-checked' : 'todo-unchecked' }}" todo="{{$todo->id}}">
                        <div class="cardtitle">
                            <div class="setting-icon">
                                <i class="fa fa-cog" ></i>
                            </div>


                            <div class="menu">

                                <div class="item status-toggle action-checked " todoid="{{$todo->id}}" >
                                    <i class="fa fa-check"></i>
                                    <label for="">Mark Done</label>
                                </div>

                                <div class="item status-toggle action-unchecked " todoid="{{$todo->id}}">
                                    <i class="fa fa-times"></i>
                                    <label for="">Mark Undone</label>
                                </div>

                                <div class="item delete " todoid="{{$todo->id}}">
                                    <i class="fa fa-trash"></i>
                                    <label for="">Delete</label>
                                </div>


                                <div class="item">
                                    <i class="fa fa-archive"></i>
                                    <label for="">Archive</label>
                                </div>

                                <div class="item">
                                    <i class="fa fa-clone"></i>
                                    <label for="">Copy</label>
                                </div>

                                <div class="item">
                                    <i class="fa fa-arrows"></i>
                                    <label for="">Move</label>
                                </div>


                            </div>

                            <div href="#" class="title" >
                               <a href=""> {{$todo->name}} </a>
                            </div>



                        </div>
                        <div class="body">
                            <div class="placeholder">
                                <div class="timeago">
                                    {{$todo->created_at->diffForHumans()}}
                                </div>
                                <div class="icon icon-checked">
                                    <img src="{{url('/')}}/assets/images/tick.png" alt="" class="img">
                                </div>
                                <div class="icon icon-unchecked">
                                    <img src="{{url('/')}}/assets/images/cross.png" alt="" class="img">
                                </div>
                                <div class="description">
                                    {!! $todo->description !!}


                                </div>
                                <div class="duedate">
                                    {{Carbon\Carbon::parse( $todo->due_date)->diffForHumans()}}
                                </div>
                            </div>
                        </div>
                    </div>

                    @endif
                @endforeach


            @endforeach






        </div>








        {{-- <div class="to-do-form" style="margin-top:20px;">
            <form>
                <div class="form-group">
                    <input type="text" class="form-control m-3 to-do-list" placeholder="Name this list..." style="width: 97%">
                </div>
                <div class="form-group p-5">
                    <textarea name="editor1"></textarea>
                    <script>
                        CKEDITOR.replace( 'editor1' );
                    </script>
                </div>

                <button type="submit" class="btn btn-primary txt-size ml-5 mb-3">Add this list</button>
            </form>
        </div> --}}
    </div>






</div>




<script src="{{url('/')}}/assets/js/formvalidation.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{url('/')}}/assets/js/global/preloader.js"></script>
<script src="{{url('/')}}/assets/js/global/toast.js"></script>



<script>
    $('document').ready(function(){
        $('.delete').click(function(){
            var todoid = $(this).attr('todoid');
            // console.log(todoid);
            // return;
            preloader(true);

            $.ajax({
                type: "Delete",
                url: "{{url('/')}}/todo/delete",
                data: {
                    todoid:todoid,
                    "_token" : "{{csrf_token()}}"
                },
                success: function(res){
                    // console.log(res);
                    preloader(false);
                    if(res.code == 200){

                        $(`[todo=${todoid}]`).remove();
                        toast({
                            message: `${res.message}`,
                            type : 'warning',
                            icon: 'fa fa-trash'
                        });

                    }
                }
            });
        });


        $('#todo-type').change(function(){
            var block = $(this).val();
            console.log(block);
            $('.todo-block').hide();

            $(`.${block}`).show();
        });


        $('.status-toggle').click(function(){
            var parent = $(this).parent().parent().parent();

            var todoid = parent.attr('todo');
            preloader(true);

            if($(this).hasClass('action-checked')){

                $.ajax({
                    url : `{{url('/')}}/todo/toggle`,
                    type : 'POST',
                    data : {
                        "_token" : "{{csrf_token()}}",
                        id : todoid,
                        type: 'internal',
                        status : 1,

                    },
                    success: function(res){
                        // console.log(data);
                        preloader(false);
                        if(res.status == 200){

                            $(`[Todo=${todoid}]`).addClass('todo-checked');
                            $(`[Todo=${todoid}]`).removeClass('todo-unchecked');
                            // $(this).hide();
                            // $('.action-unchecked').show();

                            toast({
                                message: `${res.message}`,
                                type : 'success',
                                icon: 'fa fa-check'
                            });



                        }
                        else{
                            toast({
                                message: `Unable To Change Status`,
                                type : 'warning',
                                icon: 'fa fa-times'
                            });
                        }
                    }
                });


            }
            else{


                $.ajax({
                    url : `{{url('/')}}/todo/toggle`,
                    type : 'POST',
                    data : {
                        "_token" : "{{csrf_token()}}",
                        id : todoid,
                        type: 'internal',
                        status : 0,

                    },
                    success: function(res){
                        // console.log(data);
                        preloader(false);
                        if(res.status == 200){

                            $(`[Todo=${todoid}]`).addClass('todo-unchecked');
                            $(`[Todo=${todoid}]`).removeClass('todo-checked');
                            // $(this).hide();
                            // $('.action-checked').show();

                            toast({
                                message: `${res.message}`,
                                type : 'warning',
                                icon: 'fa fa-times'
                            });



                        }
                        else{
                            toast({
                                message: `Unable To Change Status`,
                                type : 'warning',
                                icon: 'fa fa-times'
                            });
                        }
                    }
                });



            }

        });

    });
</script>


@endsection
