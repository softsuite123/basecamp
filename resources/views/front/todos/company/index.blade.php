@extends('front.Master.app')
@section('content')
    <div class="container">
        @include('front.partials.bucket-head')
        <div class="box ml-auto mr-auto p-5">
           <div class="text-center">
               <h2>To-dos</h2>
           </div>
            <div class="to-do-form">
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control m-3 to-do-list" placeholder="Name this list..." style="width: 97%">
                    </div>
                    <div class="form-group p-5">
                        <textarea name="editor1"></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1' );
                        </script>
                    </div>

                    <button type="submit" class="btn btn-primary txt-size ml-5 mb-3">Add this list</button>
                </form>
            </div>
        </div>

    </div>

@endsection
