@extends('front.Master.app')
@section('content')

@include('front.Master.partials.sidebar')





@php
    $space = App\Http\Controllers\SpaceController::get('space');
    // dd($space);
    // dd($space->load('user'));
    $space_url = strtolower(class_basename($space));

@endphp

<link rel="stylesheet" href="{{url('/')}}/assets/css/invitetocompany.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/todo.css">



<div class="actionpanel" >
    <a href="{{url('/')}}/{{$space_url}}/controlpanel" class="link" >
        <i class="fa fa-th-large"></i>
    </a>
    <div class="name">{{$space->name}}</div>
</div>
<div class="panelcard">
    <div class="header">

        <div class="title" style="width: 100%;">
            Add Todo List
        </div>
    </div>


    <div class="body">

        <div class="to-do-form" style="margin-top:20px; padding:20px;">
            <form id="newlistform" action="{{url('/')}}/todo/list/new" method="post"  >
                <div class="form-group">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="text" name="name" class="form-control  to-do-list inp" required="true" placeholder="Name this list..." style="width: 97%">
                </div>
                <div class="form-group">
                    <p class="description-toggle" > Add Extra Detail</p>
                </div>
                <div class="form-group p-2" id="list-description-block" style="display: none;" >
                    <textarea name="description"></textarea>
                    <script>
                        CKEDITOR.replace( 'description' );
                    </script>
                </div>

                <button type="button" class="btn btn-primary txt-size ml-5 mb-3 btn-submit ">Add this list</button>
            </form>
        </div>
    </div>






</div>




<script src="{{url('/')}}/assets/js/formvalidation.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{url('/')}}/assets/js/global/preloader.js"></script>



<script>
    $(document).ready(function(){
        $('.description-toggle').click(function(){
            $('#list-description-block').show();
            $(this).hide();
        });


        $('.btn-submit').click(function(){
            validation = validateform('#newlistform');
            if(validation.errors < 1 ){
                $('#newlistform').submit();

            }
        });


    });
</script>



@endsection
