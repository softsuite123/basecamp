@extends('front.Master.app')
@section('content')


@include('front.Master.partials.sidebar')


@php
    $space = App\Http\Controllers\SpaceController::get('space');
    // dd($space);
    // dd($space->load('user'));
    $space->load('user');
    // dd($space);
    $space_url = strtolower(class_basename($space));
    $space_url = strtolower(class_basename($space));

    $list = App\Models\TodoList::where('id' ,$todo_list_id)->first();


@endphp

<link rel="stylesheet" href="{{url('/')}}/assets/css/invitetocompany.css">
<link rel="stylesheet" href="{{url('/')}}/assets/css/todo.css">
{{-- <link rel="stylesheet" href="{{url('/')}}/assets/datepicker/css/bootstrap-datepicker.min.css"> --}}
{{-- <link rel="stylesheet" href="{{url('/')}}/assets/datepicker/css/bootstrap-datepicker3.min.css"> --}}
{{-- <link rel="stylesheet" href="{{url('/')}}/assets/datepicker/css/bootstrap-datepicker.standalone.min.css"> --}}

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />



<div class="actionpanel" >
    <a href="{{url('/')}}/{{$space_url}}/controlpanel" class="link" >
        <i class="fa fa-th-large"></i>
    </a>
    <div class="name">{{$space->name}}</div>
</div>
<div class="panelcard">
    <div class="header">

        <div class="title" style="width: 100%;">
            Add Todo
        </div>
    </div>


    <div class="body">

        <div class="to-do-form" style="margin-top:20px; padding:20px;">
            <form id="newlistform" action="{{url('/')}}/todo/add/{{$list->id}}" method="post"  >
                <div class="form-group">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="text" name="name" class="form-control  to-do-list inp" required="true" placeholder="Name this todo..." style="width: 97%">
                </div>
                <div class="form-group">
                    <p class="description-toggle" > Add Extra Detail</p>
                </div>
                <div class="form-group p-2" id="list-description-block" style="display: none;" >
                    <div class="form-group" style="display: flex;">
                      <label for="">Assigned to</label>
                      <select name="assignedUsers[]" id="assignedUsers" multiple="multiple" style="width:100%" >
                        @foreach ($space->user as $user )
                            <option value="{{$user->id}}"> {{$user->name}} ({{$user->email}})</option>
                        @endforeach
                      </select>

                    </div>

                    <div class="form-group" style="display: flex;">
                        <label for="">When done, notify</label>
                        <select name="notifyUsers[]" id="notifyUsers" multiple="multiple" style="width:100%" >
                            @foreach ($space->user as $user )
                                <option value="{{$user->id}}"> {{$user->name}} ({{$user->email}})</option>
                            @endforeach
                        </select>

                    </div>

                    <div class="dateholder" style="padding-left: 50px;">



                        <table>
                            <tr>
                                <td>
                                    <input type="radio" name="radio_date" value="1" id="">
                                </td>
                                <td>
                                    <label for="">No Date</label>

                                </td>
                                <td>

                                    {{-- <input type="text" name="due_date" id="noDueDate" class="form-control" placeholder="" style="border: 1px solid lightgray;" > --}}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <input type="radio" name="radio_date" value="2" id="">
                                </td>
                                <td>
                                    <label for="">Specific Date</label>

                                </td>
                                <td>
                                    <input type="datetime-local" name="due_date" id="singleDueDate" class="form-control" placeholder="" style="border: 1px solid lightgray;" >

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <input type="radio" name="radio_date" value="3" id="">
                                </td>
                                <td>
                                    <label for="">Run Multiple Date</label>

                                </td>
                                <td>
                                    <input type="datetime-local" name="due_date_multiple" id="doubleDueDate" class="form-control" placeholder="" style="border: 1px solid lightgray;" >

                                </td>

                            </tr>
                        </table>
                    </div>

                    <div class="spacer" style="height:50px;"></div>

                    <textarea name="description"></textarea>
                    <script>
                        CKEDITOR.replace( 'description' );
                    </script>
                </div>

                <button type="button" class="btn btn-primary txt-size ml-5 mb-3 btn-submit ">Add this Todo</button>
            </form>
        </div>
    </div>






</div>




<script src="{{url('/')}}/assets/js/formvalidation.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{url('/')}}/assets/js/global/preloader.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- <link rel="stylesheet" href="{{url('/')}}/assets/datepicker/js/bootstrap-datepicker.min.js"> --}}





<script>
    $(document).ready(function(){
        $('.description-toggle').click(function(){
            $('#list-description-block').show();
            $(this).hide();
        });


        $('.btn-submit').click(function(){
            validation = validateform('#newlistform');
            if(validation.errors < 1 ){
                $('#newlistform').submit();

            }
        });

        $('#assignedUsers').select2();
        $('#notifyUsers').select2();

        // $('#noDueDate').datepicker({

        // });

        // $('#singleDueDate').datepicker({

        // });

        // $('#doubleDueDate').datepicker({
        //     todayBtn: "linked",
        //     clearBtn: true,
        //     autoclose: true,
        //     todayHighlight: true
        // });


    });
</script>



@endsection
