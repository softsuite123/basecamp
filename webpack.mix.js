const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css')
//     .sourceMaps();


mix.js('resources/js/app.js', 'public/js').vue();


// mix.js('resources/js/app.js', 'public/js').vue()
//     .postCss('resources/css/app.css', 'public/css', [
//         //
//     ]);


mix.js( 'resources/js/groupchat/app.js' ,'public/js/groupchat.js').vue();


//     var path = require('path');
//  var webpack = require('webpack');
//  module.exports = {
//      entry: './resources/js/app.js',
//      output: {
//          path: path.resolve(__dirname, 'build'),
//          filename: 'app.bundle.js'
//      },
//      module: {
//          loaders: [
//              {
//                  test: /\.js$/,
//                  loader: 'babel-loader',
//                  query: {
//                     "presets": ["@babel/preset-env", "@babel/preset-react"]
//                 }
//              }
//          ]
//      },
//      stats: {
//          colors: true
//      },
//      devtool: 'source-map'
//  };
